package ar.edu.unrn.municipiopp.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class DerbyConnection {

	private DerbyConnection() {

	}

	/**
	 * This method always returns a new Connection instance.
	 * 
	 * @return Connection
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	protected static Connection serverAccess() throws SQLException {
		ResourceBundle resource = ResourceBundle.getBundle("derby");
		String driver = resource.getString("serverDriver");
		String url = resource.getString("serverUrl");
		String password = resource.getString("password");
		String user = resource.getString("user");
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new SQLException("Hubo un problema al momento de iniciar la conexión con la base de datos.");
		}
		Connection serverConnection = DriverManager.getConnection(url, user, password);

		return serverConnection;
	}

	/**
	 * This method always returns a new Connection instance.
	 * 
	 * @return Connection
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	protected static Connection localAccess() throws SQLException {
		ResourceBundle resource = ResourceBundle.getBundle("derby");
		String driver = resource.getString("localDriver");
		String url = resource.getString("localUrl");
		String password = resource.getString("password");
		String user = resource.getString("user");
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new SQLException("Hubo un problema al momento de iniciar la conexión con la base de datos.");
		}
		Connection localConnection = DriverManager.getConnection(url, user, password);

		return localConnection;
	}
}
