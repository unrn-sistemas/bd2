package ar.edu.unrn.municipiopp.models;

public class Categoria extends GenericModel {
	private String nombre;
	private Integer puntaje;

	/**
	 * Constructor para instancias existentes en la BD.
	 */
	public Categoria(String id, String nombre, Integer puntaje) {
		super(id);
		this.nombre = nombre;
		this.puntaje = puntaje;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Categoria(String nombre, Integer puntaje) {
		super();
		this.nombre = nombre;
		this.puntaje = puntaje;
	}

	/**
	 * Cualquier parámetro puede ser null.
	 * 
	 * @param nombre
	 *            nullable
	 * @param puntaje
	 *            nullable
	 */
	public void update(String nombre, Integer puntaje) {
		super.update(nombre != null || puntaje != null);
		this.nombre = nombre == null ? this.nombre : nombre;
		this.puntaje = puntaje == null ? this.puntaje : puntaje;
	}

	public String getNombre() {
		return nombre;
	}

	public Integer getPuntaje() {
		return puntaje;
	}

}
