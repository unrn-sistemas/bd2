package ar.edu.unrn.municipiopp.dto;

import java.time.LocalDate;

import ar.edu.unrn.municipiopp.models.Evento;

public class EventoDTO extends GenericDTO {
	private String descripcion;
	private LocalDate fecha;
	private String reclamoId;

	public EventoDTO(String id, String descripcion, LocalDate fecha, String reclamoId) {
		this.id = id;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.reclamoId = reclamoId;
	}

	public EventoDTO(Evento evento) {
		this.id = evento.getId();
		this.descripcion = evento.getDescripcion();
		this.fecha = evento.getFecha();
		this.reclamoId = evento.getReclamo().getId();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getReclamoId() {
		return reclamoId;
	}

	public void setReclamoId(String reclamoId) {
		this.reclamoId = reclamoId;
	}

}
