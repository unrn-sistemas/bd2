package ar.edu.unrn.municipiopp.dto;

import ar.edu.unrn.municipiopp.models.Producto;

public class ProductoDTO extends GenericDTO {
	private String nombre;
	private Integer puntajeCosto;

	public ProductoDTO() {
	}

	public ProductoDTO(Producto producto) {
		this.id = producto.getId();
		this.puntajeCosto = producto.getPuntajeCosto();
		this.nombre = producto.getNombre();
	}

	public Integer getPuntajeCosto() {
		return puntajeCosto;
	}

	public void setPuntajeCosto(Integer puntajeCosto) {
		this.puntajeCosto = puntajeCosto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
