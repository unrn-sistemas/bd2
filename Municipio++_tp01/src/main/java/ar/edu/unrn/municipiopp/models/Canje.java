package ar.edu.unrn.municipiopp.models;

import java.time.LocalDate;

public class Canje extends GenericModel {
	private LocalDate fecha;
	private Ciudadano ciudadano;
	private Producto producto;

	/**
	 * Constructor para instancias existentes en la BD.
	 */
	public Canje(String id, LocalDate fecha, Ciudadano ciudadano, Producto producto) {
		super(id);
		this.fecha = fecha;
		this.ciudadano = ciudadano;
		this.producto = producto;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Canje(LocalDate fecha, Ciudadano ciudadano, Producto producto) {
		super();
		this.fecha = fecha;
		this.ciudadano = ciudadano;
		this.producto = producto;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public Producto getProducto() {
		return producto;
	}

	public GenericModel getCiudadano() {
		return ciudadano;
	}
}
