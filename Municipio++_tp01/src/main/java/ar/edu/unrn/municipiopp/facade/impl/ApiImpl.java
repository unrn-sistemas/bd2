package ar.edu.unrn.municipiopp.facade.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.municipiopp.dao.DAOInterface;
import ar.edu.unrn.municipiopp.dao.impl.CanjeDAO;
import ar.edu.unrn.municipiopp.dao.impl.CategoriaDAO;
import ar.edu.unrn.municipiopp.dao.impl.CiudadanoDAO;
import ar.edu.unrn.municipiopp.dao.impl.EventoDAO;
import ar.edu.unrn.municipiopp.dao.impl.ProductoDAO;
import ar.edu.unrn.municipiopp.dao.impl.ReclamoDAO;
import ar.edu.unrn.municipiopp.dto.CanjeDTO;
import ar.edu.unrn.municipiopp.dto.CategoriaDTO;
import ar.edu.unrn.municipiopp.dto.CiudadanoDTO;
import ar.edu.unrn.municipiopp.dto.EventoDTO;
import ar.edu.unrn.municipiopp.dto.ProductoDTO;
import ar.edu.unrn.municipiopp.dto.ReclamoDTO;
import ar.edu.unrn.municipiopp.facade.Api;
import ar.edu.unrn.municipiopp.factories.FactoryDAO;
import ar.edu.unrn.municipiopp.models.Canje;
import ar.edu.unrn.municipiopp.models.Categoria;
import ar.edu.unrn.municipiopp.models.Ciudadano;
import ar.edu.unrn.municipiopp.models.Evento;
import ar.edu.unrn.municipiopp.models.Producto;
import ar.edu.unrn.municipiopp.models.Reclamo;

public class ApiImpl implements Api {
	private DAOInterface<Categoria> categoriaDAO = (CategoriaDAO) FactoryDAO.getInstance("CategoriaDAO");
	private DAOInterface<Producto> productoDAO = (ProductoDAO) FactoryDAO.getInstance("ProductoDAO");
	private DAOInterface<Ciudadano> ciudadanoDAO = (CiudadanoDAO) FactoryDAO.getInstance("CiudadanoDAO");
	private CanjeDAO canjeDAO = (CanjeDAO) FactoryDAO.getInstance("CanjeDAO");
	private ReclamoDAO reclamoDAO = (ReclamoDAO) FactoryDAO.getInstance("ReclamoDAO");
	private EventoDAO eventoDAO = (EventoDAO) FactoryDAO.getInstance("EventoDAO");

	/*----------------------------------------------------
	 | Categorías
	 |----------------------------------------------------
	 */
	@Override
	public void nuevaCategoria(String nombre, Integer puntaje) throws Exception {
		categoriaDAO.create(new Categoria(nombre, puntaje));
	}

	@Override
	public void bajaCategoria(String id) throws Exception {
		categoriaDAO.delete(id);
	}

	@Override
	public void modificarCategoria(String id, String nuevoNombre, Integer nuevoPuntaje) throws Exception {
		categoriaDAO.update(new Categoria(id, nuevoNombre, nuevoPuntaje));
	}

	@Override
	public List<CategoriaDTO> listarCategorias() throws Exception {
		List<Categoria> categorias = categoriaDAO.read();
		List<CategoriaDTO> categoriasDTO = new ArrayList<CategoriaDTO>();
		for (Categoria categoria : categorias) {
			categoriasDTO.add(new CategoriaDTO(categoria));
		}
		return categoriasDTO;
	}

	/*----------------------------------------------------
	 | Productos
	 |----------------------------------------------------
	 */
	@Override
	public void nuevoProducto(String nombre, Integer puntajeCosto) throws Exception {
		Producto producto = new Producto(nombre, puntajeCosto);
		productoDAO.create(producto);
	}

	@Override
	public void bajaProducto(String id) throws Exception {
		productoDAO.delete(id);
	}

	@Override
	public void modificarProducto(String id, String nuevoNombre, Integer nuevoPuntajeCosto) throws Exception {
		Producto producto = new Producto(id, null, null);
		producto.update(nuevoNombre, nuevoPuntajeCosto);
		productoDAO.update(producto);
	}

	@Override
	public List<ProductoDTO> listarProductos() throws Exception {
		List<Producto> productos = productoDAO.read();
		List<ProductoDTO> productosDTO = new ArrayList<ProductoDTO>();
		for (Producto producto : productos) {
			productosDTO.add(new ProductoDTO(producto));
		}
		return productosDTO;
	}

	/*----------------------------------------------------
	 | Ciudadanos
	 |----------------------------------------------------
	 */
	@Override
	public void nuevoCiudadano(String nombre, String apellido, String dni, String email) throws Exception {
		Ciudadano ciudadano = new Ciudadano(nombre, apellido, dni, email);
		ciudadanoDAO.create(ciudadano);
	}

	@Override
	public void bajaCiudadano(String id) throws Exception {
		ciudadanoDAO.delete(id);
	}

	@Override
	public void modificarCiudadano(String id, String nombre, String apellido, String email) throws Exception {
		Ciudadano ciudadano = new Ciudadano(id, null, null, null, null, null);
		ciudadano.update(nombre, apellido, email);
		ciudadanoDAO.update(ciudadano);
	}

	@Override
	public List<CiudadanoDTO> listarCiudadanos() throws Exception {
		List<Ciudadano> ciudadanos = ciudadanoDAO.read();
		List<CiudadanoDTO> ciudadanosDTO = new ArrayList<CiudadanoDTO>();
		for (Ciudadano ciudadano : ciudadanos) {
			ciudadanosDTO.add(new CiudadanoDTO(ciudadano));
		}
		return ciudadanosDTO;
	}

	@Override
	public List<CanjeDTO> listarCanjes(String idCiudadano) throws Exception {
		List<Canje> canjes = canjeDAO.filterByCiudadano(idCiudadano);
		List<CanjeDTO> canjesDTO = new ArrayList<CanjeDTO>();
		for (Canje canje : canjes) {
			canjesDTO.add(new CanjeDTO(canje));
		}
		return canjesDTO;
	}

	@Override
	public void canjear(String idCiudadano, String idProducto) throws Exception {
		Ciudadano ciudadano = ciudadanoDAO.read(idCiudadano);
		Producto producto = productoDAO.read(idProducto);
		ciudadano.canjear(producto);
		ciudadanoDAO.update(ciudadano);
	}

	/*----------------------------------------------------
	 | Reclamos y eventos
	 |----------------------------------------------------
	 */
	@Override
	public void nuevoReclamo(String idCiudadano, String idCategoria, String direccion, String detalle)
			throws Exception {
		Ciudadano ciudadano = ciudadanoDAO.read(idCiudadano);
		Categoria categoria = categoriaDAO.read(idCategoria);

		ciudadano.nuevoReclamo(direccion, detalle, categoria);
		ciudadanoDAO.update(ciudadano);
	}

	@Override
	public ReclamoDTO traerReclamo(String idReclamo) throws Exception {
		return new ReclamoDTO(reclamoDAO.read(idReclamo));
	}

	@Override
	public ReclamoDTO traerReclamoConEventos(String idReclamo) throws Exception {
		ReclamoDTO reclamoDTO = traerReclamo(idReclamo);
		reclamoDTO.setEventos(traerEventos(idReclamo));
		return reclamoDTO;
	}

	@Override
	public List<EventoDTO> traerEventos(String idReclamo) throws Exception {
		List<Evento> eventos = eventoDAO.filterByReclamo(idReclamo);
		List<EventoDTO> eventosDTO = new ArrayList<EventoDTO>();
		for (Evento evento : eventos) {
			eventosDTO.add(new EventoDTO(evento));
		}
		return eventosDTO;
	}

	@Override
	public void agregarEvento(String idReclamo, String descripcion) throws Exception {
		Reclamo reclamo = reclamoDAO.read(idReclamo);
		reclamo.agregarEvento(new Evento(descripcion, LocalDate.now(), reclamo));
		reclamoDAO.update(reclamo);
	}

}
