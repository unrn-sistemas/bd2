package ar.edu.unrn.municipiopp.models;

import java.util.UUID;

public abstract class GenericModel {
	protected String id;
	private Boolean dirt;

	/**
	 * Este método se invoca cuando la instancia creada no está en la BD.
	 */
	public GenericModel() {
		this.id = UUID.randomUUID().toString();
		this.dirt = true;
	}

	/**
	 * Este método se invoca cuando la instancia creada sí está en la BD.
	 */
	public GenericModel(String id) {
		if (id == null) {
			throw new RuntimeException("El ID no puede ser null.");
		}
		this.id = id;
		this.dirt = false;
	}

	public String getId() {
		return id;
	}

	public Boolean getDirt() {
		return dirt;
	}

	protected void update(Boolean dirt) {
		this.dirt = dirt;
	}

	protected void setDirt(Boolean dirt) {
		this.dirt = dirt;
	}
}
