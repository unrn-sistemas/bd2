package ar.edu.unrn.municipiopp.dao;

import java.sql.SQLException;
import java.util.List;

public interface DAOInterface<T> {
	/**
	 * 
	 * @param t
	 *            De tipo DTO.
	 * @return El ID del objeto.
	 * @throws SQLException
	 */
	void create(T t) throws SQLException;

	T read(String id) throws SQLException;

	List<T> read() throws SQLException;

	void update(T t) throws SQLException;

	void delete(String id) throws SQLException;
}
