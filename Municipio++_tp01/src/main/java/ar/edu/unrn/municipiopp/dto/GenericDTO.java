package ar.edu.unrn.municipiopp.dto;

public abstract class GenericDTO {
	protected String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
