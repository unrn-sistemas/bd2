package ar.edu.unrn.municipiopp.dto;

import java.time.LocalDate;

import ar.edu.unrn.municipiopp.models.Canje;

public class CanjeDTO extends GenericDTO {
	private String ciudadanoId;
	private String productoId;
	private LocalDate fecha;

	public CanjeDTO() {
	}

	public CanjeDTO(Canje canje) {
		this.id = canje.getId();
		this.ciudadanoId = canje.getCiudadano().getId();
		this.productoId = canje.getProducto().getId();
		this.fecha = canje.getFecha();
	}

	public String getCiudadanoId() {
		return ciudadanoId;
	}

	public void setCiudadanoId(String ciudadanoId) {
		this.ciudadanoId = ciudadanoId;
	}

	public String getProductoId() {
		return productoId;
	}

	public void setProductoId(String productoId) {
		this.productoId = productoId;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

}
