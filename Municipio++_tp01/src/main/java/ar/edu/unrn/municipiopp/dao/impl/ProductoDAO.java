package ar.edu.unrn.municipiopp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.municipiopp.models.Producto;

public class ProductoDAO extends GenericDAO<Producto> {
	// --------------------------------------------------
	// Métodos de la interface
	// --------------------------------------------------
	@Override
	public void create(Producto producto) throws SQLException {
		String insertSQL = "INSERT INTO productos (id, nombre, puntaje_costo) VALUES ";
		insertSQL += "(?, ?, ?)";
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement insertProducto = connection.prepareStatement(insertSQL);
			insertProducto.setString(1, producto.getId());
			insertProducto.setString(2, producto.getNombre());
			insertProducto.setInt(3, producto.getPuntajeCosto());
			insertProducto.executeUpdate();
		} catch (SQLException e) {
			if (e.getSQLState() == "23505") {
				throw new SQLException("El producto ya existe.", "23505");
			}
			throw e;
		}
	}

	@Override
	public Producto read(String id) throws SQLException {
		String checkSQL = "SELECT * FROM productos WHERE id = ?";
		Producto producto;
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement readSingleProducto = connection.prepareStatement(checkSQL);
			readSingleProducto.setString(1, id);
			ResultSet result = readSingleProducto.executeQuery();
			result.next();
			producto = new Producto(id, result.getString("nombre"), result.getInt("puntaje_costo"));
		} catch (SQLException e) {
			if (e.getSQLState() == "24000") {
				throw new SQLException("No existe un producto con id `" + id + "`.", "24000");
			}
			throw e;
		}

		return producto;
	}

	@Override
	public List<Producto> read() throws SQLException {
		String checkSQL = "SELECT * FROM productos";
		List<Producto> productos = new ArrayList<Producto>();
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement readAllProducto = connection.prepareStatement(checkSQL);
			ResultSet result = readAllProducto.executeQuery();
			while (result.next()) {
				Producto producto = new Producto(result.getString("id"), result.getString("nombre"),
						result.getInt("puntaje_costo"));
				productos.add(producto);
			}
		} catch (SQLException e) {
			throw e;
		}

		return productos;
	}

	@Override
	public void update(Producto producto) throws SQLException {
		String updateSQL = "UPDATE productos SET nombre = ?, puntaje_costo = ? WHERE id = ?";
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement updateProducto = connection.prepareStatement(updateSQL);
			updateProducto.setString(1, producto.getNombre());
			updateProducto.setInt(2, producto.getPuntajeCosto());
			updateProducto.setString(3, producto.getId());
			updateProducto.executeUpdate();
		} catch (SQLException e) {
			if (e.getSQLState() == "23505") {
				throw new SQLException("El producto ya existe.", "23505");
			}
			throw e;
		}
	}

	@Override
	public void delete(String id) throws SQLException {
		String updateSQL = "DELETE FROM productos WHERE id = ?";
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement deleteProducto = connection.prepareStatement(updateSQL);
			deleteProducto.setString(1, id);
			deleteProducto.executeUpdate();
		} catch (SQLException e) {
			if (e.getSQLState() == "24000") {
				throw new SQLException("No existe un producto con id `" + id + "`.", "24000");
			}
			throw e;
		}
	}
}
