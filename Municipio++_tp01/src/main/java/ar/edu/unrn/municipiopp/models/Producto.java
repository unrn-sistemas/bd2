package ar.edu.unrn.municipiopp.models;

public class Producto extends GenericModel {
	private Integer puntajeCosto;
	private String nombre;

	/**
	 * Constructor para instancias que están en la DB.
	 */
	public Producto(String id, String nombre, Integer puntajeCosto) {
		super(id);
		this.nombre = nombre;
		this.puntajeCosto = puntajeCosto;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Producto(String nombre, Integer puntajeCosto) {
		super();
		this.nombre = nombre;
		this.puntajeCosto = puntajeCosto;
	}

	/**
	 * Cualquier parámetro puede ser null.
	 * 
	 * @param nombre
	 *            nullable
	 * @param puntajeCosto
	 *            nullable
	 */
	public void update(String nombre, Integer puntajeCosto) {
		super.update(nombre != null || puntajeCosto != null);
		this.nombre = nombre == null ? this.nombre : nombre;
		this.puntajeCosto = puntajeCosto == null ? this.puntajeCosto : puntajeCosto;
	}

	public Producto() {
	}

	public String getNombre() {
		return nombre;
	}

	public Integer getPuntajeCosto() {
		return puntajeCosto;
	}

	public Boolean canjeable(Integer puntajeAcumulado) {
		return puntajeAcumulado >= this.puntajeCosto;
	}

}
