package ar.edu.unrn.municipiopp.models;

import java.time.LocalDate;

public class Evento extends GenericModel {
	private String descripcion;
	private LocalDate fecha;
	private Reclamo reclamo;

	/**
	 * Constructor para instancias existentes en la BD.
	 */
	public Evento(String id, String descripcion, LocalDate fecha, Reclamo reclamo) {
		super(id);
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.reclamo = reclamo;
	}

	/**
	 * Constructor para instancias que no están en la DB.
	 */
	public Evento(String descripcion, LocalDate fecha, Reclamo reclamo) {
		super();
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.reclamo = reclamo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public Reclamo getReclamo() {
		return reclamo;
	}
}
