package ar.edu.unrn.municipiopp.facade;

import java.util.List;

import ar.edu.unrn.municipiopp.dto.CanjeDTO;
import ar.edu.unrn.municipiopp.dto.CategoriaDTO;
import ar.edu.unrn.municipiopp.dto.CiudadanoDTO;
import ar.edu.unrn.municipiopp.dto.EventoDTO;
import ar.edu.unrn.municipiopp.dto.ProductoDTO;
import ar.edu.unrn.municipiopp.dto.ReclamoDTO;

public interface Api {
	/*----------------------------------------------------
	 | Categorías
	 |----------------------------------------------------
	 */
	void nuevaCategoria(String nombre, Integer puntaje) throws Exception;

	void bajaCategoria(String id) throws Exception;

	void modificarCategoria(String id, String nuevoNombre, Integer nuevoPuntaje) throws Exception;

	List<CategoriaDTO> listarCategorias() throws Exception;

	/*----------------------------------------------------
	 | Productos
	 |----------------------------------------------------
	 */
	void nuevoProducto(String nombre, Integer puntajeCosto) throws Exception;

	void bajaProducto(String id) throws Exception;

	void modificarProducto(String id, String nuevoNombre, Integer nuevoPuntajeCosto) throws Exception;

	List<ProductoDTO> listarProductos() throws Exception;

	/*----------------------------------------------------
	 | Ciudadanos
	 |----------------------------------------------------
	 */
	void nuevoCiudadano(String nombre, String apellido, String dni, String email) throws Exception;

	void bajaCiudadano(String id) throws Exception;

	void modificarCiudadano(String id, String nombre, String apellido, String email) throws Exception;

	List<CiudadanoDTO> listarCiudadanos() throws Exception;

	List<CanjeDTO> listarCanjes(String idCiudadano) throws Exception;

	void canjear(String idCiudadano, String idProducto) throws Exception;

	/*----------------------------------------------------
	 | Reclamos y eventos
	 |----------------------------------------------------
	 */
	void nuevoReclamo(String idCiudadano, String idCategoria, String direccion, String detalle) throws Exception;

	ReclamoDTO traerReclamo(String idReclamo) throws Exception;

	ReclamoDTO traerReclamoConEventos(String idReclamo) throws Exception;

	List<EventoDTO> traerEventos(String idReclamo) throws Exception;

	void agregarEvento(String idReclamo, String descripcion) throws Exception;
}
