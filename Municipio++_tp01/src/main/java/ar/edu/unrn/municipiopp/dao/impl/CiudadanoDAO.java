package ar.edu.unrn.municipiopp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.municipiopp.models.Canje;
import ar.edu.unrn.municipiopp.models.Ciudadano;
import ar.edu.unrn.municipiopp.models.Reclamo;

public class CiudadanoDAO extends GenericDAO<Ciudadano> {

	@Override
	public void create(Ciudadano ciudadano) throws SQLException {
		String insertSQL = "INSERT INTO ciudadanos (id, nombre, apellido, dni, email, puntaje_acumulado) VALUES ";
		insertSQL += "(?, ?, ?, ?, ?, ?)";
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement insertCiudadano = connection.prepareStatement(insertSQL);
			insertCiudadano.setString(1, ciudadano.getId());
			insertCiudadano.setString(2, ciudadano.getNombre());
			insertCiudadano.setString(3, ciudadano.getApellido());
			insertCiudadano.setString(4, ciudadano.getDni());
			insertCiudadano.setString(5, ciudadano.getEmail());
			insertCiudadano.setInt(6, ciudadano.getPuntajeAcumulado());
			insertCiudadano.executeUpdate();
		} catch (SQLException e) {
			if (e.getSQLState() == "23505") {
				throw new SQLException("El ciudadano ya existe.", e.getSQLState());
			}
			throw e;
		}
	}

	/**
	 * Este método siempre inserta el reclamo nuevo y actualiza el puntaje del
	 * usuario.
	 */
	private void create(Reclamo reclamo, Integer puntajeAcumulado) throws SQLException {

		String insertReclamoSQL = "INSERT INTO reclamos "
				+ "(id, fecha, direccion, detalle, categoria_id, ciudadano_id) VALUES ";
		insertReclamoSQL += "(?, ?, ?, ?, ?, ?)";
		String updateCiudadanoSQL = "UPDATE ciudadanos SET puntaje_acumulado = puntaje_acumulado + ? WHERE id = ?";
		try (Connection connection = DerbyConnection.localAccess()) {
			// Inserto el Reclamo
			connection.setAutoCommit(false);
			try (PreparedStatement insertReclamo = connection.prepareStatement(insertReclamoSQL);
					PreparedStatement updateCiudadano = connection.prepareStatement(updateCiudadanoSQL);) {
				insertReclamo.setString(1, reclamo.getId());
				insertReclamo.setString(2, reclamo.getFecha().toString());
				insertReclamo.setString(3, reclamo.getDireccion());
				insertReclamo.setString(4, reclamo.getDetalle());
				insertReclamo.setString(5, reclamo.getCategoria().getId());
				insertReclamo.setString(6, reclamo.getCiudadano().getId());
				insertReclamo.executeUpdate();

				// Si no hay errores, actualizo el Ciudadano
				updateCiudadano.setInt(1, puntajeAcumulado);
				updateCiudadano.setString(2, reclamo.getCiudadano().getId());
				updateCiudadano.executeUpdate();
			} catch (SQLException ex) {
				connection.rollback();
			}
			connection.commit();
		} catch (SQLException e) {
			if (e.getSQLState() == "23505") {
				throw new SQLException("El reclamo ya existe.", e.getSQLState());
			}
			throw e;
		}
	}

	/**
	 * Este update es llamado únicamente cuando se crea un nuevo Canje.
	 */
	private void create(Canje canje, Integer puntajeAcumulado) throws SQLException {
		String insertCanjeSQL = "INSERT INTO canjes ";
		insertCanjeSQL += "(id, fecha, ciudadano_id, producto_id) VALUES ";
		insertCanjeSQL += "(?, ?, ?, ?)";
		String updateCiudadanoSQL = "UPDATE ciudadanos SET puntaje_acumulado = puntaje_acumulado - ? WHERE id = ?";
		try (Connection connection = DerbyConnection.localAccess()) {
			connection.setAutoCommit(false);
			try (PreparedStatement insertCanje = connection.prepareStatement(insertCanjeSQL);
					PreparedStatement updateCiudadano = connection.prepareStatement(updateCiudadanoSQL);) {
				insertCanje.setString(1, canje.getId());
				insertCanje.setString(2, canje.getFecha().toString());
				insertCanje.setString(3, canje.getCiudadano().getId());
				insertCanje.setString(4, canje.getProducto().getId());
				insertCanje.executeUpdate();

				// Si no hay errores, actualizo el Ciudadano
				updateCiudadano.setInt(1, puntajeAcumulado);
				updateCiudadano.setString(2, canje.getCiudadano().getId());
			} catch (SQLException ex) {
				connection.rollback();
			}
			connection.commit();
		} catch (SQLException e) {
			if (e.getSQLState() == "23505") {
				throw new SQLException("El canje ya existe.", e.getSQLState());
			}
			throw e;
		}
	}

	@Override
	public Ciudadano read(String id) throws SQLException {
		String checkSQL = "SELECT * FROM ciudadanos WHERE id = ?";
		Ciudadano ciudadano;
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement readCiudadano = connection.prepareStatement(checkSQL);
			readCiudadano.setString(1, id);
			ResultSet result = readCiudadano.executeQuery();
			result.next();
			ciudadano = new Ciudadano(result.getString("id"), result.getString("nombre"), result.getString("apellido"),
					result.getString("dni"), result.getString("email"), result.getInt("puntaje_acumulado"));
		} catch (SQLException e) {
			if (e.getSQLState() == "24000") {
				throw new SQLException("No existe un ciudadano con id `" + id + "`.", e.getSQLState());
			}
			throw e;
		}

		return ciudadano;
	}

	@Override
	public List<Ciudadano> read() throws SQLException {
		String checkSQL = "SELECT * FROM ciudadanos";
		List<Ciudadano> ciudadanos = new ArrayList<Ciudadano>();
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement readAllCiudadano = connection.prepareStatement(checkSQL);
			ResultSet result = readAllCiudadano.executeQuery();
			while (result.next()) {
				Ciudadano ciudadano = new Ciudadano(result.getString("id"), result.getString("nombre"),
						result.getString("apellido"), result.getString("dni"), result.getString("email"),
						result.getInt("puntaje_acumulado"));
				ciudadanos.add(ciudadano);
			}
		} catch (SQLException e) {
			throw e;
		}

		return ciudadanos;
	}

	/**
	 * Este update update se encarga de revisar qué actualización interna del
	 * DAO debe invocar.
	 */
	@Override
	public void update(Ciudadano ciudadano) throws SQLException {
		if (ciudadano.getDirt()) {
			// Significa que el ciudadano fue el que se va a actualizar
			this.updateCiudadano(ciudadano);
			return;
		}
		try {
			Reclamo reclamo = ciudadano.getReclamos().get(ciudadano.getReclamos().size() - 1);
			if (reclamo.getDirt()) {
				// Significa que se agregó un nuevo Reclamo
				this.create(reclamo, ciudadano.getPuntajeAcumulado());
				return;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new SQLException("Antes de pedir un canje, primero realice reclamos.", "415");
		}
		// Significa que se agregó un nuevo Canje
		this.create(ciudadano.getCanjes().get(ciudadano.getCanjes().size() - 1), ciudadano.getPuntajeAcumulado());
	}

	/**
	 * Este update es para los datos propios del ciudadano únicamente.
	 * 
	 * No se puede actualizar el DNI.
	 * 
	 * No puede modificar los puntos a mano.
	 */
	private void updateCiudadano(Ciudadano ciudadano) throws SQLException {
		String updateSQL = "UPDATE ciudadanos SET nombre = ?, apellido = ?, " + "email = ? WHERE id = ?";
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement updateCiudadano = connection.prepareStatement(updateSQL);
			updateCiudadano.setString(1, ciudadano.getNombre());
			updateCiudadano.setString(2, ciudadano.getApellido());
			updateCiudadano.setString(3, ciudadano.getEmail());
			updateCiudadano.setString(4, ciudadano.getId());
			updateCiudadano.executeUpdate();
		} catch (SQLException e) {
			if (e.getSQLState() == "23505") {
				throw new SQLException("El correo ingresado ya está en uso.", e.getSQLState());
			}
			throw e;
		}
	}

	@Override
	public void delete(String id) throws SQLException {
		String updateSQL = "DELETE FROM ciudadanos WHERE id = ?";
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement deleteCiudadano = connection.prepareStatement(updateSQL);
			deleteCiudadano.setString(1, id);
			deleteCiudadano.executeUpdate();
		} catch (SQLException e) {
			if (e.getSQLState() == "24000") {
				throw new SQLException("No existe un ciudadano con id `" + id + "`.", e.getSQLState());
			}
			if (e.getSQLState() == "23503") {
				throw new SQLException(
						"El Ciudadano no puede ser eliminado debido a que existen objetos que dependen de él. Primero bórre esos objetos.",
						e.getSQLState());
			}
			throw e;
		}
	}

}
