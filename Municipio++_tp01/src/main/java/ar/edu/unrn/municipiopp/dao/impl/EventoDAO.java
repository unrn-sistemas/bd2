package ar.edu.unrn.municipiopp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.municipiopp.models.Evento;
import ar.edu.unrn.municipiopp.models.Reclamo;

public class EventoDAO extends GenericDAO<Evento> {

	// --------------------------------------------------
	// Métodos de la interface
	// --------------------------------------------------
	@Override
	public void create(Evento evento) throws SQLException {
		throw new SQLException("Esta clase no tiene la responsabilidad de crear una instancia de Canje.", "404");
	}

	@Override
	public Evento read(String id) throws SQLException {
		String checkSQL = "SELECT * FROM eventos WHERE id = ?";
		Evento evento;
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement readEvento = connection.prepareStatement(checkSQL);
			readEvento.setString(1, id);
			ResultSet result = readEvento.executeQuery();
			result.next();
			evento = new Evento(result.getString("id"), result.getString("descripcion"),
					LocalDate.parse(result.getString("fecha")),
					new Reclamo(result.getString("reclamo_id"), null, null, null, null, null));
		} catch (SQLException e) {
			if (e.getSQLState() == "24000") {
				throw new SQLException("No existe un evento con id `" + id + "`.", "24000");
			}
			throw e;
		}

		return evento;
	}

	@Override
	public List<Evento> read() throws SQLException {
		String checkSQL = "SELECT * FROM eventos";
		List<Evento> eventos = new ArrayList<Evento>();
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement readAllEventos = connection.prepareStatement(checkSQL);
			ResultSet result = readAllEventos.executeQuery();
			while (result.next()) {
				Evento evento = new Evento(result.getString("id"), result.getString("descripcion"),
						LocalDate.parse(result.getString("fecha")),
						new Reclamo(result.getString("reclamo_id"), null, null, null, null, null));
				eventos.add(evento);
			}
		}

		return eventos;
	}

	@Override
	public void update(Evento evento) throws SQLException {
		throw new SQLException("Los eventos son de sólo lectura.", "3906");
	}

	@Override
	public void delete(String id) throws SQLException {
		String updateSQL = "DELETE FROM eventos WHERE id = ?";
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement deleteEvento = connection.prepareStatement(updateSQL);
			deleteEvento.setString(1, id);
			deleteEvento.executeUpdate();
		} catch (SQLException e) {
			if (e.getSQLState() == "24000") {
				throw new SQLException("No existe un evento con id `" + id + "`.", "24000");
			}
			throw e;
		}
	}

	public List<Evento> filterByReclamo(String idReclamo) throws SQLException {
		String checkSQL = "SELECT * FROM eventos WHERE reclamo_id = ?";
		List<Evento> eventos = new ArrayList<Evento>();
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement readAllEventos = connection.prepareStatement(checkSQL);
			readAllEventos.setString(1, idReclamo);
			ResultSet result = readAllEventos.executeQuery();
			while (result.next()) {
				Evento evento = new Evento(result.getString("id"), result.getString("descripcion"),
						LocalDate.parse(result.getString("fecha")),
						new Reclamo(result.getString("reclamo_id"), null, null, null, null, null));
				eventos.add(evento);
			}
		} catch (SQLException e) {
			if (e.getSQLState() == "24000") {
				throw new SQLException("No existen eventos con reclamo_id `" + idReclamo + "`.", "24000");
			}
			throw e;
		}

		return eventos;
	}

}
