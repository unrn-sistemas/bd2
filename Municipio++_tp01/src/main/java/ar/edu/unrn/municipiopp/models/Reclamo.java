package ar.edu.unrn.municipiopp.models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Reclamo extends GenericModel {
	private LocalDate fecha;
	private String direccion;
	private String detalle;
	private Ciudadano ciudadano;
	private Categoria categoria;
	private List<Evento> eventos = new ArrayList<Evento>();

	/**
	 * Para instancias ya existentes en la DB.
	 */
	public Reclamo(String id, LocalDate fecha, String direccion, String detalle, Ciudadano ciudadano,
			Categoria categoria) {
		super(id);
		this.fecha = fecha;
		this.direccion = direccion;
		this.detalle = detalle;
		this.categoria = categoria;
		this.ciudadano = ciudadano;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Reclamo(LocalDate fecha, String direccion, String detalle, Ciudadano ciudadano, Categoria categoria) {
		super();
		this.fecha = fecha;
		this.direccion = direccion;
		this.detalle = detalle;
		this.categoria = categoria;
		this.ciudadano = ciudadano;
	}

	// ---------------------------------------------------
	// Reglas del negocio
	// ---------------------------------------------------
	public void agregarEvento(Evento evento) {
		eventos.add(evento);
	}

	// ---------------------------------------------------
	// Getters
	// ---------------------------------------------------
	public LocalDate getFecha() {
		return fecha;
	}

	public String getDireccion() {
		return direccion;
	}

	public String getDetalle() {
		return detalle;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public Ciudadano getCiudadano() {
		return ciudadano;
	}

	public List<Evento> getEventos() {
		return eventos;
	}
}
