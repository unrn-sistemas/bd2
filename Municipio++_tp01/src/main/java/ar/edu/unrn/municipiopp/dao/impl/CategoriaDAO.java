package ar.edu.unrn.municipiopp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.municipiopp.models.Categoria;

public class CategoriaDAO extends GenericDAO<Categoria> {
	// --------------------------------------------------
	// Métodos de la interface
	// --------------------------------------------------
	@Override
	public void create(Categoria categoria) throws SQLException {
		String insertSQL = "INSERT INTO categorias (id, nombre, puntaje) VALUES ";
		insertSQL += "(?, ?, ?)";
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement insertCategoria = connection.prepareStatement(insertSQL);
			insertCategoria.setString(1, categoria.getId());
			insertCategoria.setString(2, categoria.getNombre());
			insertCategoria.setInt(3, categoria.getPuntaje());
			insertCategoria.executeUpdate();
		} catch (SQLException e) {
			if (e.getSQLState() == "23505") {
				throw new SQLException("La categoría ya existe.", "23505");
			}
			throw e;
		}
	}

	@Override
	public Categoria read(String id) throws SQLException {
		String checkSQL = "SELECT * FROM categorias WHERE id = ?";
		Categoria categoria = null;
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement readCategoria = connection.prepareStatement(checkSQL);
			readCategoria.setString(1, id);
			ResultSet result = readCategoria.executeQuery();
			result.next();
			categoria = new Categoria(id, result.getString("nombre"), result.getInt("puntaje"));
		} catch (SQLException e) {
			if (e.getSQLState() == "24000") {
				throw new SQLException("No existe una categoría con id `" + id + "`.", "24000");
			}
			throw e;
		}

		return categoria;
	}

	@Override
	public List<Categoria> read() throws SQLException {
		String checkSQL = "SELECT * FROM categorias";
		List<Categoria> categorias = new ArrayList<Categoria>();
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement readAllCategoria = connection.prepareStatement(checkSQL);
			ResultSet result = readAllCategoria.executeQuery();
			while (result.next()) {
				Categoria categoria = new Categoria(result.getString("id"), result.getString("nombre"),
						result.getInt("puntaje"));
				categorias.add(categoria);
			}
		} catch (SQLException e) {
			throw e;
		}

		return categorias;
	}

	@Override
	public void update(Categoria categoria) throws SQLException {
		String updateSQL = "UPDATE categorias SET nombre = ?, puntaje = ? WHERE id = ?";
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement updateCategoria = connection.prepareStatement(updateSQL);
			updateCategoria.setString(1, categoria.getNombre());
			updateCategoria.setInt(2, categoria.getPuntaje());
			updateCategoria.setString(3, categoria.getId());
			updateCategoria.executeUpdate();
		} catch (SQLException e) {
			if (e.getSQLState() == "23505") {
				throw new SQLException("La categoría ya existe.", e.getSQLState());
			}
			throw e;
		}
	}

	@Override
	public void delete(String id) throws SQLException {
		String updateSQL = "DELETE FROM categorias WHERE id = ?";
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement deleteCategoria = connection.prepareStatement(updateSQL);
			deleteCategoria.setString(1, id);
			Integer result = deleteCategoria.executeUpdate();
			if (result == 0) {
				throw new SQLException("No existe una categoría con id `" + id + "`.", "24000");
			}
		} catch (SQLException e) {
			if (e.getSQLState() == "24000") {
				throw new SQLException("No existe una categoría con id `" + id + "`.", "24000");
			}
			if (e.getSQLState() == "23503") {
				throw new SQLException(
						"La Categoría no puede ser eliminada debido a que existen objetos que dependen de ella. Primero bórre esos objetos.",
						e.getSQLState());
			}
			throw e;
		}
	}
}
