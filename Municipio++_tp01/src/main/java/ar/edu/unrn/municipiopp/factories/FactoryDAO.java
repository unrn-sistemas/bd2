package ar.edu.unrn.municipiopp.factories;

import java.util.Hashtable;
import java.util.ResourceBundle;

public class FactoryDAO {
	private static Hashtable<String, Object> instances = new Hashtable<String, Object>();

	public static Object getInstance(String objectName) throws RuntimeException {
		try {
			Object object = instances.get(objectName);

			if (object == null) {
				ResourceBundle factoryProperties = ResourceBundle.getBundle("factory");
				String objectClassName = factoryProperties.getString(objectName);
				object = Class.forName(objectClassName).newInstance();

				instances.put(objectName, object);
			}
			return object;
		} catch (Exception e) {
			throw new RuntimeException("No existe una clase con ese nombre.");
		}
	}
}
