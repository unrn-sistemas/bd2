package ar.edu.unrn.municipiopp.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.municipiopp.models.Evento;
import ar.edu.unrn.municipiopp.models.Reclamo;

public class ReclamoDTO extends GenericDTO {
	private LocalDate fecha;
	private String direccion;
	private String detalle;
	private String categoriaId;
	private String ciudadanoId;
	private List<EventoDTO> eventos;

	public ReclamoDTO() {
	}

	public ReclamoDTO(Reclamo reclamo) {
		this.id = reclamo.getId();
		this.fecha = reclamo.getFecha();
		this.direccion = reclamo.getDireccion();
		this.detalle = reclamo.getDetalle();
		this.categoriaId = reclamo.getCategoria().getId();
		this.ciudadanoId = reclamo.getCiudadano().getId();
		this.eventos = new ArrayList<EventoDTO>();
		List<Evento> eventos = reclamo.getEventos();
		if (eventos != null)
			for (Evento evento : eventos) {
				this.eventos.add(new EventoDTO(evento));
			}
	}

	public List<EventoDTO> getEventos() {
		return eventos;
	}

	public void setEventos(List<EventoDTO> eventos) {
		this.eventos = eventos;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public String getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(String categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getCiudadanoId() {
		return ciudadanoId;
	}

	public void setCiudadanoId(String ciudadanoId) {
		this.ciudadanoId = ciudadanoId;
	}

}
