package ar.edu.unrn.municipiopp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.municipiopp.models.Categoria;
import ar.edu.unrn.municipiopp.models.Ciudadano;
import ar.edu.unrn.municipiopp.models.Evento;
import ar.edu.unrn.municipiopp.models.Reclamo;

public class ReclamoDAO extends GenericDAO<Reclamo> {
	// --------------------------------------------------
	// Métodos de la interface
	// --------------------------------------------------
	@Override
	public void create(Reclamo reclamo) throws SQLException {
		throw new SQLException("Esta clase no tiene la responsabilidad de crear una instancia de Reclamo.", "404");
	}

	/**
	 * Los objetos de tipo Evento son responsabilidad del Reclamo. Es por ello
	 * que son éstos que deben crearlos.
	 */
	private void create(Evento evento) throws SQLException {
		String insertSQL = "INSERT INTO eventos (id, descripcion, fecha, reclamo_id) VALUES ";
		insertSQL += "(?, ?, ?, ?)";
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement insertEvento = connection.prepareStatement(insertSQL);
			insertEvento.setString(1, evento.getId());
			insertEvento.setString(2, evento.getDescripcion());
			insertEvento.setString(3, evento.getFecha().toString());
			insertEvento.setString(4, evento.getReclamo().getId());
			insertEvento.executeUpdate();
		} catch (SQLException e) {
			if (e.getSQLState() == "23505") {
				throw new SQLException("El evento ya existe.", "23505");
			}
			throw e;
		}
	}

	@Override
	public Reclamo read(String id) throws SQLException {
		String checkSQL = "SELECT * FROM reclamos WHERE id = ?";
		Reclamo reclamo;
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement readReclamo = connection.prepareStatement(checkSQL);
			readReclamo.setString(1, id);
			ResultSet result = readReclamo.executeQuery();
			result.next();
			reclamo = new Reclamo(id, LocalDate.parse(result.getString("fecha")), result.getString("direccion"),
					result.getString("detalle"),
					new Ciudadano(result.getString("ciudadano_id"), null, null, null, null, null),
					new Categoria(result.getString("categoria_id"), null, null));
		} catch (SQLException e) {
			if (e.getSQLState() == "24000") {
				throw new SQLException("No existe un reclamo con id `" + id + "`.", "24000");
			}
			throw e;
		}

		return reclamo;
	}

	@Override
	public List<Reclamo> read() throws SQLException {
		String checkSQL = "SELECT * FROM reclamos";
		List<Reclamo> reclamos = new ArrayList<Reclamo>();
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement readAllReclamo = connection.prepareStatement(checkSQL);
			ResultSet result = readAllReclamo.executeQuery();
			while (result.next()) {
				Reclamo reclamo = new Reclamo(result.getString("id"), LocalDate.parse(result.getString("fecha")),
						result.getString("direccion"), result.getString("detalle"),
						new Ciudadano(result.getString("ciudadano_id"), null, null, null, null, null),
						new Categoria(result.getString("categoria_id"), null, null));
				reclamos.add(reclamo);
			}
		}

		return reclamos;
	}

	public List<Reclamo> read(String ciudadano_id, String categoria_id) throws SQLException {
		String checkSQL = "SELECT * FROM reclamos WHERE NULLIF(ciudadano_id,'') = ? AND NULLIF(categoria_id,'') = ?";
		List<Reclamo> reclamos = new ArrayList<Reclamo>();
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement readAllReclamo = connection.prepareStatement(checkSQL);
			readAllReclamo.setString(1, ciudadano_id == null ? "" : ciudadano_id);
			readAllReclamo.setString(2, categoria_id == null ? "" : categoria_id);

			ResultSet result = readAllReclamo.executeQuery();
			while (result.next()) {
				Reclamo reclamo = new Reclamo(result.getString("id"), LocalDate.parse(result.getString("fecha")),
						result.getString("direccion"), result.getString("detalle"),
						new Ciudadano(result.getString("ciudadano_id"), null, null, null, null, null),
						new Categoria(result.getString("categoria_id"), null, null));
				reclamos.add(reclamo);
			}
		}

		return reclamos;
	}

	@Override
	public void update(Reclamo reclamo) throws SQLException {
		try {
			Evento evento = reclamo.getEventos().get(reclamo.getEventos().size() - 1);
			if (evento.getDirt()) {
				this.create(evento);
				return;
			}
		} catch (Exception e) {
			// Error porque no existen eventos en los reclamos, o sea, se
			// intentó modificar el reclamo.
		}
		// El reclamo no puede modificarse
		throw new SQLException("No se pueden editar los reclamos.", "3905");
	}

	@Override
	public void delete(String id) throws SQLException {
		String updateSQL = "DELETE FROM reclamos WHERE id = ?";
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement deleteReclamo = connection.prepareStatement(updateSQL);
			deleteReclamo.setString(1, id);
			deleteReclamo.executeUpdate();
		} catch (SQLException e) {
			if (e.getSQLState() == "24000") {
				throw new SQLException("No existe un reclamo con id `" + id + "`.", "24000");
			}
			throw e;
		}
	}

}
