package ar.edu.unrn.municipiopp.models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Ciudadano extends GenericModel {
	private String nombre;
	private String apellido;
	private String dni;
	private String email;
	private Integer puntajeAcumulado;
	private List<Reclamo> reclamos = new ArrayList<Reclamo>();
	private List<Canje> canjes = new ArrayList<Canje>();

	/**
	 * Constructor para instancias existentes en la BD.
	 * 
	 * No sirve para actualizar instancias existentes.
	 */
	public Ciudadano(String id, String nombre, String apellido, String dni, String email, Integer puntajeAcumulado) {
		super(id);
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.email = email;
		this.puntajeAcumulado = puntajeAcumulado;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Ciudadano(String nombre, String apellido, String dni, String email) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.email = email;
		this.puntajeAcumulado = 0;
	}

	// ---------------------------------------------------
	// Reglas del negocio
	// ---------------------------------------------------
	public void canjear(Producto producto) throws Exception {
		// Si el mes no es septiembre, se evalúa de forma normal. Sino, es
		// gratis.
		if (LocalDate.now().getMonthValue() != 9) {
			if (!producto.canjeable(this.puntajeAcumulado)) {
				throw new Exception("No tiene puntos suficientes para canjear el producto.");
			}
			this.puntajeAcumulado -= producto.getPuntajeCosto();
		}
		canjes.add(new Canje(LocalDate.now(), this, producto));
	}

	public void nuevoReclamo(String direccion, String detalle, Categoria categoria) {
		Reclamo reclamo = new Reclamo(LocalDate.now(), direccion, detalle, this, categoria);
		this.reclamos.add(reclamo);
		// Acumulo a mi puntaje actual, el puntaje de la Categoría del nuevo
		// Reclamo. Y si es septiembre, lo multiplico.
		this.puntajeAcumulado += LocalDate.now().getMonthValue() != 9 ? categoria.getPuntaje()
				: categoria.getPuntaje() * 2;
	}

	/**
	 * Cualquier parámetro puede ser null.
	 * 
	 * @param nombre
	 *            nullable
	 * @param apellido
	 *            nullable
	 * @param email
	 *            nullable
	 */
	public void update(String nombre, String apellido, String email) {
		super.update(nombre != null || apellido != null || email != null);
		this.nombre = nombre == null ? this.nombre : nombre;
		this.apellido = apellido == null ? this.apellido : apellido;
		this.email = email == null ? this.email : email;
	}

	// ---------------------------------------------------
	// Getters
	// ---------------------------------------------------
	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getDni() {
		return dni;
	}

	public String getEmail() {
		return email;
	}

	public Integer getPuntajeAcumulado() {
		return puntajeAcumulado;
	}

	public List<Reclamo> getReclamos() {
		return reclamos;
	}

	public List<Canje> getCanjes() {
		return canjes;
	}
}
