package ar.edu.unrn.municipiopp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.municipiopp.models.Canje;
import ar.edu.unrn.municipiopp.models.Ciudadano;
import ar.edu.unrn.municipiopp.models.Producto;

public class CanjeDAO extends GenericDAO<Canje> {
	// --------------------------------------------------
	// Métodos de la interface
	// --------------------------------------------------
	@Override
	public void create(Canje canje) throws SQLException {
		throw new SQLException("Esta clase no tiene la responsabilidad de crear una instancia de Canje.", "404");
	}

	public List<Canje> filterByCiudadano(String idCiudadano) throws SQLException {
		String checkSQL = "SELECT * FROM canjes WHERE ciudadano_id = ?";
		List<Canje> canjes = new ArrayList<Canje>();
		try (Connection connection = DerbyConnection.localAccess()) {
			PreparedStatement readAllCanje = connection.prepareStatement(checkSQL);
			readAllCanje.setString(1, idCiudadano);
			ResultSet result = readAllCanje.executeQuery();
			while (result.next()) {
				Canje categoria = new Canje(result.getString("id"), LocalDate.parse(result.getString("fecha")),
						new Ciudadano(result.getString("ciudadano_id"), null, null, null, null, null),
						new Producto(result.getString("producto_id"), null, null));
				canjes.add(categoria);
			}
		}
		return canjes;
	}

	@Override
	public Canje read(String id) throws SQLException {
		throw new SQLException("No se pueden leer ni editar los canjes.", "3905");
	}

	@Override
	public List<Canje> read() throws SQLException {
		throw new SQLException("No se pueden leer ni editar los canjes.", "3905");
	}

	@Override
	public void update(Canje canje) throws SQLException {
		throw new SQLException("No se pueden leer ni editar los canjes.", "3905");
	}

	@Override
	public void delete(String id) throws SQLException {
		throw new SQLException("No se pueden leer ni editar los canjes.", "3905");
	}

}
