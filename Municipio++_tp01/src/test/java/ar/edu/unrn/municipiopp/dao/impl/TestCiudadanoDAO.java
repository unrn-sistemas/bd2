package ar.edu.unrn.municipiopp.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ar.edu.unrn.municipiopp.models.Ciudadano;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCiudadanoDAO {
	private CiudadanoDAO ciudadanoDAO = new CiudadanoDAO();
	private Ciudadano ciudadano;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		createTable();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		deleteTable();
	}

	@Before
	public void setUp() throws Exception {
		ciudadano = new Ciudadano("Luciano", "Graziani", "35591234", "lgraziani2712@gmail.com");
	}

	@Test
	public void a_testCreate() {
		try {
			ciudadanoDAO.create(ciudadano);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.assertEquals("23505", e.getSQLState());
		}
	}

	@Test
	public void b_testRead() {
		try {
			// read() method test
			List<Ciudadano> ciudadanos = ciudadanoDAO.read();
			Assert.assertTrue(ciudadanos.size() > 0);

			// read(id) method test
			Ciudadano ciudadano = ciudadanoDAO.read(ciudadanos.get(0).getId());
			Assert.assertEquals(ciudadanos.get(0).getId(), ciudadano.getId());

			ciudadanoDAO.read("asd");
		} catch (SQLException e) {
			if (e.getSQLState() != "24000")
				e.printStackTrace();
			Assert.assertEquals("24000", e.getSQLState());
		}
	}

	@Test
	public void c_testUpdate() {
		try {
			List<Ciudadano> ciudadanos = ciudadanoDAO.read();
			Ciudadano ciudadano = ciudadanos.get(0);
			ciudadano.update("dfhskdjh3487", null, null);
			ciudadanoDAO.update(ciudadano);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.assertEquals("23505", e.getSQLState());
		}
	}

	@Test
	public void d_testDelete() {
		try {
			List<Ciudadano> ciudadanos = ciudadanoDAO.read();
			Ciudadano ciudadano = ciudadanos.get(0);
			ciudadanoDAO.delete(ciudadano.getId());

			ciudadanoDAO.delete("asd");
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.assertEquals("24000", e.getSQLState());
		}
	}

	// -------------------------------------------------------
	// Table creation/delete methods
	// -------------------------------------------------------

	public static void createTable() {
		try (Connection connection = DerbyConnection.localAccess();
				Statement statement = connection.createStatement();) {
			String createCiudadanoTable = "CREATE TABLE ciudadanos " + "(id CHAR(36) PRIMARY KEY, nombre VARCHAR(22), "
					+ "apellido VARCHAR(22), dni CHAR(8), email VARCHAR(36), " + "puntaje_acumulado INT)";

			// Devuelve falso cuando el primer resultado no es un ResulSet. En
			// este caso, no lo es, por eso se espera que sea falso.
			Assert.assertFalse(statement.execute(createCiudadanoTable));
		} catch (SQLException e) {
			if (e.getErrorCode() != 30000) {
				e.printStackTrace();
			}
			Assert.assertEquals(30000, e.getErrorCode());
		}
	}

	public static void deleteTable() {
		try (Connection connection = DerbyConnection.localAccess();
				Statement statement = connection.createStatement();) {
			String deleteCiudadanoTable = "DROP TABLE ciudadanos";

			// Devuelve falso cuando el primer resultado no es un ResulSet. En
			// este caso, no lo es, por eso se espera que sea falso.
			Assert.assertFalse(statement.execute(deleteCiudadanoTable));
		} catch (SQLException e) {
			if (e.getErrorCode() != 30000) {
				e.printStackTrace();
			}
			Assert.assertEquals(30000, e.getErrorCode());
		}
	}
}
