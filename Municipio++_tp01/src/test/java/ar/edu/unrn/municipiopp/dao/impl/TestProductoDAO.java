package ar.edu.unrn.municipiopp.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ar.edu.unrn.municipiopp.models.Producto;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestProductoDAO {
	private ProductoDAO productoDAO = new ProductoDAO();
	private Producto producto;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		createTable();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		deleteTable();
	}

	@Before
	public void setUp() throws Exception {
		producto = new Producto("lkhjkghjiufty4", 99999);
	}

	@Test
	public void a_testCreate() {
		try {
			productoDAO.create(producto);
		} catch (SQLException e) {
			Assert.assertEquals("23505", e.getSQLState());
		}
	}

	@Test
	public void b_testReads() {
		try {
			// read() method test
			List<Producto> productos = productoDAO.read();
			Assert.assertTrue(productos.size() > 0);

			// read(id) method test
			Producto producto = productoDAO.read(productos.get(0).getId());
			Assert.assertEquals(productos.get(0).getId(), producto.getId());

			productoDAO.read("asd");
		} catch (SQLException e) {
			if (e.getSQLState() != "24000")
				e.printStackTrace();
			Assert.assertEquals("24000", e.getSQLState());
		}
	}

	@Test
	public void c_testUpdate() {
		try {
			List<Producto> productos = productoDAO.read();
			Producto producto = productos.get(0);
			producto.update("dfhskdjh3487", null);
			productoDAO.update(producto);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.assertEquals("23505", e.getSQLState());
		}
	}

	@Test
	public void d_testDelete() {
		try {
			List<Producto> productos = productoDAO.read();
			Producto producto = productos.get(0);
			productoDAO.delete(producto.getId());

			productoDAO.delete("asd");
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.assertEquals("24000", e.getSQLState());
		}
	}

	// -------------------------------------------------------
	// Table creation/delete methods
	// -------------------------------------------------------

	public static void createTable() {
		try (Connection connection = DerbyConnection.localAccess();
				Statement statement = connection.createStatement();) {
			String createProductoTable = "CREATE TABLE productos "
					+ "(id CHAR(36) PRIMARY KEY, nombre VARCHAR(22) UNIQUE, puntaje_costo INT)";

			// Devuelve falso cuando el primer resultado no es un ResulSet. En
			// este caso, no lo es, por eso se espera que sea falso.
			Assert.assertFalse(statement.execute(createProductoTable));
		} catch (SQLException e) {
			if (e.getErrorCode() != 30000) {
				e.printStackTrace();
			}
			Assert.assertEquals(30000, e.getErrorCode());
		}
	}

	public static void deleteTable() {
		try (Connection connection = DerbyConnection.localAccess();
				Statement statement = connection.createStatement();) {
			String deleteProductoTable = "DROP TABLE productos";

			// Devuelve falso cuando el primer resultado no es un ResulSet. En
			// este caso, no lo es, por eso se espera que sea falso.
			Assert.assertFalse(statement.execute(deleteProductoTable));
		} catch (SQLException e) {
			if (e.getErrorCode() != 30000) {
				e.printStackTrace();
			}
			Assert.assertEquals(30000, e.getErrorCode());
		}
	}
}
