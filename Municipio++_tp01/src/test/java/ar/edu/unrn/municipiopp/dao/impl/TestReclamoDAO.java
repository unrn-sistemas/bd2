package ar.edu.unrn.municipiopp.dao.impl;

import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ar.edu.unrn.municipiopp.models.Categoria;
import ar.edu.unrn.municipiopp.models.Ciudadano;
import ar.edu.unrn.municipiopp.models.Reclamo;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestReclamoDAO {
	private ReclamoDAO reclamoDAO = new ReclamoDAO();
	private static CategoriaDAO categoriaDAO = new CategoriaDAO();
	private static CiudadanoDAO ciudadanoDAO = new CiudadanoDAO();
	private static Reclamo reclamo;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		createTables();
		Categoria categoria = new Categoria("akjfhsd", 999);
		Ciudadano ciudadano = new Ciudadano("akjfhsd", "sdjfgd", "jkdshfs", "askdjshas");
		categoriaDAO.create(categoria);
		ciudadanoDAO.create(ciudadano);
		reclamo = new Reclamo(LocalDate.now(), "asldhja", "slajdhas", ciudadano, categoria);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		deleteTables();
	}

	@Before
	public void setUp() throws Exception {
		Categoria categoria = categoriaDAO.read().get(0);
		Ciudadano ciudadano = ciudadanoDAO.read().get(0);
		ciudadano.nuevoReclamo("jhfsdkjfh 2134", "sldfjhkfdjh skdjfh skdjfh skdjfs hdkifh skdfjhsdkfhsk dfjhs d.",
				categoria);
		ciudadanoDAO.update(ciudadano);
	}

	@Test
	public void a_testCreate() {
		try {
			reclamoDAO.create(reclamo);
		} catch (SQLException e) {
			if (e.getSQLState() != "404") {
				e.printStackTrace();
				return;
			}
			Assert.assertTrue(true);
		}
	}

	@Test
	public void b_testRead() {
		try {
			// read() method test
			List<Reclamo> reclamos = reclamoDAO.read();
			Assert.assertTrue(reclamos.size() > 0);

			// read(id) method test
			Reclamo reclamo = reclamoDAO.read(reclamos.get(0).getId());
			Assert.assertEquals(reclamos.get(0).getCategoria().getId(), reclamo.getCategoria().getId());
			Assert.assertEquals(reclamos.get(0).getCiudadano().getId(), reclamo.getCiudadano().getId());

			reclamoDAO.read("asd");
		} catch (SQLException e) {
			if (e.getSQLState() != "24000")
				e.printStackTrace();
			Assert.assertEquals("24000", e.getSQLState());
		}
	}

	// @Test
	public void c_testReadStringString() {
		fail("Not yet implemented");
	}

	@Test
	public void d_testUpdate() {
		try {
			reclamoDAO.update(reclamo);
		} catch (SQLException e) {
			if (e.getSQLState() != "3905")
				e.printStackTrace();
			Assert.assertEquals("3905", e.getSQLState());
		}
	}

	@Test
	public void e_testDelete() {
		try {
			List<Reclamo> reclamos = reclamoDAO.read();
			Reclamo reclamo = reclamos.get(0);
			reclamoDAO.delete(reclamo.getId());

			reclamoDAO.delete("asd");
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.assertEquals("24000", e.getSQLState());
		}
	}

	// -------------------------------------------------------
	// Table creation/delete methods
	// -------------------------------------------------------
	public static void createTables() {
		TestCategoriaDAO.createTable();
		TestCiudadanoDAO.createTable();
		createTable();
	}

	public static void createTable() {
		try (Connection connection = DerbyConnection.localAccess();
				Statement statement = connection.createStatement();) {
			String createReclamoTable = "CREATE TABLE reclamos " + "(id CHAR(36) PRIMARY KEY, " + "fecha DATE, "
					+ "direccion VARCHAR(40), " + "detalle LONG VARCHAR, " + "categoria_id CHAR(36), "
					+ "ciudadano_id CHAR(36), " + "FOREIGN KEY (categoria_id) REFERENCES categorias(id), "
					+ "FOREIGN KEY (ciudadano_id) REFERENCES ciudadanos(id)" + ")";

			// Devuelve falso cuando el primer resultado no es un ResulSet. En
			// este caso, no lo es, por eso se espera que sea falso.
			Assert.assertFalse(statement.execute(createReclamoTable));
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.assertEquals(30000, e.getErrorCode());
		}
	}

	public static void deleteTables() {
		deleteTable();
		TestCategoriaDAO.deleteTable();
		TestCiudadanoDAO.deleteTable();
	}

	public static void deleteTable() {
		try (Connection connection = DerbyConnection.localAccess();
				Statement statement = connection.createStatement();) {
			String deleteReclamoTable = "DROP TABLE reclamos";
			Assert.assertFalse(statement.execute(deleteReclamoTable));

			// Devuelve falso cuando el primer resultado no es un ResulSet. En
			// este caso, no lo es, por eso se espera que sea falso.
		} catch (SQLException e) {
			if (e.getErrorCode() != 30000) {
				e.printStackTrace();
			}
			Assert.assertEquals(30000, e.getErrorCode());
		}
	}
}
