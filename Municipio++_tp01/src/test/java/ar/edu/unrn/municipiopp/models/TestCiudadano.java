package ar.edu.unrn.municipiopp.models;

import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class TestCiudadano {
	private Integer puntosAcumulados;
	private Producto producto;

	//@Steps
	private TestCiudadanoSteps testCiudadanoSteps;

	@Given("una cantidad de puntosAcumulados")
	@Alias("una cantidad de $puntosAcumulados")
	public void puntosAcumulados(Integer puntosAcumulados) {
		this.puntosAcumulados = puntosAcumulados;
	}

	@When("se selecciona un Producto de costo, para canjear")
	@Alias("se selecciona un Producto de $costo, para canjear")
	public void seleccionarUnProducto(Integer costo) {
		producto = new Producto("Cafetera", costo);
	}

	@Then("se verifica si se puede o no canjear")
	@Alias("se verifica si se $puede o no canjear")
	public void sePuedeCanjear(Boolean esPosible) {
		testCiudadanoSteps.shouldBeTrue(esPosible, producto.canjeable(puntosAcumulados));
	}
}
