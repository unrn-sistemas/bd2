package ar.edu.unrn.municipiopp.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ar.edu.unrn.municipiopp.models.Categoria;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCategoriaDAO {
	private CategoriaDAO categoriaDAO = new CategoriaDAO();
	private Categoria categoria;

	@BeforeClass
	public static void beforeAll() throws Exception {
		createTable();
	}

	@AfterClass
	public static void tearDown() throws Exception {
		deleteTable();
	}

	@Before
	public void setUp() throws Exception {
		categoria = new Categoria("lkhjkghjiufty4", 99999);
	}

	@Test
	public void a_testCreate() {
		try {
			categoriaDAO.create(categoria);
		} catch (SQLException e) {
			// La categoría existe
			Assert.assertEquals("23505", e.getSQLState());
		}
	}

	@Test
	public void b_testReads() {
		try {
			// read() method test
			List<Categoria> categorias = categoriaDAO.read();
			Assert.assertTrue(categorias.size() > 0);

			// read(id) method test
			Categoria categoria = categoriaDAO.read(categorias.get(0).getId());
			Assert.assertEquals(categorias.get(0).getId(), categoria.getId());
			categoriaDAO.read("asd");
		} catch (SQLException e) {
			if (e.getSQLState() != "24000")
				e.printStackTrace();
			Assert.assertEquals("24000", e.getSQLState());
		}
	}

	@Test
	public void c_testUpdate() {
		try {
			List<Categoria> categorias = categoriaDAO.read();
			Categoria categoria = categorias.get(0);
			categoria.update("dfhskdjh3487", categoria.getPuntaje());
			categoriaDAO.update(categoria);
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.assertEquals("23505", e.getSQLState());
		}
	}

	@Test
	public void d_testDelete() {
		try {
			List<Categoria> categorias = categoriaDAO.read();
			Categoria categoria = categorias.get(0);
			categoriaDAO.delete(categoria.getId());

			categoriaDAO.delete("asd");
		} catch (SQLException e) {
			if (e.getSQLState() != "24000") {
				e.printStackTrace();
				return;
			}
			Assert.assertTrue(true);
		}
	}

	// -------------------------------------------------------
	// Table creation/delete methods
	// -------------------------------------------------------

	public static void createTable() {
		try (Connection connection = DerbyConnection.localAccess();
				Statement statement = connection.createStatement();) {
			String createCategoriaTable = "CREATE TABLE categorias "
					+ "(id CHAR(36) PRIMARY KEY, nombre VARCHAR(22) UNIQUE, puntaje INT)";

			// Devuelve falso cuando el primer resultado no es un ResulSet. En
			// este caso, no lo es, por eso se espera que sea falso.
			Assert.assertFalse(statement.execute(createCategoriaTable));
		} catch (SQLException e) {
			if (e.getErrorCode() != 30000) {
				e.printStackTrace();
			}
			Assert.assertEquals(30000, e.getErrorCode());
		}
	}

	public static void deleteTable() {
		try (Connection connection = DerbyConnection.localAccess();
				Statement statement = connection.createStatement();) {
			String deleteCategoriaTable = "DROP TABLE categorias";

			// Devuelve falso cuando el primer resultado no es un ResulSet. En
			// este caso, no lo es, por eso se espera que sea falso.
			Assert.assertFalse(statement.execute(deleteCategoriaTable));
		} catch (SQLException e) {
			if (e.getErrorCode() != 30000) {
				e.printStackTrace();
			}
			Assert.assertEquals(30000, e.getErrorCode());
		}
	}
}
