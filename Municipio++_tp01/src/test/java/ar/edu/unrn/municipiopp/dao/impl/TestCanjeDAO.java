package ar.edu.unrn.municipiopp.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ar.edu.unrn.municipiopp.models.Canje;
import ar.edu.unrn.municipiopp.models.Ciudadano;
import ar.edu.unrn.municipiopp.models.Producto;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCanjeDAO {
	private static CanjeDAO canjeDAO = new CanjeDAO();
	private static ProductoDAO productoDAO = new ProductoDAO();
	private static CiudadanoDAO ciudadanoDAO = new CiudadanoDAO();
	private Canje canje;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		createTables();
		productoDAO.create(new Producto("akjfhsd", 999));
		ciudadanoDAO.create(new Ciudadano("akjfhsd", "sdjfgd", "jkdshfs", "askdjshas"));
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		deleteTables();
	}

	@Before
	public void setUp() throws Exception {
		Producto producto = productoDAO.read().get(0);
		Ciudadano ciudadano = ciudadanoDAO.read().get(0);
		canje = new Canje(LocalDate.now(), ciudadano, producto);
	}

	@Test
	public void a_testCreate() {
		try {
			canjeDAO.create(canje);
		} catch (SQLException e) {
			if (e.getSQLState() != "404")
				e.printStackTrace();
			Assert.assertEquals("404", e.getSQLState());
		}
	}

	@Test
	public void b_testFilerByCiudadano() {
		try {
			Ciudadano ciudadano = ciudadanoDAO.read().get(0);
			List<Canje> canjes = canjeDAO.filterByCiudadano(ciudadano.getId());
			Assert.assertTrue(canjes.size() >= 0);
		} catch (SQLException e) {
			if (e.getSQLState() != "3905")
				e.printStackTrace();
			Assert.assertEquals("3905", e.getSQLState());
		}
	}

	@Test
	public void b_testReads() {
		try {
			canjeDAO.read();
		} catch (SQLException e) {
			if (e.getSQLState() != "3905")
				e.printStackTrace();
			Assert.assertEquals("3905", e.getSQLState());
		}
	}

	@Test
	public void c_testUpdate() {
		try {
			canjeDAO.update(canje);
		} catch (SQLException e) {
			if (e.getSQLState() != "3905")
				e.printStackTrace();
			Assert.assertEquals("3905", e.getSQLState());
		}
	}

	@Test
	public void d_testDelete() {
		try {
			canjeDAO.delete("sdfjhg");
		} catch (SQLException e) {
			if (e.getSQLState() != "3905")
				e.printStackTrace();
			Assert.assertEquals("3905", e.getSQLState());
		}
	}

	// -------------------------------------------------------
	// Table creation/delete methods
	// -------------------------------------------------------
	public static void createTables() {
		TestProductoDAO.createTable();
		TestCiudadanoDAO.createTable();
		createTable();
	}

	public static void createTable() {
		try (Connection connection = DerbyConnection.localAccess();
				Statement statement = connection.createStatement();) {
			String createTable = "CREATE TABLE canjes " + "(id CHAR(36) PRIMARY KEY, " + "fecha DATE, "
					+ "ciudadano_id CHAR(36), " + "producto_id CHAR(36), "
					+ "FOREIGN KEY (ciudadano_id) REFERENCES ciudadanos(id), "
					+ "FOREIGN KEY (producto_id) REFERENCES productos(id)" + ")";

			// Devuelve falso cuando el primer resultado no es un ResulSet. En
			// este caso, no lo es, por eso se espera que sea falso.
			Assert.assertFalse(statement.execute(createTable));
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.assertEquals(30000, e.getErrorCode());
		}
	}

	public static void deleteTables() {
		deleteTable();
		TestProductoDAO.deleteTable();
		TestCiudadanoDAO.deleteTable();
	}

	public static void deleteTable() {
		try (Connection connection = DerbyConnection.localAccess();
				Statement statement = connection.createStatement();) {
			String deleteTable = "DROP TABLE canjes";
			Assert.assertFalse(statement.execute(deleteTable));

			// Devuelve falso cuando el primer resultado no es un ResulSet. En
			// este caso, no lo es, por eso se espera que sea falso.
		} catch (SQLException e) {
			if (e.getErrorCode() != 30000) {
				e.printStackTrace();
			}
			Assert.assertEquals(30000, e.getErrorCode());
		}
	}
}
