package ar.edu.unrn.municipiopp.facade.impl;

import static org.junit.Assert.fail;

import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.unrn.municipiopp.dao.impl.CategoriaDAO;
import ar.edu.unrn.municipiopp.dao.impl.CiudadanoDAO;
import ar.edu.unrn.municipiopp.dao.impl.EventoDAO;
import ar.edu.unrn.municipiopp.dao.impl.ProductoDAO;
import ar.edu.unrn.municipiopp.dao.impl.ReclamoDAO;
import ar.edu.unrn.municipiopp.dao.impl.TestCanjeDAO;
import ar.edu.unrn.municipiopp.dao.impl.TestCategoriaDAO;
import ar.edu.unrn.municipiopp.dao.impl.TestCiudadanoDAO;
import ar.edu.unrn.municipiopp.dao.impl.TestEventoDAO;
import ar.edu.unrn.municipiopp.dao.impl.TestProductoDAO;
import ar.edu.unrn.municipiopp.dao.impl.TestReclamoDAO;
import ar.edu.unrn.municipiopp.dto.CanjeDTO;
import ar.edu.unrn.municipiopp.dto.CategoriaDTO;
import ar.edu.unrn.municipiopp.dto.CiudadanoDTO;
import ar.edu.unrn.municipiopp.dto.EventoDTO;
import ar.edu.unrn.municipiopp.dto.ProductoDTO;
import ar.edu.unrn.municipiopp.dto.ReclamoDTO;
import ar.edu.unrn.municipiopp.facade.Api;
import ar.edu.unrn.municipiopp.factories.FactoryDAO;
import ar.edu.unrn.municipiopp.models.Categoria;
import ar.edu.unrn.municipiopp.models.Ciudadano;
import ar.edu.unrn.municipiopp.models.Evento;
import ar.edu.unrn.municipiopp.models.Producto;
import ar.edu.unrn.municipiopp.models.Reclamo;

/**
 * No hay validación alguna de los datos. Es esperable que pudiera fallar el
 * este debido a ello.
 * 
 * @author Luciano
 *
 */
public class TestAPIImpl {
	private Api api = new ApiImpl();
	private CategoriaDAO categoriaDAO = (CategoriaDAO) FactoryDAO.getInstance("CategoriaDAO");
	private ProductoDAO productoDAO = (ProductoDAO) FactoryDAO.getInstance("ProductoDAO");
	private CiudadanoDAO ciudadanoDAO = (CiudadanoDAO) FactoryDAO.getInstance("CiudadanoDAO");
	private ReclamoDAO reclamoDAO = (ReclamoDAO) FactoryDAO.getInstance("ReclamoDAO");
	private EventoDAO eventoDAO = (EventoDAO) FactoryDAO.getInstance("EventoDAO");

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestCategoriaDAO.createTable();
		TestCiudadanoDAO.createTable();
		TestProductoDAO.createTable();
		TestCanjeDAO.createTable();
		TestReclamoDAO.createTable();
		TestEventoDAO.createTable();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		TestEventoDAO.deleteTable();
		TestReclamoDAO.deleteTable();
		TestCanjeDAO.deleteTable();
		TestProductoDAO.deleteTable();
		TestCiudadanoDAO.deleteTable();
		TestCategoriaDAO.deleteTable();
	}

	/*----------------------------------------------------
	 | Categorías
	 |----------------------------------------------------
	 */
	@Test
	public void testNuevaCategoria() {
		try {
			api.nuevaCategoria("sldjhs", 999);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testModificarCategoria() {
		try {
			api.nuevaCategoria("dlfkgj", 999);
			Categoria categoria = categoriaDAO.read().get(0);
			api.modificarCategoria(categoria.getId(), "sldjhs", 999);
		} catch (Exception e) {
			if (!e.getMessage().equals("La categoría ya existe.")) {
				e.printStackTrace();
			}
			Assert.assertEquals("La categoría ya existe.", e.getMessage());
		}
	}

	@Test
	public void testListarCategorias() {
		try {
			List<CategoriaDTO> categoriasDTO = api.listarCategorias();
			Assert.assertTrue(categoriasDTO.size() >= 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void testBajaCategoria() {
		try {
			api.nuevaCategoria("kjfhg", 999);
			Categoria categoria = categoriaDAO.read().get(0);
			api.bajaCategoria(categoria.getId());
			api.bajaCategoria("kjsdhdskjh");
		} catch (Exception e) {
			if (!e.getMessage().equals("No existe una categoría con id `kjsdhdskjh`.") && !e.getMessage().equals(
					"La Categoría no puede ser eliminada debido a que existen objetos que dependen de ella. Primero bórre esos objetos.")) {
				e.printStackTrace();
				fail();
				return;
			}
			Assert.assertTrue(true);
		}

	}

	/*----------------------------------------------------
	 | Productos
	 |----------------------------------------------------
	 */
	@Test
	public void testNuevoProducto() {
		try {
			api.nuevoProducto("sldjhs", 999);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testBajaProducto() {
		try {
			api.nuevoProducto("dlfkgj", 999);
			Producto producto = productoDAO.read().get(0);
			api.bajaProducto(producto.getId());
			api.bajaProducto("kjsdhdskjh");
		} catch (Exception e) {
			if (!e.getMessage().equals("No existe un producto con id `kjsdhdskjh`.")) {
				e.printStackTrace();
			}
			Assert.assertEquals("No existe un producto con id `kjsdhdskjh`.", e.getMessage());
		}
	}

	@Test
	public void testModificarProducto() {
		try {
			api.nuevoProducto("dlfkgj", 999);
			Producto producto = productoDAO.read().get(0);
			api.modificarProducto(producto.getId(), "sldjhs", 999);
		} catch (Exception e) {
			if (!e.getMessage().equals("El producto ya existe.")) {
				e.printStackTrace();
			}
			Assert.assertEquals("El producto ya existe.", e.getMessage());
		}
	}

	@Test
	public void testListarProductos() {
		try {
			List<ProductoDTO> productosDTO = api.listarProductos();
			Assert.assertTrue(productosDTO.size() >= 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	/*----------------------------------------------------
	 | Ciudadanos
	 |----------------------------------------------------
	 */
	@Test
	public void testNuevoCiudadano() {
		try {
			api.nuevoCiudadano("Luciano", "Graziani", "35591234", "lgraziani2712@gmail.com");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testBajaCiudadano() {
		try {
			api.nuevoCiudadano("Luciano", "Graziani", "sdjhfkjs", "lkfdjglj");
			Ciudadano ciudadano = ciudadanoDAO.read().get(0);
			api.bajaCiudadano(ciudadano.getId());
			api.bajaCiudadano("kjsdhdskjh");
		} catch (Exception e) {
			if (!e.getMessage().equals("No existe un ciudadano con id `kjsdhdskjh`.") && !e.getMessage().equals(
					"El Ciudadano no puede ser eliminado debido a que existen objetos que dependen de él. Primero bórre esos objetos.")) {
				e.printStackTrace();
				fail();
				return;
			}
			Assert.assertTrue(true);
		}
	}

	@Test
	public void testModificarCiudadano() {
		try {
			api.nuevoCiudadano("Luciano", "Graziani", "sdjjsd3h", "lkfdjglj");
			Ciudadano ciudadano = ciudadanoDAO.read().get(0);
			api.modificarCiudadano(ciudadano.getId(), "Luciano", "Graziani", "lgraziani2712@gmail.com");
		} catch (Exception e) {
			if (!e.getMessage().equals("El correo ingresado ya está en uso.")) {
				e.printStackTrace();
			}
			Assert.assertEquals("El correo ingresado ya está en uso.", e.getMessage());
		}
	}

	@Test
	public void testListarCiudadanos() {
		try {
			List<CiudadanoDTO> ciudadanosDTO = api.listarCiudadanos();
			Assert.assertTrue(ciudadanosDTO.size() >= 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void testListarCanjes() {
		try {
			api.nuevoCiudadano("Luciano", "Graziani", "sdjjs43h", "asjdhksjh");
			Ciudadano ciudadano = ciudadanoDAO.read().get(0);
			List<CanjeDTO> canjesDTO = api.listarCanjes(ciudadano.getId());
			Assert.assertTrue(canjesDTO.size() >= 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void testCanjear() {
		try {
			api.nuevaCategoria("sdljfh", 88);
			Categoria categoria = categoriaDAO.read().get(0);

			api.nuevoCiudadano("Luciano", "Graziani", "sdjjs43h", "asjdhksjh");
			Ciudadano ciudadano = ciudadanoDAO.read().get(0);

			api.nuevoReclamo(ciudadano.getId(), categoria.getId(), "lskdjfsdlkj", "lksdjfskdlj");
			ciudadanoDAO.update(ciudadano);

			api.nuevoProducto("ksdhfskd", 88);
			Producto producto1 = productoDAO.read().get(0);
			api.nuevoProducto("lkjhfd", 999);
			Producto producto2 = productoDAO.read().get(1);

			api.canjear(ciudadano.getId(), producto1.getId());
			api.canjear(ciudadano.getId(), producto2.getId());
		} catch (Exception e) {
			if (!e.getMessage().equals("No tiene puntos suficientes para canjear el producto.")
					&& !e.getMessage().equals("Antes de pedir un canje, primero realice reclamos.")) {
				e.printStackTrace();
			}
			Assert.assertTrue(e.getMessage().equals("No tiene puntos suficientes para canjear el producto.")
					|| e.getMessage().equals("Antes de pedir un canje, primero realice reclamos."));
		}
	}

	/*----------------------------------------------------
	 | Reclamos y eventos
	 |----------------------------------------------------
	 */
	@Test
	public void testNuevoReclamo() {
		try {
			api.nuevoCiudadano("sdsdfj", "dfsfd", "12345678", "fgdfsadash");
			api.nuevaCategoria("dfdlsjfh", 998);
			Categoria categoria = categoriaDAO.read().get(0);
			Ciudadano ciudadano = ciudadanoDAO.read().get(0);
			api.nuevoReclamo(ciudadano.getId(), categoria.getId(), "flkgkfdglh", "lgkjfgho");
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void testTraerReclamo() {
		try {
			api.nuevoCiudadano("sdsdfj", "dfsfd", "62345678", "9ug87rhd");
			Ciudadano ciudadano = ciudadanoDAO.read().get(0);
			api.nuevaCategoria("098fgj", 998);
			Categoria categoria = categoriaDAO.read().get(0);
			api.nuevoReclamo(ciudadano.getId(), categoria.getId(), "flkgkfdglh", "lgkjfgho");
			Reclamo reclamo = reclamoDAO.read().get(0);

			ReclamoDTO reclamoDTO = api.traerReclamo(reclamo.getId());
			Assert.assertEquals(ReclamoDTO.class, reclamoDTO.getClass());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	// @Test
	public void testTraerReclamoConEventos() {
		// Es el resultado de api.traerReclamo() y api.traerEventos().
	}

	@Test
	public void testTraerEventos() {
		try {
			api.nuevoCiudadano("lfg", "kji", "62347678", "9890yuhnv");
			api.nuevaCategoria("kndf95", 998);
			Categoria categoria = categoriaDAO.read().get(0);
			Ciudadano ciudadano = ciudadanoDAO.read().get(0);
			api.nuevoReclamo(ciudadano.getId(), categoria.getId(), "bs02", "90fhjfk");
			Reclamo reclamo = reclamoDAO.read().get(0);
			api.agregarEvento(reclamo.getId(), "flgkjhdfuh");
			List<EventoDTO> eventos = api.traerEventos(reclamo.getId());
			Assert.assertTrue(eventos.size() >= 1);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void testAgregarEvento() {
		try {
			api.nuevoCiudadano("p987yh", "kj098i", "62247678", "9890y908hnv");
			api.nuevaCategoria("kn0df95", 998);
			Categoria categoria = categoriaDAO.read().get(0);
			Ciudadano ciudadano = ciudadanoDAO.read().get(0);
			api.nuevoReclamo(ciudadano.getId(), categoria.getId(), "bs02", "90fhjfk");
			Reclamo reclamo = reclamoDAO.read().get(0);
			api.agregarEvento(reclamo.getId(), "sdjlfhskdjhf.");
			Evento evento = eventoDAO.read().get(0);
			Assert.assertEquals(Evento.class, evento.getClass());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

}
