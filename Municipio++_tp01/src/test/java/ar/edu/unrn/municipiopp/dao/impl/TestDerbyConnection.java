package ar.edu.unrn.municipiopp.dao.impl;

import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Assert;
import org.junit.Test;

public class TestDerbyConnection {

	/******************************************************************
	 * Derby serverTest connection
	 ******************************************************************
	 * This test tries to access to the server side DB.
	 * 
	 * @version 0.1.0
	 * @author Luciano Graziani
	 */
	// @Test
	public void serverSideTest() {
		// try-with-resources
		try (Connection connection = DerbyConnection.serverAccess()) {
			Assert.assertFalse(connection.isClosed());
		} catch (SQLException e) {
			e.printStackTrace();
			fail("Excepción de SQL.");
		}
	}

	/******************************************************************
	 * Derby localTest connection
	 ******************************************************************
	 * This test tries to access to the local side DB.
	 * 
	 * @version 0.1.0
	 * @author Luciano Graziani
	 */
	@Test
	public void localSideTest() {
		try (Connection connection = DerbyConnection.localAccess()) {
			Assert.assertFalse(connection.isClosed());
		} catch (SQLException e) {
			e.printStackTrace();
			fail("Excepción de SQL.");
		}
	}
}
