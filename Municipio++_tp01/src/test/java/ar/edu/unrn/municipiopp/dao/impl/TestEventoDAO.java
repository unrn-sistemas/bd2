package ar.edu.unrn.municipiopp.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ar.edu.unrn.municipiopp.models.Categoria;
import ar.edu.unrn.municipiopp.models.Ciudadano;
import ar.edu.unrn.municipiopp.models.Evento;
import ar.edu.unrn.municipiopp.models.Reclamo;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestEventoDAO {
	private EventoDAO eventoDAO = new EventoDAO();
	private static ReclamoDAO reclamoDAO = new ReclamoDAO();
	private static CategoriaDAO categoriaDAO = new CategoriaDAO();
	private static CiudadanoDAO ciudadanoDAO = new CiudadanoDAO();
	private Evento evento;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		createTables();
		categoriaDAO.create(new Categoria("akjfhsd", 999));
		ciudadanoDAO.create(new Ciudadano("akjfhsd", "sdjfgd", "jkdshfs", "askdjshas"));
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		deleteTables();
	}

	@Before
	public void setUp() throws Exception {
		Categoria categoria = categoriaDAO.read().get(0);
		Ciudadano ciudadano = ciudadanoDAO.read().get(0);
		ciudadano.nuevoReclamo("ljsdhasj", "lksjdalskj", categoria);
		ciudadanoDAO.update(ciudadano);
		Reclamo reclamo = reclamoDAO.read().get(0);
		reclamo.agregarEvento(new Evento("lkhjkghjiufty4", LocalDate.now(), reclamo));
		reclamoDAO.update(reclamo);
	}

	@Test
	public void a_testCreate() {
		try {
			eventoDAO.create(evento);
		} catch (SQLException e) {
			if (e.getSQLState() != "404")
				e.printStackTrace();
			Assert.assertTrue(true);
		}
	}

	@Test
	public void b_testRead() {
		try {
			// read() method test
			List<Evento> eventos = eventoDAO.read();
			Assert.assertTrue(eventos.size() > 0);

			// read(id) method test
			Evento evento = eventoDAO.read(eventos.get(0).getId());
			Assert.assertEquals(eventos.get(0).getReclamo().getId(), evento.getReclamo().getId());

			reclamoDAO.read("asd");
		} catch (SQLException e) {
			if (e.getSQLState() != "24000")
				e.printStackTrace();
			Assert.assertEquals("24000", e.getSQLState());
		}
	}

	@Test
	public void c_testUpdate() {
		try {
			List<Evento> eventos = eventoDAO.read();
			Evento evento = eventos.get(0);
			Evento eventoActualizado = new Evento(evento.getId(), "dfhskdjh3487", evento.getFecha(),
					evento.getReclamo());
			eventoDAO.update(eventoActualizado);
		} catch (SQLException e) {
			if (e.getSQLState() != "3906")
				e.printStackTrace();
			Assert.assertEquals("3906", e.getSQLState());
		}
	}

	@Test
	public void d_testDelete() {
		try {
			List<Evento> eventos = eventoDAO.read();
			Evento evento = eventos.get(0);
			eventoDAO.delete(evento.getId());

			eventoDAO.delete("asd");
		} catch (SQLException e) {
			e.printStackTrace();
			Assert.assertEquals("24000", e.getSQLState());
		}
	}

	// -------------------------------------------------------
	// Table creation/delete methods
	// -------------------------------------------------------
	public static void createTables() {
		TestReclamoDAO.createTables();
		createTable();
	}

	public static void createTable() {
		try (Connection connection = DerbyConnection.localAccess();
				Statement statement = connection.createStatement();) {
			String createEventoTable = "CREATE TABLE eventos " + "(" + "id CHAR(36) PRIMARY KEY,"
					+ "descripcion LONG VARCHAR," + "fecha DATE," + "reclamo_id CHAR(36), "
					+ "FOREIGN KEY (reclamo_id) REFERENCES reclamos(id)" + ")";

			// Devuelve falso cuando el primer resultado no es un ResulSet. En
			// este caso, no lo es, por eso se espera que sea falso.
			Assert.assertFalse(statement.execute(createEventoTable));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void deleteTables() {
		deleteTable();
		TestReclamoDAO.deleteTables();
	}

	public static void deleteTable() {
		try (Connection connection = DerbyConnection.localAccess();
				Statement statement = connection.createStatement();) {
			String deleteEventoTable = "DROP TABLE eventos";

			// Devuelve falso cuando el primer resultado no es un ResulSet. En
			// este caso, no lo es, por eso se espera que sea falso.
			Assert.assertFalse(statement.execute(deleteEventoTable));
		} catch (SQLException e) {
			if (e.getErrorCode() != 30000) {
				e.printStackTrace();
			}
			Assert.assertEquals(30000, e.getErrorCode());
		}
	}
}
