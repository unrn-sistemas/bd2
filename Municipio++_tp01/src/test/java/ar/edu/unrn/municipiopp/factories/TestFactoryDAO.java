package ar.edu.unrn.municipiopp.factories;

import org.junit.Assert;
import org.junit.Test;

import ar.edu.unrn.municipiopp.dao.impl.CanjeDAO;
import ar.edu.unrn.municipiopp.dao.impl.CategoriaDAO;
import ar.edu.unrn.municipiopp.dao.impl.CiudadanoDAO;
import ar.edu.unrn.municipiopp.dao.impl.EventoDAO;
import ar.edu.unrn.municipiopp.dao.impl.ProductoDAO;
import ar.edu.unrn.municipiopp.dao.impl.ReclamoDAO;

public class TestFactoryDAO {

	@Test
	public void testGetInstance() {
		try {
			Assert.assertEquals(CanjeDAO.class, FactoryDAO.getInstance("CanjeDAO").getClass());
			Assert.assertEquals(CategoriaDAO.class, FactoryDAO.getInstance("CategoriaDAO").getClass());
			Assert.assertEquals(CiudadanoDAO.class, FactoryDAO.getInstance("CiudadanoDAO").getClass());
			Assert.assertEquals(EventoDAO.class, FactoryDAO.getInstance("EventoDAO").getClass());
			Assert.assertEquals(ProductoDAO.class, FactoryDAO.getInstance("ProductoDAO").getClass());
			Assert.assertEquals(ReclamoDAO.class, FactoryDAO.getInstance("ReclamoDAO").getClass());

			FactoryDAO.getInstance("ljhdfsj");
		} catch (RuntimeException e) {
			if (!e.getMessage().equals("No existe una clase con ese nombre.")) {
				e.printStackTrace();
			}
			Assert.assertEquals("No existe una clase con ese nombre.", e.getMessage());
		}
	}

}
