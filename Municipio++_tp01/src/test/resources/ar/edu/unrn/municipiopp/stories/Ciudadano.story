Ciudadano Story

Narrative:
// Con el fin de
In order to conseguir un Producto
As a Ciudadano
I want to canjear mis puntos

Scenario: Comprobación de que se puede o no canjear un Producto
Given una cantidad de <puntosAcumulados>
When se selecciona un Producto de <costo>, para canjear
Then se verifica si se <puede> o no canjear

Examples:
| puntosAcumulados	| costo	| puede	|
| 0					| 15	| false	|
| 123				| 15	| true	|
| 15				| 15	| true	|