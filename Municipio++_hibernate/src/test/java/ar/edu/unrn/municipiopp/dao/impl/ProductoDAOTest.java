package ar.edu.unrn.municipiopp.dao.impl;

import static org.junit.Assert.fail;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ar.edu.unrn.municipiopp.dao.impl.ProductoDAO;
import ar.edu.unrn.municipiopp.models.Producto;
import ar.edu.unrn.municipiopp.utils.HibernateUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductoDAOTest {

	private ProductoDAO productoDAO;
	private static Session session;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		session = HibernateUtil.getSession();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		session.close();
	}

	@Before
	public void setUp() throws Exception {
		productoDAO = new ProductoDAO(session);
	}

	@Test
	public void a_testCreate() throws Exception {
		Producto producto = new Producto("Tv LED", 1000);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			productoDAO.create(producto);
			transaction.commit();
		} catch (Throwable e) {
			if (transaction != null)
				transaction.rollback();
			fail("Error");
			throw new RuntimeException(e);
		}
	}

	@Test
	public void b_testRead() {
		try {
			List<Producto> productos = productoDAO.read();
			Assert.assertTrue(productos.size() > 0);

			Producto producto = productoDAO.read(productos.get(0).getId());
			Assert.assertTrue(Producto.class.toString().equals(producto.getClass().toString().split("_")[0]));
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

	@Test
	public void c_testUpdate() {
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Producto producto = productoDAO.read().get(0);
			producto.update("Producto Nombre Nuevo", 2000);
			productoDAO.update(producto);
			transaction.commit();
		} catch (Throwable e) {
			if (transaction != null)
				transaction.rollback();
			fail("Falló");
			throw new RuntimeException(e);
		}
	}

	// @Test
	public void d_testDelete() {
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Producto producto = productoDAO.read().get(0);
			productoDAO.delete(producto.getId());
			transaction.commit();
		} catch (Throwable e) {
			if (transaction != null)
				transaction.rollback();
			fail("Falló");
			throw new RuntimeException(e);
		}
	}

}
