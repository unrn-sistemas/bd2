package ar.edu.unrn.municipiopp.dao.impl;

import static org.junit.Assert.fail;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ar.edu.unrn.municipiopp.dao.impl.CategoriaDAO;
import ar.edu.unrn.municipiopp.dao.impl.CiudadanoDAO;
import ar.edu.unrn.municipiopp.dao.impl.ProductoDAO;
import ar.edu.unrn.municipiopp.models.Canje;
import ar.edu.unrn.municipiopp.models.Categoria;
import ar.edu.unrn.municipiopp.models.Ciudadano;
import ar.edu.unrn.municipiopp.models.Producto;
import ar.edu.unrn.municipiopp.utils.HibernateUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CiudadanoDAOTest {
	private CiudadanoDAO ciudadanoDAO;
	private static Session session;
	private Transaction transaction = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		session = HibernateUtil.getSession();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		session.close();
	}

	@Before
	public void setUp() throws Exception {
		ciudadanoDAO = new CiudadanoDAO(session);
	}

	@Test
	public void a_testCreate() throws Exception {
		Ciudadano ciudadano = new Ciudadano("Luciano", "Graziani", "35591234", "lgraziani2712@gmail.com");
		try {
			transaction = session.beginTransaction();
			ciudadanoDAO.create(ciudadano);
			transaction.commit();
		} catch (Throwable e) {
			if (transaction != null)
				transaction.rollback();
			fail("Error");
			throw new RuntimeException(e);
		}
	}

	@Test
	public void b_testRead() {
		try {
			List<Ciudadano> ciudadanos = ciudadanoDAO.read();
			Assert.assertTrue(ciudadanos.size() > 0);

			Ciudadano ciudadano = ciudadanoDAO.read(ciudadanos.get(0).getId());
			Assert.assertTrue(Ciudadano.class.toString().equals(ciudadano.getClass().toString().split("_")[0]));
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

	@Test
	public void c_testUpdate() {
		try {
			transaction = session.beginTransaction();
			Ciudadano ciudadano = ciudadanoDAO.read().get(0);
			ciudadanoDAO.update(ciudadano);
			transaction.commit();
		} catch (Throwable e) {
			if (transaction != null)
				transaction.rollback();
			fail("Falló");
			throw new RuntimeException(e);
		}
	}

	@Test
	public void d_testCanjear() throws SQLException {
		try {
			transaction = session.beginTransaction();
			Ciudadano ciudadano = ciudadanoDAO.read().get(0);
			ciudadano.canjear(new Producto("Producto1", 0));
			ciudadano.canjear(new Producto("Producto2", 0));
			ciudadanoDAO.update(ciudadano);
			transaction.commit();
			Assert.assertTrue(new ProductoDAO(session).read().size() > 0);
		} catch (Throwable e) {
			if (transaction != null)
				transaction.rollback();
			fail("Falló");
			throw new RuntimeException(e);
		}
	}

	@Test
	public void e_testReadCanjes() throws SQLException {
		Ciudadano ciudadano = ciudadanoDAO.read().get(0);
		List<Canje> canjes = ciudadano.getCanjes();
		Assert.assertTrue(canjes.size() > 0);
	}

	@Test
	public void f_testCanjearonProductoElDia() throws SQLException {
		ProductoDAO productoDAO = new ProductoDAO(session);
		Producto producto = productoDAO.read().get(0);
		List<Ciudadano> ciudadanos = ciudadanoDAO.canjearonProductoElDia(producto, Date.valueOf(LocalDate.now()));
		Assert.assertTrue(ciudadanos.size() > 0);
		ciudadanos = ciudadanoDAO.canjearonProductoElDia(producto, Date.valueOf(LocalDate.parse("2013-03-20")));
		Assert.assertTrue(ciudadanos.size() == 0);
	}

	@Test
	public void g_testConReclamosDeCategoria() throws SQLException {
		try {
			transaction = session.beginTransaction();
			Categoria categoria = new Categoria("locuras", 99999);
			CategoriaDAO categoriaDAO = new CategoriaDAO(session);
			categoriaDAO.create(categoria);
			Ciudadano ciudadano = ciudadanoDAO.read().get(0);
			ciudadano.nuevoReclamo("asdsdjfh", "sfkhsjdfsdas", categoria);
			ciudadanoDAO.update(ciudadano);
			transaction.commit();
			List<Ciudadano> ciudadanos = ciudadanoDAO.conReclamosDeCategoria(categoria);
			Assert.assertTrue(ciudadanos.size() > 0);
		} catch (Throwable e) {
			if (transaction != null)
				transaction.rollback();
			fail("Falló");
			throw new RuntimeException(e);
		}
	}

	@Test
	public void h_testListarProductosQueFueronCanjeadosEnFecha() {
		List<Producto> productos = ciudadanoDAO.listarProductosQueFueronCanjeadosEnFecha(Date.valueOf(LocalDate.now()));
		Assert.assertTrue(productos.size() > 0);
	}

	@Test
	public void i_testProductosMasCanjeados() throws SQLException {
		List<Producto> productos = ciudadanoDAO.productosMasCanjeados();
		Assert.assertTrue(productos.size() > 0);
	}

	@Test
	public void j_testListarEnOrdenDeCantidadDeCanjes() throws SQLException {
		// --------------------
		// Pre-test
		// --------------------
		Ciudadano ciudadano = new Ciudadano("Gabriela", "Cayú", "33823853", "gabycayu77@gmail.com");
		try {
			transaction = session.beginTransaction();
			ciudadanoDAO.create(ciudadano);
			transaction.commit();
		} catch (Throwable e) {
			if (transaction != null)
				transaction.rollback();
			fail("Error");
			throw new RuntimeException(e);
		}
		// --------------------
		// Test
		// --------------------
		List<Object[]> ciudadanos = ciudadanoDAO.listarEnOrdenDeCantidadDeCanjes();
		Assert.assertTrue(ciudadanos.size() == 2);
		Assert.assertTrue(ciudadanos.get(0)[0].equals("Luciano"));
	}

	@Test
	public void z_testDelete() throws SQLException {
		transaction = session.beginTransaction();
		Ciudadano ciudadano;
		try {
			ciudadano = ciudadanoDAO.read().get(0);
			ciudadanoDAO.delete(ciudadano.getId());
			transaction.commit();
			Assert.assertTrue(true);
		} catch (ConstraintViolationException e) {
			Assert.assertTrue(true);
		} catch (Throwable e) {
			if (transaction != null)
				transaction.rollback();
			fail("Falló");
			throw new RuntimeException(e);
		}
	}

}