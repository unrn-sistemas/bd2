package ar.edu.unrn.municipiopp.facade.impl;

import static org.junit.Assert.fail;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.hibernate.Session;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ar.edu.unrn.municipiopp.dao.impl.CategoriaDAO;
import ar.edu.unrn.municipiopp.dao.impl.CiudadanoDAO;
import ar.edu.unrn.municipiopp.dao.impl.ProductoDAO;
import ar.edu.unrn.municipiopp.dao.impl.ReclamoDAO;
import ar.edu.unrn.municipiopp.dto.CanjeDTO;
import ar.edu.unrn.municipiopp.dto.CategoriaDTO;
import ar.edu.unrn.municipiopp.dto.CiudadanoDTO;
import ar.edu.unrn.municipiopp.dto.EventoDTO;
import ar.edu.unrn.municipiopp.dto.ProductoDTO;
import ar.edu.unrn.municipiopp.dto.ReclamoDTO;
import ar.edu.unrn.municipiopp.facade.Api;
import ar.edu.unrn.municipiopp.models.Categoria;
import ar.edu.unrn.municipiopp.models.Ciudadano;
import ar.edu.unrn.municipiopp.models.Producto;
import ar.edu.unrn.municipiopp.models.Reclamo;
import ar.edu.unrn.municipiopp.utils.HibernateUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestApiImpl {
	private static Api api = ApiImpl.getInstance();

	// --------------------------------------------------------------------------
	// Todas las creaciones independientes
	// --------------------------------------------------------------------------
	@Test
	public void a_testNuevaCategoria() {
		try {
			api.nuevaCategoria("Categoria 1", 10);
			api.nuevaCategoria("Categoria 2", 20);
			api.nuevaCategoria("Categoria 3", 30);
			api.nuevaCategoria("Categoria 4", 30);
			Assert.assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void a_testNuevoProducto() {
		try {
			api.nuevoProducto("Producto 1", 5);
			api.nuevoProducto("Producto 2", 15);
			api.nuevoProducto("Producto 3", 25);
			api.nuevoProducto("Producto 4", 35);
			Assert.assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void a_testNuevoCiudadano() {
		try {
			api.nuevoCiudadano("Gabriela", "Cayú", "33841853", "gcayu@unrn.edu.ar");
			api.nuevoCiudadano("Luciano", "Graziani", "35591234", "lgraziani@unrn.edu.ar");
			api.nuevoCiudadano("Mauro", "Cambarieri", "25252525", "mcambarieri@unrn.edu.ar");
			api.nuevoCiudadano("Enrique", "Molinari", "28282828", "mmolinari@unrn.edu.ar");
			Assert.assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	// --------------------------------------------------------------------------
	// Todas las creaciones dependientes
	// --------------------------------------------------------------------------
	@Test
	public void b_testNuevoReclamo() {
		Session session = HibernateUtil.getSession();
		CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(session);
		CategoriaDAO categoriaDAO = new CategoriaDAO(session);
		try {
			List<Ciudadano> ciudadanos = ciudadanoDAO.read();
			List<Categoria> categorias = categoriaDAO.read();

			// Test
			for (int i = 0; i < 4; i++) {
				Ciudadano ciudadano = ciudadanos.get(i);
				Categoria categoria = categorias.get(i);
				api.nuevoReclamo(ciudadano.getId(), categoria.getId(), "dlfjshdkjfh sdfsd",
						"Reclamo dlfjhsdfkshgdfkhsgdjf");
			}
			Assert.assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void c_testCanjear() {
		Session session = HibernateUtil.getSession();
		CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(session);
		ProductoDAO productoDAO = new ProductoDAO(session);
		try {
			List<Ciudadano> ciudadanos = ciudadanoDAO.read();
			List<Producto> productos = productoDAO.read();

			// Test
			for (int i = 0; i < 4; i++) {
				Ciudadano ciudadano = ciudadanos.get(i);
				Producto producto = productos.get(i);
				api.canjear(ciudadano.getId(), producto.getId());
			}
			Assert.assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void d_testAgregarEvento() {
		Session session = HibernateUtil.getSession();
		ReclamoDAO reclamoDAO = new ReclamoDAO(session);
		try {
			List<Reclamo> reclamos = reclamoDAO.read();

			// Test
			for (int i = 0; i < 4; i++) {
				Reclamo reclamo = reclamos.get(i);
				api.agregarEvento(reclamo.getId(), "Evento asjkdhakjs");
			}
			Assert.assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	// --------------------------------------------------------------------------
	// Todas las lecturas
	// --------------------------------------------------------------------------
	@Test
	public void e_testListarCategorias() {
		try {
			List<CategoriaDTO> categorias = api.listarCategorias();
			Assert.assertTrue(categorias.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_testListarProductos() {
		try {
			List<ProductoDTO> productos = api.listarProductos();
			Assert.assertTrue(productos.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_testListarCiudadanos() {
		try {
			List<CiudadanoDTO> ciudadanos = api.listarCiudadanos();
			Assert.assertTrue(ciudadanos.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_testListarCanjes() {
		try {
			List<CiudadanoDTO> ciudadanosDTO = api.listarCiudadanos();
			for (CiudadanoDTO ciudadanoDTO : ciudadanosDTO) {
				List<CanjeDTO> canjes = api.listarCanjes(ciudadanoDTO.getId());
				Assert.assertTrue(canjes.size() == 1 || canjes.size() == 0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_testTraerReclamo() {
		try {
			List<ReclamoDTO> reclamosDTO = api.listarReclamos();
			for (ReclamoDTO reclamoDTO : reclamosDTO) {
				ReclamoDTO reclamo = api.traerReclamo(reclamoDTO.getId());
				Assert.assertTrue(reclamo.getId().equals(reclamoDTO.getId()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_testTraerEventos() {
		try {
			List<ReclamoDTO> reclamosDTO = api.listarReclamos();
			for (ReclamoDTO reclamoDTO : reclamosDTO) {
				List<EventoDTO> eventosDTO = api.traerEventos(reclamoDTO.getId());
				Assert.assertTrue(eventosDTO.size() == 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_testTraerReclamoConEventos() {
		try {
			List<ReclamoDTO> reclamosDTO = api.listarReclamos();
			for (ReclamoDTO reclamoDTO : reclamosDTO) {
				ReclamoDTO reclamo = api.traerReclamoConEventos(reclamoDTO.getId());
				Assert.assertTrue(reclamo.getEventos().size() == 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_testReclamoEntreFechas() {
		try {
			List<ReclamoDTO> reclamosDTO = api.reclamoEntreFechas(Date.valueOf(LocalDate.now()), null);
			Assert.assertTrue(reclamosDTO.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_testCanjearonProductoElDia() {
		try {
			List<ProductoDTO> productosDTO = api.listarProductos();
			for (ProductoDTO productoDTO : productosDTO) {
				List<CiudadanoDTO> ciudadanosDTO = api.canjearonProductoElDia(productoDTO.getId(),
						Date.valueOf(LocalDate.now()));
				Assert.assertTrue(ciudadanosDTO.size() == 1 || ciudadanosDTO.size() == 0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_ConReclamosDeCategoria() {
		try {
			List<CategoriaDTO> categoriasDTO = api.listarCategorias();
			for (CategoriaDTO categoriaDTO : categoriasDTO) {
				List<CiudadanoDTO> ciudadanosDTO = api.conReclamosDeCategoria(categoriaDTO.getId());
				Assert.assertTrue(ciudadanosDTO.size() == 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_ListarProductosQueFueronCanjeadosEnFecha() {
		try {
			List<ProductoDTO> productosDTO = api
					.listarProductosQueFueronCanjeadosEnFecha(Date.valueOf(LocalDate.now()));
			Assert.assertTrue(productosDTO.size() == 3);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_ProductosMasCanjeados() {
		try {
			List<ProductoDTO> productos = api.productosMasCanjeados();
			Assert.assertTrue(productos.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_ListarEnOrdenDeCantidadDeCanjes() {
		try {
			List<Object[]> ciudadanos = api.listarEnOrdenDeCantidadDeCanjes();
			for (Object[] ciudadano : ciudadanos) {
				Assert.assertTrue(String.class == ciudadano[0].getClass());
				Assert.assertTrue(Integer.class == ciudadano[1].getClass());
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	// --------------------------------------------------------------------------
	// Modificaciones
	// --------------------------------------------------------------------------
	@Test
	public void f_testModificarCategoria() {
		try {
			List<CategoriaDTO> categoriasDTO = api.listarCategorias();
			for (int i = 1; i <= categoriasDTO.size(); i++) {
				CategoriaDTO categoria = categoriasDTO.get(i - 1);
				categoria.setNombre("Modificada " + i);
				api.modificarCategoria(categoria.getId(), categoria.getNombre(), null);
			}
			Assert.assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void f_testModificarProducto() {
		try {
			List<ProductoDTO> productosDTO = api.listarProductos();
			for (int i = 1; i <= productosDTO.size(); i++) {
				ProductoDTO producto = productosDTO.get(i - 1);
				producto.setNombre("Modificado " + i);
				api.modificarProducto(producto.getId(), producto.getNombre(), null);
			}
			Assert.assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void f_testModificarCiudadano() {
		try {
			List<CiudadanoDTO> ciudadanosDTO = api.listarCiudadanos();
			for (int i = 1; i <= ciudadanosDTO.size(); i++) {
				CiudadanoDTO ciudadano = ciudadanosDTO.get(i - 1);
				ciudadano.setNombre("Modificado " + i);
				api.modificarCiudadano(ciudadano.getId(), ciudadano.getNombre(), null, null);
			}
			Assert.assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	// --------------------------------------------------------------------------
	// Modificaciones
	// --------------------------------------------------------------------------
	// Nota: ¿Es realmente necesario implementar un mecanismo de borrado cuando
	// los dos objetos más importantes (Canjes y Reclamos) no pueden ser
	// borrados? Y los tres objetos aquí debajo dependen de ellos.
	@Test
	public void g_testBajaCategoria() {
		try {
			List<CategoriaDTO> categoriasDTO = api.listarCategorias();
			for (CategoriaDTO categoria : categoriasDTO) {
				api.bajaCategoria(categoria.getId());
			}
			Assert.assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(e.getMessage()
					.equals("No fue posible dar de baja la categoria porque hay objetos que hacen referencia a ésta."));
		}
	}

	@Test
	public void g_testBajaProducto() {
		try {
			List<ProductoDTO> productosDTO = api.listarProductos();
			for (ProductoDTO producto : productosDTO) {
				api.bajaProducto(producto.getId());
			}
			Assert.assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(e.getMessage()
					.equals("No fue posible dar de baja el producto porque hay objetos que hacen referencia a éste."));
		}
	}

	@Test
	public void g_testBajaCiudadano() {
		try {
			List<CiudadanoDTO> ciudadanosDTO = api.listarCiudadanos();
			for (CiudadanoDTO ciudadano : ciudadanosDTO) {
				api.bajaCiudadano(ciudadano.getId());
			}
			Assert.assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(e.getMessage()
					.equals("No fue posible dar de baja el ciudadano porque hay objetos que hacen referencia a éste."));
		}
	}

}
