package ar.edu.unrn.municipiopp.dao.impl;

import static org.junit.Assert.fail;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ar.edu.unrn.municipiopp.dao.impl.CategoriaDAO;
import ar.edu.unrn.municipiopp.dao.impl.ReclamoDAO;
import ar.edu.unrn.municipiopp.models.Categoria;
import ar.edu.unrn.municipiopp.models.Ciudadano;
import ar.edu.unrn.municipiopp.models.Evento;
import ar.edu.unrn.municipiopp.models.Reclamo;
import ar.edu.unrn.municipiopp.utils.HibernateUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ReclamoDAOTest {
	private ReclamoDAO reclamoDAO;
	private static Session session;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		session = HibernateUtil.getSession();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		session.close();
	}

	@Before
	public void setUp() throws Exception {
		reclamoDAO = new ReclamoDAO(session);
	}

	@Test
	public void a_testCreate() {
		Categoria categoria = new Categoria("Categoria Nueva", 12);
		Reclamo reclamo = new Reclamo(Date.valueOf(LocalDate.now()), "Moreno 1751", "producto económico",
				new Ciudadano("Gabriela", "Cayú", "33823853", "gabycayu77@gmail.com"), categoria);
		Transaction transaction = null;
		transaction = session.beginTransaction();
		try {
			CategoriaDAO categoriaDAO = new CategoriaDAO(session);
			categoriaDAO.create(categoria);
			reclamoDAO.create(reclamo);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		}
	}

	@Test
	public void b_testRead() {
		try {
			List<Reclamo> reclamos = reclamoDAO.read();
			Assert.assertTrue(reclamos.size() > 0);

			Reclamo reclamo = reclamoDAO.read(reclamos.get(0).getId());
			Assert.assertTrue(Reclamo.class.toString().equals(reclamo.getClass().toString().split("_")[0]));
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

	@Test
	public void c_testUpdate() {
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Reclamo reclamo = reclamoDAO.read().get(0);
			reclamoDAO.update(reclamo);
			transaction.commit();
		} catch (Throwable e) {
			if (transaction != null)
				transaction.rollback();
			fail("Falló");
			throw new RuntimeException(e);
		}
	}

	@Test
	public void d_testCreateEvento() {
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Reclamo reclamo = reclamoDAO.read().get(0);
			reclamo.agregarEvento(new Evento("Evento Nuevo", Date.valueOf(LocalDate.now()), reclamo));
			reclamoDAO.update(reclamo);
			transaction.commit();
		} catch (Throwable e) {
			if (transaction != null)
				transaction.rollback();
			fail("Falló");
			throw new RuntimeException(e);
		}
	}

	@Test
	public void e_testReadEventos() throws SQLException {
		Reclamo reclamo = reclamoDAO.read().get(0);
		List<Evento> eventos = reclamo.getEventos();
		Assert.assertTrue(eventos.size() > 0);
	}

	@Test
	public void f_testReclamoDeCategoria() throws SQLException {
		CategoriaDAO categoriaDAO = new CategoriaDAO(session);
		Categoria categoria = categoriaDAO.read().get(0);
		List<Reclamo> reclamos = reclamoDAO.reclamoDeCategoria(categoria);
		Assert.assertTrue(reclamos.size() > 0);
	}

	@Test
	public void g_testReclamoEntreFechas() throws SQLException {
		// -------------------------------
		// Pre-Test
		// -------------------------------
		Categoria categoria = new Categoria("Categoria Vieja", 12);
		Reclamo reclamo = new Reclamo(Date.valueOf(LocalDate.parse("2015-03-02")), "Ashfskdfh", "sñkjfsldjfhsldkj",
				new Ciudadano("Luciano", "Graziani", "35591234", "skfgjhkdjfh"), categoria);
		Transaction transaction = null;
		transaction = session.beginTransaction();
		try {
			CategoriaDAO categoriaDAO = new CategoriaDAO(session);
			categoriaDAO.create(categoria);
			reclamoDAO.create(reclamo);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		}
		// -------------------------------
		// Test w/HQL
		// -------------------------------
		List<Reclamo> reclamos = reclamoDAO.reclamoEntreFechasHQL(Date.valueOf(LocalDate.parse("2015-03-01")), null);
		Assert.assertTrue(reclamos.size() == 2);
		reclamos = reclamoDAO.reclamoEntreFechasHQL(Date.valueOf(LocalDate.parse("2015-03-01")),
				Date.valueOf(LocalDate.now()));
		Assert.assertTrue(reclamos.size() == 2);
		reclamos = reclamoDAO.reclamoEntreFechasHQL(Date.valueOf(LocalDate.parse("2015-04-03")),
				Date.valueOf(LocalDate.now()));
		Assert.assertTrue(reclamos.size() == 1);
		reclamos = reclamoDAO.reclamoEntreFechasHQL(null, Date.valueOf(LocalDate.now()));
		Assert.assertTrue(reclamos.size() == 2);
		// -------------------------------
		// Test w/Criteria
		// -------------------------------
		reclamos = reclamoDAO.reclamoEntreFechasCriteria(Date.valueOf(LocalDate.parse("2015-03-01")), null);
		Assert.assertTrue(reclamos.size() == 2);
		reclamos = reclamoDAO.reclamoEntreFechasCriteria(Date.valueOf(LocalDate.parse("2015-03-01")),
				Date.valueOf(LocalDate.now()));
		Assert.assertTrue(reclamos.size() == 2);
		reclamos = reclamoDAO.reclamoEntreFechasCriteria(Date.valueOf(LocalDate.parse("2015-04-03")),
				Date.valueOf(LocalDate.now()));
		Assert.assertTrue(reclamos.size() == 1);
		reclamos = reclamoDAO.reclamoEntreFechasCriteria(null, Date.valueOf(LocalDate.now()));
		Assert.assertTrue(reclamos.size() == 2);
	}

	@Test
	public void z_testDelete() throws SQLException {
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Reclamo reclamo = reclamoDAO.read().get(0);
			reclamoDAO.delete(reclamo.getId());
			transaction.commit();
		} catch (ConstraintViolationException e) {
			if (transaction != null)
				transaction.rollback();
			Assert.assertTrue(true);
		}
	}

}
