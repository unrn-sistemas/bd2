package ar.edu.unrn.municipiopp.dao.impl;

import static org.junit.Assert.fail;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ar.edu.unrn.municipiopp.dao.impl.CategoriaDAO;
import ar.edu.unrn.municipiopp.models.Categoria;
import ar.edu.unrn.municipiopp.utils.HibernateUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CategoriaDAOTest {
	private CategoriaDAO categoriaDAO;
	private static Session session;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		session = HibernateUtil.getSession();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		session.close();
	}

	@Before
	public void setUp() throws Exception {
		categoriaDAO = new CategoriaDAO(session);
	}

	@Test
	public void a_testCreate() {
		Categoria categoria = new Categoria("Categoria Nueva", 5);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			categoriaDAO.create(categoria);
			transaction.commit();
		} catch (Throwable e) {
			if (transaction != null)
				transaction.rollback();
			fail("Error");
			throw new RuntimeException(e);
		}
	}

	@Test
	public void b_testRead() {
		try {
			List<Categoria> categorias = categoriaDAO.read();
			Assert.assertTrue(categorias.size() > 0);

			Categoria categoria = categoriaDAO.read(categorias.get(0).getId());
			Assert.assertTrue(Categoria.class == categoria.getClass());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

	@Test
	public void c_testUpdate() {
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Categoria categoria = categoriaDAO.read().get(0);
			categoria.update("asdljhdsf", null);
			categoriaDAO.update(categoria);
			transaction.commit();
		} catch (Throwable e) {
			if (transaction != null)
				transaction.rollback();
			fail("Falló");
			throw new RuntimeException(e);
		}
	}

	@Test
	public void d_testDelete() {
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Categoria categoria = categoriaDAO.read().get(0);
			categoriaDAO.delete(categoria.getId());
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			fail("Falló");
			e.printStackTrace();
		}
	}

}
