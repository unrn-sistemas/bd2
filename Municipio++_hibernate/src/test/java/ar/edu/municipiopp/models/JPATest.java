package ar.edu.municipiopp.models;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;

public class JPATest {
	protected EntityManager em;
	protected EntityTransaction tx;
	
	@Before
	public void inicializar() {
		// Start EntityManagerFactory
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("test");
		// First unit of work
		em = emf.createEntityManager();
		tx = em.getTransaction();
		tx.begin();
		System.out.println("jpa test setup....");
	}

	@After
	public void tearDown() {
		tx.commit();
		em.close();
	}
}
