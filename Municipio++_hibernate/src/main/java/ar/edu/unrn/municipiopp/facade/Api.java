package ar.edu.unrn.municipiopp.facade;

import java.sql.Date;
import java.util.List;

import ar.edu.unrn.municipiopp.dto.CanjeDTO;
import ar.edu.unrn.municipiopp.dto.CategoriaDTO;
import ar.edu.unrn.municipiopp.dto.CiudadanoDTO;
import ar.edu.unrn.municipiopp.dto.EventoDTO;
import ar.edu.unrn.municipiopp.dto.ProductoDTO;
import ar.edu.unrn.municipiopp.dto.ReclamoDTO;

public interface Api {
	/*----------------------------------------------------
	 | Categorías
	 |----------------------------------------------------
	 */
	void nuevaCategoria(String nombre, Integer puntaje) throws RuntimeException;

	void bajaCategoria(String id) throws RuntimeException;

	/**
	 * Modifica los datos de la categoría. Nota: la modificación del puntaje no
	 * es retroactiva.
	 * 
	 * @param id
	 * @param nuevoNombre
	 *            null able
	 * @param nuevoPuntaje
	 *            null able
	 * @throws RuntimeException
	 */
	void modificarCategoria(String id, String nuevoNombre, Integer nuevoPuntaje) throws RuntimeException;

	List<CategoriaDTO> listarCategorias() throws RuntimeException;

	/*----------------------------------------------------
	 | Productos
	 |----------------------------------------------------
	 */

	/**
	 * Modifica los datos del producto. Nota: la modificación del puntaje no es
	 * retroactiva.
	 * 
	 * @param nombre
	 *            null able
	 * @param puntajeCosto
	 *            null able
	 * @throws RuntimeException
	 */
	void nuevoProducto(String nombre, Integer puntajeCosto) throws RuntimeException;

	void bajaProducto(String id) throws RuntimeException;

	void modificarProducto(String id, String nuevoNombre, Integer nuevoPuntajeCosto) throws RuntimeException;

	List<ProductoDTO> listarProductos() throws RuntimeException;

	/*----------------------------------------------------
	 | Ciudadanos
	 |----------------------------------------------------
	 */
	void nuevoCiudadano(String nombre, String apellido, String dni, String email) throws RuntimeException;

	void bajaCiudadano(String id) throws RuntimeException;

	/**
	 * 
	 * @param id
	 * @param nombre
	 *            null able
	 * @param apellido
	 *            null able
	 * @param email
	 *            null able
	 * @throws RuntimeException
	 */
	void modificarCiudadano(String id, String nombre, String apellido, String email) throws RuntimeException;

	List<CiudadanoDTO> listarCiudadanos() throws RuntimeException;

	List<CiudadanoDTO> canjearonProductoElDia(String idProducto, Date fecha) throws RuntimeException;

	List<CiudadanoDTO> conReclamosDeCategoria(String idCategoria) throws RuntimeException;

	List<ProductoDTO> listarProductosQueFueronCanjeadosEnFecha(Date fecha) throws RuntimeException;

	List<ProductoDTO> productosMasCanjeados() throws RuntimeException;

	List<Object[]> listarEnOrdenDeCantidadDeCanjes() throws RuntimeException;

	List<CanjeDTO> listarCanjes(String idCiudadano) throws RuntimeException;

	void canjear(String idCiudadano, String idProducto) throws RuntimeException;

	/*----------------------------------------------------
	 | Reclamos y eventos
	 |----------------------------------------------------
	 */
	void nuevoReclamo(String idCiudadano, String idCategoria, String direccion, String detalle) throws RuntimeException;

	List<ReclamoDTO> listarReclamos() throws RuntimeException;

	ReclamoDTO traerReclamo(String idReclamo) throws RuntimeException;

	ReclamoDTO traerReclamoConEventos(String idReclamo) throws RuntimeException;

	List<ReclamoDTO> reclamoEntreFechas(Date fechaDesde, Date fechaHasta) throws RuntimeException;

	List<EventoDTO> traerEventos(String idReclamo) throws RuntimeException;

	void agregarEvento(String idReclamo, String descripcion) throws RuntimeException;

}
