package ar.edu.unrn.municipiopp.dto;

import java.sql.Date;

import ar.edu.unrn.municipiopp.models.Evento;

public class EventoDTO extends GenericDTO {
	private String descripcion;
	private Date fecha;
	private String reclamoId;

	public EventoDTO(String id, String descripcion, Date fecha, String reclamoId) {
		this.id = id;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.reclamoId = reclamoId;
	}

	public EventoDTO(Evento evento) {
		this.id = evento.getId();
		this.descripcion = evento.getDescripcion();
		this.fecha = evento.getFecha();
		this.reclamoId = evento.getReclamo().getId();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getReclamoId() {
		return reclamoId;
	}

	public void setReclamoId(String reclamoId) {
		this.reclamoId = reclamoId;
	}

}
