package ar.edu.unrn.municipiopp.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import ar.edu.unrn.municipiopp.dao.DAOInterface;

@SuppressWarnings("unchecked")
public abstract class GenericDAO<T> implements DAOInterface<T> {
	protected Session session;

	public GenericDAO(Session session) {
		this.session = session;
	}

	// --------------------------------------------------
	// Métodos de la interface
	// --------------------------------------------------
	@Override
	public void create(T t) {
		session.save(t);
	}

	@Override
	public List<T> read() {
		Query query = session.createQuery(this.getAllQuery());
		List<T> t = query.list();
		return t;
	}

	@Override
	public T read(String id) {
		return (T) session.load(this.getActualClass(), id);
	}

	/**
	 * Si con recuperar un objeto de la DB, modificarlo en sesión, y hacer el
	 * commit, se guarda solo, ¿en qué situaciones se utilizaría este método?
	 */
	@Override
	public void update(T t) {
		session.saveOrUpdate(t);
	}

	@Override
	public void delete(String id) {
		T t = (T) session.get(this.getActualClass(), id);
		session.delete(t);
	}

	// --------------------------------------------------
	// Métodos hook
	// --------------------------------------------------
	protected abstract String getAllQuery();

	protected abstract Class<T> getActualClass();

	protected abstract String getObjectName();

}
