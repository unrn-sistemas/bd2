package ar.edu.unrn.municipiopp.dao.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ar.edu.unrn.municipiopp.models.Canje;
import ar.edu.unrn.municipiopp.models.Categoria;
import ar.edu.unrn.municipiopp.models.Ciudadano;
import ar.edu.unrn.municipiopp.models.Producto;

@SuppressWarnings("unchecked")
public class CiudadanoDAO extends GenericDAO<Ciudadano> {

	public CiudadanoDAO(Session session) {
		super(session);
	}

	// --------------------------------------------------
	// Métodos propios
	// --------------------------------------------------
	public List<Ciudadano> canjearonProductoElDia(Producto producto, Date fecha) {
		List<Ciudadano> ciudadanos = session.createCriteria(Ciudadano.class).createCriteria("canjes")
				.add(Restrictions.eq("fecha", fecha)).createCriteria("producto")
				.add(Restrictions.eq("id", producto.getId())).list();
		return ciudadanos;
	}

	public List<Canje> listarCanjes(String id) {
		String HQLString = "SELECT canje " + "FROM Canje canje " + "WHERE canje.ciudadano.id = :id";
		Query query = session.createQuery(HQLString);
		query.setString("id", id);
		return query.list();
	}

	public List<Ciudadano> conReclamosDeCategoria(Categoria categoria) {
		List<Ciudadano> ciudadanos = session.createCriteria(Ciudadano.class).createCriteria("reclamos")
				.createCriteria("categoria").add(Restrictions.eq("id", categoria.getId())).list();
		return ciudadanos;
	}

	public List<Producto> listarProductosQueFueronCanjeadosEnFecha(Date fecha) {
		String HQLString = "SELECT canjes.producto " + "FROM Ciudadano ciudadano, " + "IN (ciudadano.canjes) canjes, "
				+ "IN (ciudadano.reclamos) reclamos "
				+ "WHERE YEAR(reclamos.fecha) = :year and MONTH(reclamos.fecha) = :month";
		Query query = session.createQuery(HQLString);
		query.setInteger("month", fecha.toLocalDate().getMonthValue());
		query.setInteger("year", fecha.toLocalDate().getYear());
		return query.list();
	}

	/**
	 * Devuelve un listado de los productos que tienen la mayor cantidad de
	 * canjes realizados.
	 * 
	 * La poco eficiente solución que se nos ocurrió es la siguiente: 1r
	 * consulta: se recupera la cantidad de canjes más alta. 2a consulta: se
	 * recuperan todos los ID de los productos con igual cantidad de canjes que
	 * el valor anterior. 3a y más consultas: se recupera cada uno de los
	 * productos recuperados en la consulta anterior.
	 * 
	 * Una mejora sería realizar la primer y segunda consulta en una sola. Pero
	 * creemos que (al menos en Derby) la tercera en adelante seguirán estando.
	 */
	public List<Producto> productosMasCanjeados() {
		String HQLString = "SELECT COUNT(*) as cantidad FROM Canje canjes GROUP BY canjes.producto.nombre ORDER BY 1 DESC";
		Query query = session.createQuery(HQLString);
		Long count = (Long) query.list().get(0);

		HQLString = "SELECT pro.id FROM Canje canje JOIN canje.producto pro GROUP BY pro HAVING COUNT(pro.id) = :cant";
		query = session.createQuery(HQLString);
		query.setLong("cant", count);
		List<String> productosId = (List<String>) query.list();
		List<Producto> productos = new ArrayList<Producto>();
		for (String productoId : productosId) {
			productos.add((Producto) session.load(Producto.class, productoId));
		}

		return productos;
	}

	/**
	 * Devuelve el nombre de los ciudadanos ordenados según la cantidad de
	 * canjes.
	 * 
	 * @return {"nombre","cantidad"}
	 */
	public List<Object[]> listarEnOrdenDeCantidadDeCanjes() {
		Query query = session.getNamedQuery("listarEnOrdenDeCantidadDeCanjes");

		List<Object[]> objects = query.list();

		// Transforma cada long del COUNT(*) a entero
		for (int i = 0; i < objects.size(); i++) {
			Object[] object = objects.get(i);
			Integer count = safeLongToInt((long) object[1]);
			object[1] = count;
			objects.set(i, object);
		}
		return objects;
	}

	private Integer safeLongToInt(long l) {
		if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
			throw new IllegalArgumentException(l + " cannot be cast to int without changing its value.");
		}
		return (int) l;
	}

	// --------------------------------------------------
	// Métodos hook
	// --------------------------------------------------
	@Override
	protected Class<Ciudadano> getActualClass() {
		return Ciudadano.class;
	}

	@Override
	protected String getAllQuery() {
		return "FROM Ciudadano ciudadanos";
	}

	@Override
	protected String getObjectName() {
		return "Ciudadano";
	}
}
