package ar.edu.unrn.municipiopp.facade.impl;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;

import ar.edu.unrn.municipiopp.dao.impl.CategoriaDAO;
import ar.edu.unrn.municipiopp.dao.impl.CiudadanoDAO;
import ar.edu.unrn.municipiopp.dao.impl.ProductoDAO;
import ar.edu.unrn.municipiopp.dao.impl.ReclamoDAO;
import ar.edu.unrn.municipiopp.dto.CanjeDTO;
import ar.edu.unrn.municipiopp.dto.CategoriaDTO;
import ar.edu.unrn.municipiopp.dto.CiudadanoDTO;
import ar.edu.unrn.municipiopp.dto.EventoDTO;
import ar.edu.unrn.municipiopp.dto.ProductoDTO;
import ar.edu.unrn.municipiopp.dto.ReclamoDTO;
import ar.edu.unrn.municipiopp.facade.Api;
import ar.edu.unrn.municipiopp.models.Canje;
import ar.edu.unrn.municipiopp.models.Categoria;
import ar.edu.unrn.municipiopp.models.Ciudadano;
import ar.edu.unrn.municipiopp.models.Evento;
import ar.edu.unrn.municipiopp.models.Producto;
import ar.edu.unrn.municipiopp.models.Reclamo;
import ar.edu.unrn.municipiopp.utils.HibernateUtil;

public class ApiImpl implements Api {
	private static ApiImpl instance = null;

	private ApiImpl() {

	}

	public static synchronized ApiImpl getInstance() {
		if (instance != null)
			return instance;

		return new ApiImpl();
	}

	private void ejecutarEnSession(Command command) {
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();

			command.execute(session);

			session.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (session.getTransaction() != null)
				session.getTransaction().rollback();
		} finally {
			HibernateUtil.closeSession(session);
		}

	}

	/*----------------------------------------------------
	 | Categorías
	 |----------------------------------------------------
	 */
	@Override
	public void nuevaCategoria(String nombre, Integer puntaje) throws RuntimeException {
		ejecutarEnSession((Session session) -> {
			CategoriaDAO categoriaDAO = new CategoriaDAO(session);
			categoriaDAO.create(new Categoria(nombre, puntaje));
		});
	}

	@Override
	public void bajaCategoria(String id) throws RuntimeException {
		ejecutarEnSession((Session session) -> {
			try {
				CategoriaDAO categoriaDAO = new CategoriaDAO(session);
				categoriaDAO.delete(id);
			} catch (ConstraintViolationException e) {
				if (session.getTransaction() != null)
					session.getTransaction().rollback();
				throw new RuntimeException(
						"No fue posible dar de baja la categoria porque hay objetos que hacen referencia a ésta.");
			}
		});
	}

	@Override
	public void modificarCategoria(String id, String nuevoNombre, Integer nuevoPuntaje) throws RuntimeException {
		ejecutarEnSession((Session session) -> {
			CategoriaDAO categoriaDAO = new CategoriaDAO(session);
			Categoria categoria = categoriaDAO.read(id);
			categoria.update(nuevoNombre, nuevoPuntaje);
		});
	}

	@Override
	public List<CategoriaDTO> listarCategorias() throws RuntimeException {
		List<CategoriaDTO> categoriasDTO = new ArrayList<CategoriaDTO>();
		ejecutarEnSession((Session session) -> {
			CategoriaDAO categoriaDAO = new CategoriaDAO(session);
			List<Categoria> categorias = categoriaDAO.read();
			for (Categoria categoria : categorias)
				categoriasDTO.add(new CategoriaDTO(categoria));
		});

		return categoriasDTO;
	}

	/*----------------------------------------------------
	 | Productos
	 |----------------------------------------------------
	 */
	@Override
	public void nuevoProducto(String nombre, Integer puntajeCosto) throws RuntimeException {
		ejecutarEnSession((Session session) -> {
			ProductoDAO productoDAO = new ProductoDAO(session);
			productoDAO.create(new Producto(nombre, puntajeCosto));
		});
	}

	@Override
	public void bajaProducto(String id) throws RuntimeException {
		ejecutarEnSession((Session session) -> {
			try {
				ProductoDAO productoDAO = new ProductoDAO(session);
				productoDAO.delete(id);
			} catch (ConstraintViolationException e) {
				if (session.getTransaction() != null)
					session.getTransaction().rollback();
				throw new RuntimeException(
						"No fue posible dar de baja el producto porque hay objetos que hacen referencia a éste.");
			}
		});
	}

	@Override
	public void modificarProducto(String id, String nuevoNombre, Integer nuevoPuntajeCosto) throws RuntimeException {
		ejecutarEnSession((Session session) -> {
			ProductoDAO productoDAO = new ProductoDAO(session);
			Producto producto = productoDAO.read(id);
			producto.update(nuevoNombre, nuevoPuntajeCosto);
		});
	}

	@Override
	public List<ProductoDTO> listarProductos() throws RuntimeException {
		List<ProductoDTO> productosDTO = new ArrayList<ProductoDTO>();
		ejecutarEnSession((Session session) -> {
			ProductoDAO productoDAO = new ProductoDAO(session);
			List<Producto> productos = productoDAO.read();
			for (Producto producto : productos)
				productosDTO.add(new ProductoDTO(producto));
		});

		return productosDTO;
	}

	/*----------------------------------------------------
	 | Ciudadanos
	 |----------------------------------------------------
	 */
	@Override
	public void nuevoCiudadano(String nombre, String apellido, String dni, String email) throws RuntimeException {
		ejecutarEnSession((Session session) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(session);
			ciudadanoDAO.create(new Ciudadano(nombre, apellido, dni, email));
		});
	}

	@Override
	public void bajaCiudadano(String id) throws RuntimeException {
		ejecutarEnSession((Session session) -> {
			try {
				CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(session);
				ciudadanoDAO.delete(id);
			} catch (ConstraintViolationException e) {
				if (session.getTransaction() != null)
					session.getTransaction().rollback();
				throw new RuntimeException(
						"No fue posible dar de baja el ciudadano porque hay objetos que hacen referencia a éste.");
			}
		});
	}

	@Override
	public void modificarCiudadano(String id, String nombre, String apellido, String email) throws RuntimeException {
		ejecutarEnSession((Session session) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(session);
			Ciudadano ciudadano = ciudadanoDAO.read(id);
			ciudadano.update(nombre, apellido, email);
		});
	}

	@Override
	public List<CiudadanoDTO> listarCiudadanos() throws RuntimeException {
		List<CiudadanoDTO> ciudadanosDTO = new ArrayList<CiudadanoDTO>();
		ejecutarEnSession((Session session) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(session);
			List<Ciudadano> ciudadanos = ciudadanoDAO.read();
			for (Ciudadano ciudadano : ciudadanos)
				ciudadanosDTO.add(new CiudadanoDTO(ciudadano));
		});
		return ciudadanosDTO;
	}

	@Override
	public List<CanjeDTO> listarCanjes(String idCiudadano) throws RuntimeException {
		List<CanjeDTO> canjesDTO = new ArrayList<CanjeDTO>();
		ejecutarEnSession((Session session) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(session);
			List<Canje> canjes = ciudadanoDAO.listarCanjes(idCiudadano);
			for (Canje canje : canjes)
				canjesDTO.add(new CanjeDTO(canje));
		});

		return canjesDTO;
	}

	@Override
	public List<CiudadanoDTO> canjearonProductoElDia(String idProducto, Date fecha) throws RuntimeException {
		List<CiudadanoDTO> ciudadanosDTO = new ArrayList<CiudadanoDTO>();
		ejecutarEnSession((Session session) -> {
			ProductoDAO productoDAO = new ProductoDAO(session);
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(session);
			Producto producto = productoDAO.read(idProducto);
			List<Ciudadano> ciudadanos = ciudadanoDAO.canjearonProductoElDia(producto, fecha);
			for (Ciudadano ciudadano : ciudadanos)
				ciudadanosDTO.add(new CiudadanoDTO(ciudadano));
		});

		return ciudadanosDTO;
	}

	@Override
	public List<CiudadanoDTO> conReclamosDeCategoria(String idCategoria) throws RuntimeException {
		List<CiudadanoDTO> ciudadanosDTO = new ArrayList<CiudadanoDTO>();
		ejecutarEnSession((Session session) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(session);
			CategoriaDAO categoriaDAO = new CategoriaDAO(session);
			List<Ciudadano> ciudadanos = ciudadanoDAO.conReclamosDeCategoria(categoriaDAO.read(idCategoria));
			for (Ciudadano ciudadano : ciudadanos)
				ciudadanosDTO.add(new CiudadanoDTO(ciudadano));
		});

		return ciudadanosDTO;
	}

	@Override
	public List<ProductoDTO> listarProductosQueFueronCanjeadosEnFecha(Date fecha) {
		List<ProductoDTO> productosDTO = new ArrayList<ProductoDTO>();
		ejecutarEnSession((Session session) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(session);
			List<Producto> productos = ciudadanoDAO.listarProductosQueFueronCanjeadosEnFecha(fecha);
			for (Producto producto : productos)
				productosDTO.add(new ProductoDTO(producto));
		});

		return productosDTO;
	}

	@Override
	public List<ProductoDTO> productosMasCanjeados() {
		List<ProductoDTO> productosDTO = new ArrayList<ProductoDTO>();
		ejecutarEnSession((Session session) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(session);
			List<Producto> productos = ciudadanoDAO.productosMasCanjeados();
			for (Producto producto : productos)
				productosDTO.add(new ProductoDTO(producto));
		});

		return productosDTO;
	}

	@Override
	public List<Object[]> listarEnOrdenDeCantidadDeCanjes() {
		List<Object[]> enOrden = new ArrayList<Object[]>();
		ejecutarEnSession((Session session) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(session);
			enOrden.addAll(ciudadanoDAO.listarEnOrdenDeCantidadDeCanjes());
		});

		return enOrden;
	}

	/*----------------------------------------------------
	 | Canjes
	 |----------------------------------------------------
	 */
	@Override
	public void canjear(String idCiudadano, String idProducto) throws RuntimeException {
		ejecutarEnSession((Session session) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(session);
			ProductoDAO productoDAO = new ProductoDAO(session);
			Ciudadano ciudadano = ciudadanoDAO.read(idCiudadano);
			ciudadano.canjear(productoDAO.read(idProducto));
		});
	}

	/*----------------------------------------------------
	 | Reclamos
	 |----------------------------------------------------
	 */
	@Override
	public void nuevoReclamo(String idCiudadano, String idCategoria, String direccion, String detalle)
			throws RuntimeException {
		ejecutarEnSession((Session session) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(session);
			CategoriaDAO categoriaDAO = new CategoriaDAO(session);
			Ciudadano ciudadano = ciudadanoDAO.read(idCiudadano);
			ciudadano.nuevoReclamo(direccion, detalle, categoriaDAO.read(idCategoria));
		});
	}

	@Override
	public List<ReclamoDTO> listarReclamos() throws RuntimeException {
		List<ReclamoDTO> reclamosDTO = new ArrayList<ReclamoDTO>();
		ejecutarEnSession((Session session) -> {
			ReclamoDAO reclamoDAO = new ReclamoDAO(session);
			List<Reclamo> reclamos = reclamoDAO.read();
			for (Reclamo reclamo : reclamos)
				reclamosDTO.add(new ReclamoDTO(reclamo));
		});

		return reclamosDTO;
	}

	@Override
	public ReclamoDTO traerReclamo(String idReclamo) throws RuntimeException {
		List<ReclamoDTO> reclamoDTO = new ArrayList<ReclamoDTO>();
		ejecutarEnSession((Session session) -> {
			ReclamoDAO reclamoDAO = new ReclamoDAO(session);
			reclamoDTO.add(new ReclamoDTO(reclamoDAO.read(idReclamo)));
		});

		return reclamoDTO.get(0);
	}

	@Override
	public ReclamoDTO traerReclamoConEventos(String idReclamo) throws RuntimeException {
		List<ReclamoDTO> reclamoDTO = new ArrayList<ReclamoDTO>();
		ejecutarEnSession((Session session) -> {
			ReclamoDAO reclamoDAO = new ReclamoDAO(session);
			Reclamo reclamo = reclamoDAO.read(idReclamo);
			reclamo.getEventos(); // Se fuerza a que se recuperen \o/
			reclamoDTO.add(new ReclamoDTO(reclamo));
		});

		return reclamoDTO.get(0);
	}

	@Override
	public List<ReclamoDTO> reclamoEntreFechas(Date fechaDesde, Date fechaHasta) throws RuntimeException {
		List<ReclamoDTO> reclamosDTO = new ArrayList<ReclamoDTO>();
		ejecutarEnSession((Session session) -> {
			ReclamoDAO reclamoDAO = new ReclamoDAO(session);
			List<Reclamo> reclamos = reclamoDAO.reclamoEntreFechasCriteria(fechaDesde, fechaHasta);
			for (Reclamo reclamo : reclamos)
				reclamosDTO.add(new ReclamoDTO(reclamo));
		});

		return reclamosDTO;
	}

	/*----------------------------------------------------
	 | Eventos
	 |----------------------------------------------------
	 */
	@Override
	public List<EventoDTO> traerEventos(String idReclamo) throws RuntimeException {
		List<EventoDTO> eventosDTO = new ArrayList<EventoDTO>();
		ejecutarEnSession((Session session) -> {
			ReclamoDAO reclamoDAO = new ReclamoDAO(session);
			List<Evento> eventos = reclamoDAO.listarEventos(idReclamo);
			for (Evento evento : eventos)
				eventosDTO.add(new EventoDTO(evento));
		});

		return eventosDTO;
	}

	@Override
	public void agregarEvento(String idReclamo, String descripcion) throws RuntimeException {
		ejecutarEnSession((Session session) -> {
			ReclamoDAO reclamoDAO = new ReclamoDAO(session);
			Reclamo reclamo = reclamoDAO.read(idReclamo);
			reclamo.agregarEvento(new Evento(descripcion, Date.valueOf(LocalDate.now()), reclamo));
		});
	}
}
