package ar.edu.unrn.municipiopp.dao.impl;

import org.hibernate.Session;

import ar.edu.unrn.municipiopp.models.Producto;

public class ProductoDAO extends GenericDAO<Producto> {

	public ProductoDAO(Session session) {
		super(session);
	}

	// --------------------------------------------------
	// Métodos de la interface
	// --------------------------------------------------
	@Override
	protected Class<Producto> getActualClass() {
		return Producto.class;
	}

	@Override
	protected String getAllQuery() {
		return "FROM Producto productos";
	}

	@Override
	protected String getObjectName() {
		return "Producto";
	}

}
