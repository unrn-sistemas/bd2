package ar.edu.unrn.municipiopp.models;

import java.util.UUID;

public class Producto {

	private String id;
	private Integer puntajeCosto;
	private String nombre;

	public Producto() {
	}

	/**
	 * Constructor para instancias que están en la DB.
	 */
	public Producto(String id, String nombre, Integer puntajeCosto) {
		this.id = id;
		this.nombre = nombre;
		this.puntajeCosto = puntajeCosto;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Producto(String nombre, Integer puntajeCosto) {
		this.id = UUID.randomUUID().toString();
		this.nombre = nombre;
		this.puntajeCosto = puntajeCosto;
	}

	/**
	 * Cualquier parámetro puede ser null.
	 * 
	 * @param nombre
	 *            nullable
	 * @param puntajeCosto
	 *            nullable
	 */
	public void update(String nombre, Integer puntajeCosto) {
		this.nombre = nombre == null ? this.nombre : nombre;
		this.puntajeCosto = puntajeCosto == null ? this.puntajeCosto : puntajeCosto;
	}

	public String getNombre() {
		return nombre;
	}

	public Integer getPuntajeCosto() {
		return puntajeCosto;
	}

	public Boolean canjeable(Integer puntajeAcumulado) {
		return puntajeAcumulado >= this.puntajeCosto;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPuntajeCosto(Integer puntajeCosto) {
		this.puntajeCosto = puntajeCosto;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
