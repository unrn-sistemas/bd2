package ar.edu.unrn.municipiopp.models;

import java.sql.Date;
import java.util.UUID;

public class Evento {

	private String id;
	private String descripcion;
	private Date fecha;
	private Reclamo reclamo;

	/**
	 * Constructor para instancias existentes en la BD.
	 */
	public Evento() {

	}

	public Evento(String id, String descripcion, Date fecha, Reclamo reclamo) {
		this.id = id;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.reclamo = reclamo;
	}

	/**
	 * Constructor para instancias que no están en la DB.
	 */
	public Evento(String descripcion, Date fecha, Reclamo reclamo) {
		this.id = UUID.randomUUID().toString();
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.reclamo = reclamo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public Date getFecha() {
		return fecha;
	}

	public Reclamo getReclamo() {
		return reclamo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public void setReclamo(Reclamo reclamo) {
		this.reclamo = reclamo;
	}

}
