package ar.edu.unrn.municipiopp.models;

import java.util.UUID;

public class Categoria {
	private String id;
	private String nombre;
	private Integer puntaje;

	public Categoria() {

	}

	/**
	 * Constructor para instancias existentes en la BD.
	 */
	public Categoria(String id, String nombre, Integer puntaje) {
		this.id = id;
		this.nombre = nombre;
		this.puntaje = puntaje;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Categoria(String nombre, Integer puntaje) {
		this.id = UUID.randomUUID().toString();
		this.nombre = nombre;
		this.puntaje = puntaje;
	}

	/**
	 * Cualquier parámetro puede ser null.
	 * 
	 * @param nombre
	 *            nullable
	 * @param puntaje
	 *            nullable
	 */
	public void update(String nombre, Integer puntaje) {
		this.nombre = nombre == null ? this.nombre : nombre;
		this.puntaje = puntaje == null ? this.puntaje : puntaje;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(Integer puntaje) {
		this.puntaje = puntaje;
	}

}
