package ar.edu.unrn.municipiopp.dao.impl;

import org.hibernate.Session;

import ar.edu.unrn.municipiopp.models.Categoria;

public class CategoriaDAO extends GenericDAO<Categoria> {

	public CategoriaDAO(Session session) {
		super(session);
	}

	@Override
	protected Class<Categoria> getActualClass() {
		return Categoria.class;
	}

	@Override
	protected String getAllQuery() {
		return "FROM Categoria categorias";
	}

	@Override
	protected String getObjectName() {
		return "Categoria";
	}
}
