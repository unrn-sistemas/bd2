package ar.edu.unrn.municipiopp.dto;

import ar.edu.unrn.municipiopp.models.Categoria;

public class CategoriaDTO extends GenericDTO {
	private String nombre;
	private Integer puntaje;

	public CategoriaDTO() {
	}

	public CategoriaDTO(Categoria categoria) {
		this.id = categoria.getId();
		this.nombre = categoria.getNombre();
		this.puntaje = categoria.getPuntaje();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(Integer puntaje) {
		this.puntaje = puntaje;
	}

}
