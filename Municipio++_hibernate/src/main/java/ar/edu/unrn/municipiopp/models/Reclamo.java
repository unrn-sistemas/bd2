package ar.edu.unrn.municipiopp.models;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Reclamo {

	private String id;
	private Date fecha;
	private String direccion;
	private String detalle;
	private Ciudadano ciudadano;
	private Categoria categoria;
	private List<Evento> eventos = new ArrayList<Evento>();

	/**
	 * Para instancias ya existentes en la DB.
	 */

	public Reclamo() {

	}

	public Reclamo(String id, Date fecha, String direccion, String detalle, Ciudadano ciudadano,
			Categoria categoria) {

		this.id = id;
		this.fecha = fecha;
		this.direccion = direccion;
		this.detalle = detalle;
		this.categoria = categoria;
		this.ciudadano = ciudadano;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Reclamo(Date fecha, String direccion, String detalle, Ciudadano ciudadano, Categoria categoria) {
		this.id = UUID.randomUUID().toString();
		this.fecha = fecha;
		this.direccion = direccion;
		this.detalle = detalle;
		this.ciudadano = ciudadano;
		this.categoria = categoria;
	}

	// ---------------------------------------------------
	// Reglas del negocio
	// ---------------------------------------------------

	// ---------------------------------------------------
	// Getters
	// ---------------------------------------------------

	public void agregarEvento(Evento evento) {
		eventos.add(evento);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public Ciudadano getCiudadano() {
		return ciudadano;
	}

	public void setCiudadano(Ciudadano ciudadano) {
		this.ciudadano = ciudadano;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public List<Evento> getEventos() {
		return eventos;
	}

	public void setEventos(List<Evento> eventos) {
		this.eventos = eventos;
	}

}
