package ar.edu.unrn.municipiopp.facade.impl;

import org.hibernate.Session;

public interface Command {

	public void execute(Session session);

}
