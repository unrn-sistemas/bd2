package ar.edu.unrn.municipiopp.dao.impl;

import java.sql.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ar.edu.unrn.municipiopp.models.Categoria;
import ar.edu.unrn.municipiopp.models.Evento;
import ar.edu.unrn.municipiopp.models.Reclamo;

@SuppressWarnings("unchecked")
public class ReclamoDAO extends GenericDAO<Reclamo> {

	public ReclamoDAO(Session session) {
		super(session);
	}

	// --------------------------------------------------
	// Métodos propios
	// --------------------------------------------------
	public List<Evento> listarEventos(String id) {
		String HQLString = "SELECT evento " + "FROM Evento evento " + "WHERE evento.reclamo.id = :id";
		Query query = session.createQuery(HQLString);
		query.setString("id", id);
		return query.list();
	}

	@SuppressWarnings("unused")
	public List<Reclamo> reclamoDeCategoria(Categoria categoria) {
		String HQLStringImplicito = "SELECT reclamo " + "FROM Reclamo reclamo "
				+ "WHERE reclamo.categoria.id = :categoria";
		String HQLStringExplicito = "SELECT reclamo " + "FROM Reclamo reclamo " + "JOIN reclamo.categoria categoria "
				+ "WHERE categoria.id = :categoria";
		Query query = session.createQuery(HQLStringExplicito);
		query.setString("categoria", categoria.getId());
		return query.list();
	}

	public List<Reclamo> reclamoEntreFechasHQL(Date fechaDesde, Date fechaHasta) {
		StringBuffer HQL = new StringBuffer("SELECT reclamo FROM Reclamo reclamo ");
		if (fechaDesde != null && fechaHasta != null)
			HQL.append("WHERE reclamo.fecha BETWEEN :fechaDesde AND :fechaHasta");
		if (fechaDesde == null && fechaHasta != null) {
			HQL.append("WHERE reclamo.fecha <= :fechaHasta");
		}
		if (fechaHasta == null && fechaDesde != null) {
			HQL.append("WHERE reclamo.fecha >= :fechaDesde");
		}
		Query query = session.createQuery(HQL.toString());
		if (fechaDesde != null && fechaHasta != null) {
			query.setDate("fechaDesde", fechaDesde);
			query.setDate("fechaHasta", fechaHasta);
		}
		if (fechaDesde == null && fechaHasta != null)
			query.setDate("fechaHasta", fechaHasta);
		if (fechaHasta == null && fechaDesde != null)
			query.setDate("fechaDesde", fechaDesde);

		return query.list();

	}

	public List<Reclamo> reclamoEntreFechasCriteria(Date fechaDesde, Date fechaHasta) {
		Criteria reclamoCriteria = session.createCriteria(Reclamo.class);
		if (fechaDesde != null && fechaHasta != null)
			reclamoCriteria.add(Restrictions.between("fecha", fechaDesde, fechaHasta));
		if (fechaDesde == null && fechaHasta != null)
			reclamoCriteria.add(Restrictions.le("fecha", fechaHasta));
		if (fechaHasta == null && fechaDesde != null)
			reclamoCriteria.add(Restrictions.ge("fecha", fechaDesde));
		return reclamoCriteria.list();
	}

	// --------------------------------------------------
	// Métodos hook
	// --------------------------------------------------
	@Override
	protected Class<Reclamo> getActualClass() {
		return Reclamo.class;
	}

	@Override
	protected String getAllQuery() {
		return "FROM Reclamo reclamos";
	}

	@Override
	protected String getObjectName() {
		return "reclamo";
	}

}
