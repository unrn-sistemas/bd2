package ar.edu.unrn.municipiopp.models;

import java.sql.Date;
import java.util.UUID;

public class Canje {
	private String id;
	private Date fecha;
	private Ciudadano ciudadano;
	private Producto producto;

	public Canje() {
	}

	/**
	 * Constructor para instancias existentes en la BD.
	 */
	public Canje(String id, Date fecha, Ciudadano ciudadano, Producto producto) {
		this.setId(id);
		this.fecha = fecha;
		this.ciudadano = ciudadano;
		this.producto = producto;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Canje(Date fecha, Ciudadano ciudadano, Producto producto) {
		this.setId(UUID.randomUUID().toString());
		this.fecha = fecha;
		this.ciudadano = ciudadano;
		this.producto = producto;
	}

	public Date getFecha() {
		return fecha;
	}

	public Producto getProducto() {
		return producto;
	}

	public Ciudadano getCiudadano() {
		return ciudadano;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public void setCiudadano(Ciudadano ciudadano) {
		this.ciudadano = ciudadano;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}
