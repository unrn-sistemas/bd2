package ar.edu.unrn.municipiopp.dao;

import java.util.List;

public interface DAOInterface<T> {

	void create(T t);

	T read(String id);

	List<T> read();

	void update(T t);

	void delete(String id);
}
