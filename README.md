# Repositorio de Base de datos 2 de Luciano Graziani

## Estructura
El repositorio contiene los proyectos realizados en la materia, uno en cada carpeta.

- bd2.
	- TP01: Municipio++\_tp01
	- TP02 y 03: Municipio++\_hibernate
	- TP04: jpa-reclamos
	- TP final: final

Tanto el TP04 como el TP final implementan Gradle como administrador de dependencias, por lo que al acceder a la carpeta de cada TP, hay un README.md con el detalle de los requisitos necesarios para poder instalar las dependencias y poder ejecutarlos correctamente.
