package ar.edu.unrn.dbII.db4o.book;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.db4o.ObjectContainer;

import ar.edu.unrn.dbII.utils.Db4oUtil;

public class Chapter6Test {
	public class Piloto {
		private String nombre;

		public Piloto(String nombre) {
			this.nombre = nombre;
		}

		public String getNombre() {
			return nombre;
		}
	}

	public class Data {
		private int value;

		public Data(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	public class Auto {
		private Piloto piloto;
		private List<Data> values;

		public Auto() {
			piloto = new Piloto("piloto 1");
			this.values = new ArrayList<>();
		}

		public Piloto getPiloto() {
			return piloto;
		}

		public void snapshot() {
			values.add(new Data(new Random().nextInt((10 - 1) + 1) + 1));
		}

		public List<Data> getData() {
			return values;
		}
	}

	@Test
	public void testAuto() {
		Auto auto = new Auto();
		ObjectContainer db = Db4oUtil.openObjectDb();
		db.store(auto);
		auto = db.query(Auto.class).get(0);
		auto.snapshot();
		auto.snapshot();
		db.store(auto);
		auto = db.query(Auto.class).get(0);
		List<Data> values = auto.getData();
		Assert.assertTrue(values.size() > 1);
		db.close();
	}
}
