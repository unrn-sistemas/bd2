package ar.edu.unrn.dbII.api.impl;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.db4o.ObjectContainer;

import ar.edu.unrn.dbII.api.Api;
import ar.edu.unrn.dbII.clearDb.ClearDb;
import ar.edu.unrn.dbII.dao.impl.CategoriaDAO;
import ar.edu.unrn.dbII.dao.impl.CiudadanoDAO;
import ar.edu.unrn.dbII.dao.impl.ProductoDAO;
import ar.edu.unrn.dbII.dao.impl.ReclamoDAO;
import ar.edu.unrn.dbII.dto.CanjeDTO;
import ar.edu.unrn.dbII.dto.CategoriaDTO;
import ar.edu.unrn.dbII.dto.CiudadanoDTO;
import ar.edu.unrn.dbII.dto.EventoDTO;
import ar.edu.unrn.dbII.dto.ProductoDTO;
import ar.edu.unrn.dbII.dto.ReclamoDTO;
import ar.edu.unrn.dbII.modelo.Categoria;
import ar.edu.unrn.dbII.modelo.Ciudadano;
import ar.edu.unrn.dbII.modelo.Producto;
import ar.edu.unrn.dbII.modelo.Reclamo;
import ar.edu.unrn.dbII.utils.Db4oUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestApiImpl extends ClearDb {
	private static Api api = ApiImpl.getInstance();

	// --------------------------------------------------------------------------
	// Todas las creaciones independientes
	// --------------------------------------------------------------------------
	@Test
	public void a_nuevaCategoria() {
		try {
			api.nuevaCategoria("Categoria 1", 10);
			api.nuevaCategoria("Categoria 2", 20);
			api.nuevaCategoria("Categoria 3", 30);
			api.nuevaCategoria("Categoria 4", 30);
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void a_nuevoProducto() {
		try {
			api.nuevoProducto("Producto 1", 5);
			api.nuevoProducto("Producto 2", 15);
			api.nuevoProducto("Producto 3", 25);
			api.nuevoProducto("Producto 4", 35);
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void a_nuevoCiudadano() {
		try {
			api.nuevoCiudadano("Gabriela", "Cayú", "33841853", "gcayu@unrn.edu.ar");
			api.nuevoCiudadano("Luciano", "Graziani", "35591234", "lgraziani@unrn.edu.ar");
			api.nuevoCiudadano("Mauro", "Cambarieri", "25252525", "mcambarieri@unrn.edu.ar");
			api.nuevoCiudadano("Enrique", "Molinari", "28282828", "mmolinari@unrn.edu.ar");
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	// --------------------------------------------------------------------------
	// Todas las creaciones dependientes
	// --------------------------------------------------------------------------
	@Test
	public void b_reclamar() {
		try {
			ObjectContainer objectDb = Db4oUtil.openObjectDb();
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(objectDb);
			CategoriaDAO categoriaDAO = new CategoriaDAO(objectDb);
			List<Ciudadano> ciudadanosTemp = ciudadanoDAO.findAll();
			List<UUID> ciudadanos = new ArrayList<>();
			List<UUID> categorias = new ArrayList<>();
			for (Ciudadano ciudadano : ciudadanosTemp) {
				ciudadanos.add(ciudadano.getId());
			}
			List<Categoria> categoriasTemp = categoriaDAO.findAll();
			for (Categoria categoria : categoriasTemp) {
				categorias.add(categoria.getId());
			}
			Db4oUtil.closeObjectDb();

			// Test
			for (int i = 0; i < 4; i++) {
				UUID ciudadanoId = ciudadanos.get(i);
				UUID categoriaId = categorias.get(i);
				api.reclamar(ciudadanoId, categoriaId, "dlfjshdkjfh sdfsd", "Reclamo dlfjhsdfkshgdfkhsgdjf");
			}
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void c_canjear() {
		try {
			ObjectContainer objectDb = Db4oUtil.openObjectDb();
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(objectDb);
			ProductoDAO productoDAO = new ProductoDAO(objectDb);
			List<Ciudadano> ciudadanosTemp = ciudadanoDAO.findAll();
			List<UUID> ciudadanos = new ArrayList<>();
			List<UUID> productos = new ArrayList<>();
			for (Ciudadano ciudadano : ciudadanosTemp) {
				ciudadanos.add(ciudadano.getId());
			}
			List<Producto> productosTemp = productoDAO.findAll();
			for (Producto producto : productosTemp) {
				productos.add(producto.getId());
			}
			Db4oUtil.closeObjectDb();

			// Test
			for (int i = 0; i < 4; i++) {
				UUID ciudadanoId = ciudadanos.get(i);
				UUID productoId = productos.get(i);
				api.canjear(ciudadanoId, productoId);
			}
			assertTrue(true);
		} catch (RuntimeException e) {
			if (!e.getMessage().equals("No tiene puntos suficientes para canjear el producto")) {
				fail("Error");
			} else {
				e.printStackTrace();
			}
		}
	}

	@Test
	public void d_agregarEvento() {
		try {
			ObjectContainer objectDb = Db4oUtil.openObjectDb();
			ReclamoDAO reclamoDAO = new ReclamoDAO(objectDb);
			List<Reclamo> reclamosTemp = reclamoDAO.findAll();
			List<UUID> reclamos = new ArrayList<>();
			for (Reclamo reclamo : reclamosTemp) {
				reclamos.add(reclamo.getId());
			}

			// Test
			for (int i = 0; i < 4; i++) {
				UUID reclamoId = reclamos.get(i);
				api.agregarEvento(reclamoId, "Evento asjkdhakjs");
			}
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	// --------------------------------------------------------------------------
	// Todas las lecturas
	// --------------------------------------------------------------------------
	@Test
	public void e_listarCategorias() {
		try {
			List<CategoriaDTO> categorias = api.listarCategorias();
			assertTrue(categorias.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_listarProductos() {
		try {
			List<ProductoDTO> productos = api.listarProductos();
			assertTrue(productos.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_listarCiudadanos() {
		try {
			List<CiudadanoDTO> ciudadanos = api.listarCiudadanos();
			assertTrue(ciudadanos.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_listarCanjes() {
		try {
			List<CiudadanoDTO> ciudadanosDTO = api.listarCiudadanos();
			for (CiudadanoDTO ciudadanoDTO : ciudadanosDTO) {
				List<CanjeDTO> canjes = api.listarCanjes(ciudadanoDTO.getId());
				assertTrue(canjes.size() == 1 || canjes.size() == 0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_traerReclamo() {
		try {
			List<ReclamoDTO> reclamosDTO = api.listarReclamos();
			for (ReclamoDTO reclamoDTO : reclamosDTO) {
				ReclamoDTO reclamo = api.traerReclamo(reclamoDTO.getId());
				assertTrue(reclamo.getId().equals(reclamoDTO.getId()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_traerEventos() {
		try {
			List<ReclamoDTO> reclamosDTO = api.listarReclamos();
			for (ReclamoDTO reclamoDTO : reclamosDTO) {
				List<EventoDTO> eventosDTO = api.traerEventos(reclamoDTO.getId());
				assertTrue(eventosDTO.size() == 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_traerReclamoConEventos() {
		try {
			List<ReclamoDTO> reclamosDTO = api.listarReclamos();
			for (ReclamoDTO reclamoDTO : reclamosDTO) {
				ReclamoDTO reclamo = api.traerReclamoConEventos(reclamoDTO.getId());
				assertTrue(reclamo.getEventos().size() == 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_reclamoEntreFechas() {
		try {
			List<ReclamoDTO> reclamosDTO = api.reclamoEntreFechas(LocalDate.now(), null);
			assertTrue(reclamosDTO.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_canjearonProductoElDia() {
		try {
			List<ProductoDTO> productosDTO = api.listarProductos();
			for (ProductoDTO productoDTO : productosDTO) {
				List<CiudadanoDTO> ciudadanosDTO = api.canjearonProductoElDia(productoDTO.getId(), LocalDate.now());
				assertTrue(ciudadanosDTO.size() == 1 || ciudadanosDTO.size() == 0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_conReclamosDeCategoria() {
		try {
			List<CategoriaDTO> categoriasDTO = api.listarCategorias();
			for (CategoriaDTO categoriaDTO : categoriasDTO) {
				List<CiudadanoDTO> ciudadanosDTO = api.conReclamosDeCategoria(categoriaDTO.getId());
				assertTrue(ciudadanosDTO.size() == 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_listarProductosQueFueronCanjeadosEnFecha() {
		try {
			List<ProductoDTO> productosDTO = api.listarProductosQueFueronCanjeadosEnFecha(LocalDate.now());
			assertTrue(productosDTO.size() == 3);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_productosMasCanjeados() {
		try {
			List<ProductoDTO> productos = api.productosMasCanjeados();
			assertTrue(productos.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void e_listarEnOrdenDeCantidadDeCanjes() {
		try {
			Map<Integer, CiudadanoDTO> ciudadanos = api.listarEnOrdenDeCantidadDeCanjes();
			assertTrue(ciudadanos.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	// --------------------------------------------------------------------------
	// Modificaciones
	// --------------------------------------------------------------------------
	@Test
	public void f_modificarCategoria() {
		try {
			List<CategoriaDTO> categoriasDTO = api.listarCategorias();
			for (int i = 1; i <= categoriasDTO.size(); i++) {
				CategoriaDTO categoria = categoriasDTO.get(i - 1);
				categoria.setNombre("Modificada " + i);
				api.modificarCategoria(categoria.getId(), categoria.getNombre(), null);
			}
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void f_modificarProducto() {
		try {
			List<ProductoDTO> productosDTO = api.listarProductos();
			for (int i = 1; i <= productosDTO.size(); i++) {
				ProductoDTO producto = productosDTO.get(i - 1);
				producto.setNombre("Modificado " + i);
				api.modificarProducto(producto.getId(), producto.getNombre(), null);
			}
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	@Test
	public void f_modificarCiudadano() {
		try {
			List<CiudadanoDTO> ciudadanosDTO = api.listarCiudadanos();
			for (int i = 1; i <= ciudadanosDTO.size(); i++) {
				CiudadanoDTO ciudadano = ciudadanosDTO.get(i - 1);
				ciudadano.setNombre("Modificado " + i);
				api.modificarCiudadano(ciudadano.getId(), ciudadano.getNombre(), null, null);
			}
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error");
		}
	}

	// --------------------------------------------------------------------------
	// Modificaciones
	// --------------------------------------------------------------------------
	// Nota: ¿Es realmente necesario implementar un mecanismo de borrado cuando
	// los dos objetos más importantes (Canjes y Reclamos) no pueden ser
	// borrados? Y los tres objetos aquí debajo dependen de ellos.
	@Test
	public void g_bajaCategoria() {
		try {
			List<CategoriaDTO> categoriasDTO = api.listarCategorias();
			for (CategoriaDTO categoria : categoriasDTO) {
				api.bajaCategoria(categoria.getId());
			}
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue("Borre los objetos relacionados al que intentó borrar antes de hacerlo", true);
		}
	}

	@Test
	public void g_bajaProducto() {
		try {
			List<ProductoDTO> productosDTO = api.listarProductos();
			for (ProductoDTO producto : productosDTO) {
				api.bajaProducto(producto.getId());
			}
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(e.getMessage().equals("Borre los objetos relacionados al que intentó borrar antes de hacerlo"));
		}
	}

	@Test
	public void g_bajaCiudadano() {
		try {
			List<CiudadanoDTO> ciudadanosDTO = api.listarCiudadanos();
			for (CiudadanoDTO ciudadano : ciudadanosDTO) {
				api.bajaCiudadano(ciudadano.getId());
			}
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(e.getMessage().equals("Borre los objetos relacionados al que intentó borrar antes de hacerlo"));
		}
	}

}
