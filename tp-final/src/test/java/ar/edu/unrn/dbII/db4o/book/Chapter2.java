package ar.edu.unrn.dbII.db4o.book;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;

import ar.edu.unrn.dbII.modelo.Producto;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Chapter2 {
	private static ObjectContainer db;
	private static final String dbName = "src/main/resources/db4o-test.db4o";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		new File(dbName).delete();
		db = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), dbName);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		db.close();
	}

	@Test
	public void a_storeProducts() {
		for (int i = 0; i < 5; i++) {
			Producto producto = new Producto("producto " + i, 123);
			db.store(producto);
		}
		System.out.println("The products have been stored.");
	}

	@Test
	public void b_retrieveAll() {
		List<Producto> productos = db.query(Producto.class);
		assertTrue(productos.size() == 5);
		System.out.println("The products have been retrieved.");
	}

	/**
	 * So, when you wanna search something, you don't fetch by name, by x attr,
	 * you fetch for everything and nothing at the same time. This means, the
	 * driver's clever enough to know if an attr is filled, and search by that.
	 */
	@Test
	public void c_retrieveByName() {
		Producto producto = new Producto(null, "producto 1", null);
		List<Producto> productos = db.queryByExample(producto);
		assertTrue(productos.size() == 1);
		System.out.println("Product 1 has been retrieved.");
	}

	@Test
	public void d_update() {
		// Recupero un producto por el nombre
		Producto producto = new Producto(null, "producto 1", null);
		List<Producto> productos = db.queryByExample(producto);
		producto = productos.get(0);

		// Verifico que tiene el costo viejo
		assertTrue(producto.getPuntajeCosto() == 123);
		// Actualizo el costo
		producto.update(null, 321);
		// Lo guardo
		db.store(producto);

		// Recupero la colección de productos
		productos = db.query(Producto.class);
		// Verifico que sigue habiendo 5 productos
		assertTrue(productos.size() == 5);

		// Creo el producto vacío que se utilizará para buscar
		producto = new Producto(null, "producto 1", null);
		// Chequeo que tiene el puntaje actualizado
		assertTrue(((Producto) db.queryByExample(producto).get(0)).getPuntajeCosto() == 321);
	}
}
