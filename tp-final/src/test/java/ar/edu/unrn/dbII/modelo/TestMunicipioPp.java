package ar.edu.unrn.dbII.modelo;

import org.junit.Assert;
import org.junit.Test;

import com.db4o.ObjectContainer;

import ar.edu.unrn.dbII.clearDb.ClearDb;
import ar.edu.unrn.dbII.dao.impl.MunicipioPpDAO;
import ar.edu.unrn.dbII.utils.Db4oUtil;

public class TestMunicipioPp extends ClearDb {

	@Test
	public void testGetInstance() {
		ObjectContainer db = Db4oUtil.openObjectDb();
		try {
			MunicipioPp municipio = MunicipioPpDAO.getInstance(db);
			Assert.assertTrue(municipio != null);
			Db4oUtil.closeObjectDb();
			municipio = null;
			db = Db4oUtil.openObjectDb();
			municipio = db.query(MunicipioPp.class).get(0);
			Assert.assertTrue(municipio != null);
		} finally {
			Db4oUtil.closeObjectDb();
		}
	}

}
