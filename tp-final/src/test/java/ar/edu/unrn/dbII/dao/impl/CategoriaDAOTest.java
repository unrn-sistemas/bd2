package ar.edu.unrn.dbII.dao.impl;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.db4o.ObjectContainer;
import com.db4o.query.Predicate;

import ar.edu.unrn.dbII.clearDb.ClearDb;
import ar.edu.unrn.dbII.modelo.Categoria;
import ar.edu.unrn.dbII.utils.EntityUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CategoriaDAOTest extends ClearDb {

	@Test
	public void a_persist() {
		Categoria categoria = new Categoria("Nueva Categoria", 123);
		try {
			EntityUtil.sendToDB((ObjectContainer objectDb) -> MunicipioPpDAO.getInstance(objectDb).add(categoria));
		} catch (Exception e) {
			fail("Error");
			e.printStackTrace();
		}
	}

	@Test
	public void b_findAll() {
		try {
			EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
				CategoriaDAO categoriaDAO = new CategoriaDAO(objectDb);
				List<Categoria> categorias = categoriaDAO.findAll();
				assertTrue(categorias.size() > 0);

				Categoria categoria = categoriaDAO.find(categorias.get(0).getId());
				Assert.assertTrue(categoria != null);

				return null;
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

	@SuppressWarnings("serial")
	@Test
	public void c_testModify() {
		try {
			EntityUtil.sendToDB((ObjectContainer objectDb) -> {
				CategoriaDAO categoriaDAO = new CategoriaDAO(objectDb);
				Categoria categoria2 = categoriaDAO.findAll().get(0);
				Assert.assertTrue(categoria2.getPuntaje() == 123);
				categoria2.update(null, 321);
			});
			EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
				List<Categoria> categorias = objectDb.query(new Predicate<Categoria>() {
					public boolean match(Categoria categoria) {
						return categoria.getPuntaje() == 321;
					}
				});
				Assert.assertTrue(categorias.size() > 0);
				return categorias;
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

	@Test
	public void d_testRemove() {
		try {
			Integer cant = EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
				return MunicipioPpDAO.getInstance(objectDb).getCategorias().size();
			});
			EntityUtil.sendToDB((ObjectContainer objectDb) -> {
				CategoriaDAO categoriaDAO = new CategoriaDAO(objectDb);
				List<Categoria> categorias = categoriaDAO.findAll();
				MunicipioPpDAO.getInstance(objectDb).remove(categorias.get(0));
			});
			EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
				Integer cant2 = MunicipioPpDAO.getInstance(objectDb).getCategorias().size();
				Assert.assertTrue(cant > cant2);
				return null;
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

}
