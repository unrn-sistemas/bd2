package ar.edu.unrn.dbII.db4o.book;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.query.Predicate;

import ar.edu.unrn.dbII.modelo.Producto;

@SuppressWarnings("serial")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Chapter4 {
	private static ObjectContainer db;
	private static final String dbName = "src/main/resources/db4o-test.db4o";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		new File(dbName).delete();
		db = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), dbName);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		db.close();
	}

	@Test
	public void a_storeProducts() {
		for (int i = 0; i < 5; i++) {
			Producto producto = new Producto("producto " + i, 123);
			db.store(producto);
		}
		System.out.println("The products have been stored.");
	}

	@Test
	public void b_simpleNativeQuery() {
		List<Producto> productos = db.query(new Predicate<Producto>() {
			public boolean match(Producto producto) {
				return producto.getPuntajeCosto() == 123;
			}
		});

		assertTrue(productos.size() == 5);
	}

	@Test
	public void c_complexNativeQuery() {
		List<Producto> productos = db.query(new Predicate<Producto>() {
			public boolean match(Producto producto) {
				return producto.getPuntajeCosto() > 1 && producto.getPuntajeCosto() < 122
						|| producto.getNombre().equals("producto 1");
			}
		});

		assertTrue(productos.size() == 1);
	}
}
