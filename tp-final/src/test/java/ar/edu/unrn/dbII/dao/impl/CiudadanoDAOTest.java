package ar.edu.unrn.dbII.dao.impl;

import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.db4o.ObjectContainer;

import ar.edu.unrn.dbII.clearDb.ClearDb;
import ar.edu.unrn.dbII.modelo.Canje;
import ar.edu.unrn.dbII.modelo.Categoria;
import ar.edu.unrn.dbII.modelo.Ciudadano;
import ar.edu.unrn.dbII.modelo.Producto;
import ar.edu.unrn.dbII.utils.EntityUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CiudadanoDAOTest extends ClearDb {
	public static final Integer categoriaPuntaje = 123;
	public static final Integer productoPuntaje = 5;

	@Test
	public void a_persist() {
		Ciudadano ciudadano = new Ciudadano("Luciano", "Graziani", "35591234", "lgraziani2712@gmail.com");
		try {
			EntityUtil.sendToDB((ObjectContainer objectDb) -> MunicipioPpDAO.getInstance(objectDb).add(ciudadano));
		} catch (Exception e) {
			fail("Error");
			e.printStackTrace();
		}
	}

	@Test
	public void b_findAll() {
		try {
			EntityUtil.sendToDB((ObjectContainer objectDb) -> {
				CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(objectDb);
				List<Ciudadano> ciudadanos = ciudadanoDAO.findAll();
				Assert.assertTrue(ciudadanos.size() > 0);

				Ciudadano ciudadano = ciudadanoDAO.find(ciudadanos.get(0).getId());
				Assert.assertTrue(ciudadano != null);
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

	@Test
	public void c_reclamar() {
		try {
			EntityUtil.sendToDB((ObjectContainer objectDb) -> {
				CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(objectDb);
				Ciudadano ciudadano = ciudadanoDAO.findAll().get(0);
				Categoria categoria = new Categoria("Categoria nueva", categoriaPuntaje);
				MunicipioPpDAO.getInstance(objectDb).add(categoria);
				ciudadano.reclamar("ASjdhf", "fdjshdkfjh", categoria);
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

	@Test
	public void d_canjear() {
		try {
			EntityUtil.sendToDB((ObjectContainer objectDb) -> {
				CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(objectDb);
				Ciudadano ciudadano = ciudadanoDAO.findAll().get(0);
				Producto productoAccesible = new Producto("Producto accesible", productoPuntaje);
				Producto productoCaro = new Producto("Producto caro", 500);
				MunicipioPpDAO.getInstance(objectDb).add(productoAccesible);
				MunicipioPpDAO.getInstance(objectDb).add(productoCaro);
				ciudadano.canjear(productoAccesible);
				Assert.assertTrue(
						"¿El puntaje restante del ciudadano es igual al puntaje de la Categoría menos al del Producto?",
						ciudadano.getPuntajeAcumulado() == (categoriaPuntaje - productoPuntaje));
				try {
					ciudadano.canjear(productoCaro);
				} catch (RuntimeException ex) {
					Assert.assertTrue(ex.getMessage().equals("No tiene puntos suficientes para canjear el producto"));
				}
			});
			EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
				List<Canje> canjes = objectDb.query(Canje.class);
				Assert.assertTrue(canjes.size() > 0);
				return null;
			});
		} catch (RuntimeException e) {
			if (!e.getMessage().equals("No tiene puntos suficientes para canjear el producto")) {
				fail("Falló");
				e.printStackTrace();
			}
		}
	}

	// --------------------------------------------------
	// Métodos propios
	// --------------------------------------------------
	@Test
	public void e_testListarCanjes() {
		EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(objectDb);
			UUID id = ciudadanoDAO.findAll().get(0).getId();
			List<Canje> canjes = ciudadanoDAO.listarCanjes(id);
			Assert.assertTrue(canjes.size() > 0);

			return null;
		});
	}

	@Test
	public void f_testCanjearonProductoElDia() {
		EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
			List<Producto> productos = objectDb.queryByExample(new Producto(null, null, productoPuntaje));
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(objectDb);
			List<Ciudadano> ciudadanos = ciudadanoDAO.canjearonProductoElDia(productos.get(0), LocalDate.now());
			Assert.assertTrue(ciudadanos.size() > 0);

			return null;
		});
	}

	@Test
	public void g_testConReclamosDeCategoria() {
		EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
			List<Categoria> categorias = objectDb.queryByExample(new Categoria(null, null, categoriaPuntaje));
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(objectDb);
			List<Ciudadano> ciudadanos = ciudadanoDAO.conReclamosDeCategoria(categorias.get(0));
			Assert.assertTrue(ciudadanos.size() > 0);

			return null;
		});
	}

	@Test
	public void h_testProductosMasCanjeados() {
		EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(objectDb);
			List<Producto> productos = ciudadanoDAO.productosMasCanjeados();
			Assert.assertTrue(productos.size() > 0);

			return null;
		});
	}

	@Test
	public void i_testListarEnOrdenDeCantidadDeCanjes() {
		EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(objectDb);
			Map<Ciudadano, Integer> ciudadanos = ciudadanoDAO.listarEnOrdenDeCantidadDeCanjes();
			Assert.assertTrue(ciudadanos.size() > 0);

			return null;
		});
	}

	@Test
	public void j_testListarProductosQueFueronCanjeadosEnFecha() {
		EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(objectDb);
			List<Producto> productos = ciudadanoDAO.listarProductosQueFueronCanjeadosPorCiudadanosQueReclamaronEn(LocalDate.now());
			Assert.assertTrue(productos.size() > 0);

			return null;
		});
	}

	// --------------------------------------------------

	@Test
	public void z_remove() {
		try {
			Integer cant = EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
				return MunicipioPpDAO.getInstance(objectDb).getCiudadanos().size();
			});
			EntityUtil.sendToDB((ObjectContainer objectDb) -> {
				CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(objectDb);
				Ciudadano ciudadano = ciudadanoDAO.findAll().get(0);
				MunicipioPpDAO.getInstance(objectDb).remove(ciudadano);
			});
			EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
				Integer cant2 = MunicipioPpDAO.getInstance(objectDb).getCiudadanos().size();
				Assert.assertTrue(cant > cant2);
				return null;
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

}
