package ar.edu.unrn.dbII.dto;

import java.util.UUID;

import org.bson.Document;
import org.bson.json.JsonWriterSettings;
import org.junit.Assert;
import org.junit.Test;

import ar.edu.unrn.dbII.modelo.Ciudadano;

public class TestCiudadanoDTO {

	@Test
	public void testToDocument() {
		try {
			CiudadanoDTO ciudadano = new CiudadanoDTO(
					new Ciudadano(UUID.randomUUID(), "luciano", "graziani", "35591234", "lgraziani2712@gmail.com", 0));
			Document document = ciudadano.toDocument();
			String json = document.toJson(new JsonWriterSettings(true));
			System.out.println(json);
			Assert.assertTrue(true);

			CiudadanoDTO ciudadanoCopia = new CiudadanoDTO(document);
			Assert.assertTrue(ciudadano.getId().equals(ciudadanoCopia.getId()));
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail("Falló");
		}
	}

}
