package ar.edu.unrn.dbII.dto;

import java.time.LocalDate;
import java.util.UUID;

import org.bson.Document;
import org.bson.json.JsonWriterSettings;
import org.junit.Assert;
import org.junit.Test;

import ar.edu.unrn.dbII.modelo.Categoria;
import ar.edu.unrn.dbII.modelo.Ciudadano;
import ar.edu.unrn.dbII.modelo.Evento;
import ar.edu.unrn.dbII.modelo.Reclamo;

public class TestReclamoDTO {

	@Test
	public void testToDocument() {
		try {
			Reclamo reclamo = new Reclamo(UUID.randomUUID(), LocalDate.now(), "Del Pehuén 1597",
					"No me anda el internet", new Ciudadano(UUID.randomUUID(), null, null, null, null, null),
					new Categoria(UUID.randomUUID(), null, null));
			reclamo.agregarEvento(new Evento(UUID.randomUUID(), "Ola ké asé", LocalDate.now(), reclamo));
			ReclamoDTO reclamoDTO = new ReclamoDTO(reclamo);
			Document document = reclamoDTO.toDocument();
			String json = document.toJson(new JsonWriterSettings(true));
			System.out.println(json);
			Assert.assertTrue(true);

			ReclamoDTO reclamoCopia = new ReclamoDTO(document);
			Assert.assertTrue(reclamoCopia.getEventos().get(0).getId().equals(reclamoDTO.getEventos().get(0).getId()));
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail("Falló");
		}
	}

}
