package ar.edu.unrn.dbII.dao.impl;

import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.db4o.ObjectContainer;
import com.db4o.query.Predicate;

import ar.edu.unrn.dbII.clearDb.ClearDb;
import ar.edu.unrn.dbII.modelo.Producto;
import ar.edu.unrn.dbII.utils.EntityUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductoDAOTest extends ClearDb {

	@Test
	public void a_persist() {
		Producto producto = new Producto("Nuevo Producto", 123);
		try {
			EntityUtil.sendToDB((ObjectContainer objectDb) -> MunicipioPpDAO.getInstance(objectDb).add(producto));
		} catch (Exception e) {
			fail("Error");
			e.printStackTrace();
		}
	}

	@Test
	public void b_testFindAll() {
		try {
			EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
				ProductoDAO productoDAO = new ProductoDAO(objectDb);
				List<Producto> productos = productoDAO.findAll();
				Assert.assertTrue(productos.size() > 0);

				Producto producto = productoDAO.find(productos.get(0).getId());
				Assert.assertTrue(producto != null);

				return productos;
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

	@SuppressWarnings("serial")
	@Test
	public void c_testModify() {
		try {
			EntityUtil.sendToDB((ObjectContainer objectDb) -> {
				ProductoDAO productoDAO = new ProductoDAO(objectDb);
				Producto producto2 = productoDAO.findAll().get(0);
				Assert.assertTrue(producto2.getPuntajeCosto() == 123);
				producto2.update(null, 321);
			});
			EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
				List<Producto> productos = objectDb.query(new Predicate<Producto>() {
					public boolean match(Producto producto) {
						return producto.getPuntajeCosto() == 321;
					}
				});
				Assert.assertTrue(productos.size() > 0);
				return productos;
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

	@Test
	public void d_testRemove() {
		try {
			Integer cant = EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
				return MunicipioPpDAO.getInstance(objectDb).getProductos().size();
			});
			EntityUtil.sendToDB((ObjectContainer objectDb) -> {
				ProductoDAO productoDAO = new ProductoDAO(objectDb);
				Producto producto = productoDAO.findAll().get(0);
				MunicipioPpDAO.getInstance(objectDb).remove(producto);
			});
			EntityUtil.retrieveFromDB((ObjectContainer objectDb) -> {
				Integer cant2 = MunicipioPpDAO.getInstance(objectDb).getProductos().size();
				Assert.assertTrue(cant > cant2);
				return null;
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

}
