package ar.edu.unrn.dbII.clearDb;

import java.io.File;

import org.junit.BeforeClass;

import ar.edu.unrn.dbII.utils.Db4oUtil;

public class ClearDb {

	@BeforeClass
	public static void beforeStarts() {
		new File("src/main/resources/db4o-test.db4o").delete();
		Db4oUtil.closeObjectDb();
	}
}
