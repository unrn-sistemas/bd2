package ar.edu.unrn.dbII.dto;

import java.util.UUID;

import org.bson.Document;
import org.bson.json.JsonWriterSettings;
import org.junit.Assert;
import org.junit.Test;

import ar.edu.unrn.dbII.modelo.Categoria;

public class TestCategoriaDTO {

	@Test
	public void testToDocument() {
		try {
			CategoriaDTO categoria = new CategoriaDTO(new Categoria(UUID.randomUUID(), "Categoría", 123));
			Document document = categoria.toDocument();
			String json = document.toJson(new JsonWriterSettings(true));
			System.out.println(json);
			Assert.assertTrue(true);

			CategoriaDTO categoriaCopia = new CategoriaDTO(document);
			Assert.assertTrue(categoria.getId().equals(categoriaCopia.getId()));
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail("Falló");
		}
	}

}
