package ar.edu.unrn.dbII.utils;

import com.db4o.ObjectContainer;

public interface Command {
	void execute(ObjectContainer objectDb);
}
