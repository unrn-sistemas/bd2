package ar.edu.unrn.dbII.dao.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.db4o.ObjectContainer;
import com.db4o.query.Predicate;

import ar.edu.unrn.dbII.modelo.Canje;
import ar.edu.unrn.dbII.modelo.Categoria;
import ar.edu.unrn.dbII.modelo.Ciudadano;
import ar.edu.unrn.dbII.modelo.Producto;
import ar.edu.unrn.dbII.modelo.Reclamo;
import ar.edu.unrn.dbII.utils.MapOrderer;

@SuppressWarnings("serial")
public class CiudadanoDAO extends GenericDAO<Ciudadano> {

	public CiudadanoDAO(ObjectContainer objectDb) {
		super(objectDb);
	}

	// --------------------------------------------------
	// Métodos hook
	// --------------------------------------------------
	@Override
	protected Class<Ciudadano> getActualClass() {
		return Ciudadano.class;
	}

	@Override
	protected Ciudadano getPrototype(UUID id) {
		return new Ciudadano(id, null, null, null, null, null);
	}

	// --------------------------------------------------
	// Métodos propios
	// --------------------------------------------------
	public List<Canje> listarCanjes(UUID idCiudadano) {
		List<Canje> canjes = objectDb.query(new Predicate<Canje>() {
			public boolean match(Canje canje) {
				return canje.getCiudadano().getId() == idCiudadano;
			}
		});
		return canjes;
	}

	// 1. Listar los ciudadanos que canjearon el Producto <X> el día <DD/MM/YYY>
	public List<Ciudadano> canjearonProductoElDia(Producto producto, LocalDate fecha) {
		List<Ciudadano> ciudadanos = objectDb.query(new Predicate<Ciudadano>() {
			public boolean match(Ciudadano ciudadano) {
				for (Canje canje : ciudadano.getCanjes())
					return canje.getProducto().getId().equals(producto.getId()) && canje.getFecha().equals(fecha);

				return false;
			}
		});
		return ciudadanos;
	}

	// 2. Listar los Ciudadanos que generaron reclamos con la categoría <X>.
	public List<Ciudadano> conReclamosDeCategoria(Categoria categoria) {
		List<Ciudadano> ciudadanos = objectDb.query(new Predicate<Ciudadano>() {
			public boolean match(Ciudadano ciudadano) {
				for (Reclamo reclamo : ciudadano.getReclamos())
					return reclamo.getCategoria().getId().equals(categoria.getId());

				return false;
			}
		});

		return ciudadanos;
	}

	// 4. Liste los productos más canjeados
	public List<Producto> productosMasCanjeados() {
		Map<UUID, Integer> cantidades = new HashMap<>();
		List<UUID> idsDeLosproductosMasCanjeados = new ArrayList<>();
		Integer valorMaximo = 0;

		// En cada canje, cuenta la cantidad de veces que se repite un producto
		for (Canje canje : objectDb.query(Canje.class)) {
			UUID id = canje.getProducto().getId();
			if (!cantidades.containsKey(id))
				cantidades.put(id, 0);
			cantidades.put(id, cantidades.get(id) + 1);
		}

		// Ordena el mapa de mayor a menor
		cantidades = MapOrderer.sortByValue(cantidades);
		valorMaximo = cantidades.values().iterator().next();
		// Guarda los ID de todos los productos con igual valor que el máximo
		for (UUID id : cantidades.keySet())
			if (cantidades.get(id) == valorMaximo)
				idsDeLosproductosMasCanjeados.add(id);

		List<Producto> productos = objectDb.query(new Predicate<Producto>() {
			@Override
			public boolean match(Producto producto) {
				// Recupera todos los productos que coinciden están dentro de
				return idsDeLosproductosMasCanjeados.contains(producto.getId());
			}
		});

		return productos;
	}

	public Map<Ciudadano, Integer> listarEnOrdenDeCantidadDeCanjes() {
		Map<Ciudadano, Integer> ciudadanos = new HashMap<>();

		// En cada canje, cuenta la cantidad de veces que se repite un producto
		for (Canje canje : objectDb.query(Canje.class)) {
			Ciudadano ciudadano = canje.getCiudadano();
			if (!ciudadanos.containsKey(ciudadano))
				ciudadanos.put(ciudadano, 0);
			ciudadanos.put(ciudadano, ciudadanos.get(ciudadano) + 1);
		}
		ciudadanos = MapOrderer.sortByValue(ciudadanos);

		return ciudadanos;
	}

	// 3. Listar los productos canjeados por aquellos ciudadanos que iniciaron
	// reclamos en MM/YYYY
	public List<Producto> listarProductosQueFueronCanjeadosPorCiudadanosQueReclamaronEn(LocalDate fecha) {
		List<Producto> productos = new ArrayList<>();
		List<Ciudadano> ciudadanos = objectDb.query(new Predicate<Ciudadano>() {
			@Override
			public boolean match(Ciudadano ciudadano) {
				for (Reclamo reclamo : ciudadano.getReclamos()) {
					return reclamo.getFecha().isEqual(fecha);
				}
				return false;
			}
		});

		for (Ciudadano ciudadano : ciudadanos) {
			for (Canje canje : ciudadano.getCanjes()) {
				productos.add(canje.getProducto());
			}
		}

		return productos;
	}

}
