package ar.edu.unrn.dbII.dao.impl;

import java.util.UUID;

import com.db4o.ObjectContainer;

import ar.edu.unrn.dbII.modelo.Producto;

public class ProductoDAO extends GenericDAO<Producto> {

	public ProductoDAO(ObjectContainer objectDb) {
		super(objectDb);
	}

	@Override
	protected Class<Producto> getActualClass() {
		return Producto.class;
	}

	@Override
	protected Producto getPrototype(UUID id) {
		return new Producto(id, null, null);
	}

}
