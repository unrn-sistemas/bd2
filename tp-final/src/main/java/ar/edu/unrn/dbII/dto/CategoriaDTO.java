package ar.edu.unrn.dbII.dto;

import java.util.UUID;

import org.bson.Document;

import ar.edu.unrn.dbII.modelo.Categoria;

public class CategoriaDTO extends GenericDTO {
	private String nombre;
	private Integer puntaje;

	public CategoriaDTO() {
	}

	public CategoriaDTO(Categoria categoria) {
		this.id = categoria.getId();
		this.nombre = categoria.getNombre();
		this.puntaje = categoria.getPuntaje();
	}

	public CategoriaDTO(Document document) {
		this.id = document.get("id", UUID.class);
		this.nombre = document.getString("nombre");
		this.puntaje = document.getInteger("puntaje");
	}

	public Document toDocument() {
		Document document = super.toDocument();
		document.append("nombre", nombre).append("puntaje", puntaje);
		return document;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(Integer puntaje) {
		this.puntaje = puntaje;
	}

}
