package ar.edu.unrn.dbII.utils;

import com.db4o.ObjectContainer;

public interface Query<T> {
	T execute(ObjectContainer objectDb);
}
