package ar.edu.unrn.dbII.modelo;

import java.util.ArrayList;
import java.util.List;

import com.db4o.activation.ActivationPurpose;
import com.db4o.activation.Activator;
import com.db4o.ta.Activatable;

public class MunicipioPp implements Activatable {

	private int id;
	private List<Ciudadano> ciudadanos;
	private List<Categoria> categorias;
	private List<Producto> productos;
	private transient Activator activator;

	public MunicipioPp() {
		id = 1;
		ciudadanos = new ArrayList<>();
		categorias = new ArrayList<>();
		productos = new ArrayList<>();
	}

	public MunicipioPp(int id, List<Ciudadano> ciudadanos, List<Categoria> categorias, List<Producto> productos) {
		this.id = id;
		this.ciudadanos = ciudadanos;
		this.categorias = categorias;
		this.productos = productos;
	}

	public int getId() {
		activate(ActivationPurpose.READ);
		return id;
	}

	public List<Ciudadano> getCiudadanos() {
		activate(ActivationPurpose.READ);
		return ciudadanos;
	}

	public List<Categoria> getCategorias() {
		activate(ActivationPurpose.READ);
		return categorias;
	}

	public List<Producto> getProductos() {
		activate(ActivationPurpose.READ);
		return productos;
	}

	public void add(Categoria categoria) {
		activate(ActivationPurpose.WRITE);
		if (!categorias.contains(categoria)) {
			categorias.add(categoria);
		}
	}

	public void remove(Categoria categoria) {
		activate(ActivationPurpose.WRITE);
		if (categorias.contains(categoria)) {
			categorias.remove(categoria);
		}
	}

	public void add(Producto producto) {
		activate(ActivationPurpose.WRITE);
		if (!productos.contains(producto)) {
			productos.add(producto);
		}
	}

	public void remove(Producto producto) {
		activate(ActivationPurpose.WRITE);
		if (productos.contains(producto)) {
			productos.remove(producto);
		}
	}

	public void add(Ciudadano ciudadano) {
		activate(ActivationPurpose.WRITE);
		if (!ciudadanos.contains(ciudadano)) {
			ciudadanos.add(ciudadano);
		}
	}

	public void remove(Ciudadano ciudadano) {
		activate(ActivationPurpose.WRITE);
		if (ciudadanos.contains(ciudadano)) {
			ciudadanos.remove(ciudadano);
		}
	}

	@Override
	public void activate(ActivationPurpose purpose) {
		if (activator != null) {
			activator.activate(purpose);
		}
	}

	@Override
	public void bind(Activator activator) {
		if (this.activator == activator) {
			return;
		}
		if (this.activator != null && activator != null) {
			throw new IllegalStateException();
		}
		this.activator = activator;
	}

}
