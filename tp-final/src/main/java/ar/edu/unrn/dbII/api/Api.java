package ar.edu.unrn.dbII.api;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import ar.edu.unrn.dbII.dto.CanjeDTO;
import ar.edu.unrn.dbII.dto.CategoriaDTO;
import ar.edu.unrn.dbII.dto.CiudadanoDTO;
import ar.edu.unrn.dbII.dto.EventoDTO;
import ar.edu.unrn.dbII.dto.ProductoDTO;
import ar.edu.unrn.dbII.dto.ReclamoDTO;

public interface Api {
	/*----------------------------------------------------
	 | Categorías
	 |----------------------------------------------------
	 */
	void nuevaCategoria(String nombre, Integer puntaje) throws RuntimeException;

	void bajaCategoria(UUID id) throws RuntimeException;

	/**
	 * Modifica los datos de la categoría. Nota: la modificación del puntaje no es retroactiva.
	 * 
	 * @param id
	 * @param nuevoNombre
	 *            null able
	 * @param nuevoPuntaje
	 *            null able
	 * @throws RuntimeException
	 */
	void modificarCategoria(UUID id, String nuevoNombre, Integer nuevoPuntaje) throws RuntimeException;

	List<CategoriaDTO> listarCategorias() throws RuntimeException;

	/*----------------------------------------------------
	 | Productos
	 |----------------------------------------------------
	 */

	/**
	 * Modifica los datos del producto. Nota: la modificación del puntaje no es retroactiva.
	 * 
	 * @param nombre
	 *            null able
	 * @param puntajeCosto
	 *            null able
	 * @throws RuntimeException
	 */
	void nuevoProducto(String nombre, Integer puntajeCosto) throws RuntimeException;

	void bajaProducto(UUID id) throws RuntimeException;

	void modificarProducto(UUID id, String nuevoNombre, Integer nuevoPuntajeCosto) throws RuntimeException;

	List<ProductoDTO> listarProductos() throws RuntimeException;

	/*----------------------------------------------------
	 | Ciudadanos
	 |----------------------------------------------------
	 */
	void nuevoCiudadano(String nombre, String apellido, String dni, String email) throws RuntimeException;

	void bajaCiudadano(UUID id) throws RuntimeException;

	/**
	 * 
	 * @param id
	 * @param nombre
	 *            null able
	 * @param apellido
	 *            null able
	 * @param email
	 *            null able
	 * @throws RuntimeException
	 */
	void modificarCiudadano(UUID id, String nombre, String apellido, String email) throws RuntimeException;

	List<CiudadanoDTO> listarCiudadanos() throws RuntimeException;

	List<CiudadanoDTO> canjearonProductoElDia(UUID idProducto, LocalDate fecha) throws RuntimeException;

	List<CiudadanoDTO> conReclamosDeCategoria(UUID idCategoria) throws RuntimeException;

	List<ProductoDTO> listarProductosQueFueronCanjeadosEnFecha(LocalDate fecha) throws RuntimeException;

	List<ProductoDTO> productosMasCanjeados() throws RuntimeException;

	Map<Integer, CiudadanoDTO> listarEnOrdenDeCantidadDeCanjes() throws RuntimeException;

	List<CanjeDTO> listarCanjes(UUID idCiudadano) throws RuntimeException;

	void canjear(UUID idCiudadano, UUID idProducto) throws RuntimeException;

	/*----------------------------------------------------
	 | Reclamos y eventos
	 |----------------------------------------------------
	 */
	void reclamar(UUID idCiudadano, UUID idCategoria, String direccion, String detalle) throws RuntimeException;

	List<ReclamoDTO> listarReclamos() throws RuntimeException;

	ReclamoDTO traerReclamo(UUID idReclamo) throws RuntimeException;

	ReclamoDTO traerReclamoConEventos(UUID idReclamo) throws RuntimeException;

	List<ReclamoDTO> reclamoEntreFechas(LocalDate fechaDesde, LocalDate fechaHasta) throws RuntimeException;

	List<EventoDTO> traerEventos(UUID idReclamo) throws RuntimeException;

	void agregarEvento(UUID idReclamo, String descripcion) throws RuntimeException;

}
