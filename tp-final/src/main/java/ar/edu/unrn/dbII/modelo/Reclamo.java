package ar.edu.unrn.dbII.modelo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.db4o.activation.ActivationPurpose;

public class Reclamo extends GenericModel {
	private LocalDate fecha;
	private String direccion;
	private String detalle;
	private Ciudadano ciudadano;
	private Categoria categoria;
	private List<Evento> eventos = new ArrayList<Evento>();

	public Reclamo(UUID id, LocalDate fecha, String direccion, String detalle, Ciudadano ciudadano,
			Categoria categoria) {
		super(id);
		this.fecha = fecha;
		this.direccion = direccion;
		this.detalle = detalle;
		this.categoria = categoria;
		this.ciudadano = ciudadano;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Reclamo(LocalDate fecha, String direccion, String detalle, Ciudadano ciudadano, Categoria categoria) {
		this.fecha = fecha;
		this.direccion = direccion;
		this.detalle = detalle;
		this.ciudadano = ciudadano;
		this.categoria = categoria;
	}

	public void agregarEvento(Evento evento) {
		activate(ActivationPurpose.WRITE);
		if (!eventos.contains(evento)) {
			eventos.add(evento);
		}
	}

	public LocalDate getFecha() {
		activate(ActivationPurpose.READ);
		return fecha;
	}

	public String getDireccion() {
		activate(ActivationPurpose.READ);
		return direccion;
	}

	public String getDetalle() {
		activate(ActivationPurpose.READ);
		return detalle;
	}

	public Ciudadano getCiudadano() {
		activate(ActivationPurpose.READ);
		return ciudadano;
	}

	public Categoria getCategoria() {
		activate(ActivationPurpose.READ);
		return categoria;
	}

	public List<Evento> getEventos() {
		activate(ActivationPurpose.READ);
		return eventos;
	}
}
