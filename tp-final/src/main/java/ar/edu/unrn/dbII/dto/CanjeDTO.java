package ar.edu.unrn.dbII.dto;

import java.time.LocalDate;
import java.util.UUID;

import org.bson.Document;

import ar.edu.unrn.dbII.modelo.Canje;

public class CanjeDTO extends GenericDTO {
	private UUID ciudadanoId;
	private UUID productoId;
	private LocalDate fecha;

	public CanjeDTO() {
	}

	public CanjeDTO(Canje canje) {
		this.id = canje.getId();
		this.ciudadanoId = canje.getCiudadano().getId();
		this.productoId = canje.getProducto().getId();
		this.fecha = canje.getFecha();
	}

	public CanjeDTO(Document document) {
		this.id = document.get("id", UUID.class);
		this.ciudadanoId = document.get("ciudadanoId", UUID.class);
		this.productoId = document.get("id", UUID.class);
		this.fecha = LocalDate.parse(document.getString("fecha"));
	}

	public Document toDocument() {
		Document document = super.toDocument();
		document.append("ciudadanoId", ciudadanoId).append("productoId", productoId).append("fecha", fecha.toString());
		return document;
	}

	public UUID getCiudadanoId() {
		return ciudadanoId;
	}

	public void setCiudadanoId(UUID ciudadanoId) {
		this.ciudadanoId = ciudadanoId;
	}

	public UUID getProductoId() {
		return productoId;
	}

	public void setProductoId(UUID productoId) {
		this.productoId = productoId;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

}
