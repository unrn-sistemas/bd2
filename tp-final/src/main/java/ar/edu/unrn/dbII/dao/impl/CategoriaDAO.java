package ar.edu.unrn.dbII.dao.impl;

import java.util.UUID;

import com.db4o.ObjectContainer;

import ar.edu.unrn.dbII.modelo.Categoria;

public class CategoriaDAO extends GenericDAO<Categoria> {

	public CategoriaDAO(ObjectContainer objectDb) {
		super(objectDb);
	}

	@Override
	protected Class<Categoria> getActualClass() {
		return Categoria.class;
	}

	@Override
	protected Categoria getPrototype(UUID id) {
		return new Categoria(id, null, null);
	}

}
