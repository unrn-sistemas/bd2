package ar.edu.unrn.dbII.dao.impl;

import java.util.List;

import com.db4o.ObjectContainer;

import ar.edu.unrn.dbII.modelo.MunicipioPp;

public class MunicipioPpDAO {

	/**
	 * Este método verifica en la BD si existe el objeto Root, en caso de no, lo
	 * inserta, en caso de sí, no hace nada. En definitiva, un pattern Singleton
	 * para la persistencia en BD.
	 */
	public static MunicipioPp getInstance(ObjectContainer objectDb) {
		List<MunicipioPp> municipios = objectDb.query(MunicipioPp.class);
		if (municipios.size() == 1)
			return municipios.get(0);
		MunicipioPp municipio = new MunicipioPp();
		objectDb.store(municipio);

		return municipio;
	}

}
