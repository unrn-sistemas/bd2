package ar.edu.unrn.dbII.modelo;

import java.util.UUID;

import com.db4o.activation.ActivationPurpose;

public class Producto extends GenericModel {
	private Integer puntajeCosto;
	private String nombre;

	/**
	 * Constructor para instancias que están en la DB.
	 */
	public Producto(UUID id, String nombre, Integer puntajeCosto) {
		super(id);
		this.nombre = nombre;
		this.puntajeCosto = puntajeCosto;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Producto(String nombre, Integer puntajeCosto) {
		super();
		this.nombre = nombre;
		this.puntajeCosto = puntajeCosto;
	}

	/**
	 * Cualquier parámetro puede ser null.
	 * 
	 * @param nombre
	 *            nullable
	 * @param puntajeCosto
	 *            nullable
	 */
	public void update(String nombre, Integer puntajeCosto) {
		activate(ActivationPurpose.WRITE);
		this.nombre = nombre == null ? this.nombre : nombre;
		this.puntajeCosto = puntajeCosto == null ? this.puntajeCosto : puntajeCosto;
	}

	public String getNombre() {
		activate(ActivationPurpose.READ);
		return nombre;
	}

	public Integer getPuntajeCosto() {
		activate(ActivationPurpose.READ);
		return puntajeCosto;
	}

	protected Boolean canjeable(Integer puntajeAcumulado) {
		activate(ActivationPurpose.READ);
		return puntajeAcumulado >= this.puntajeCosto;
	}

	@Override
	public boolean equals(Object model) {
		if (model == this)
			return true;
		if (!(model instanceof Producto))
			return false;

		return this.getId().equals(((Producto) model).getId());
	}

}
