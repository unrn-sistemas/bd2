package ar.edu.unrn.dbII.dao.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import com.db4o.ObjectContainer;
import com.db4o.query.Predicate;

import ar.edu.unrn.dbII.modelo.Categoria;
import ar.edu.unrn.dbII.modelo.Evento;
import ar.edu.unrn.dbII.modelo.Reclamo;

@SuppressWarnings("serial")
public class ReclamoDAO extends GenericDAO<Reclamo> {

	public ReclamoDAO(ObjectContainer objectDb) {
		super(objectDb);
	}

	// --------------------------------------------------
	// Métodos hook
	// --------------------------------------------------
	@Override
	protected Class<Reclamo> getActualClass() {
		return Reclamo.class;
	}

	@Override
	protected Reclamo getPrototype(UUID id) {
		return new Reclamo(id, null, null, null, null, null);
	}

	// --------------------------------------------------
	// Métodos propios
	// --------------------------------------------------
	public List<Evento> listarEventos(UUID id) {
		List<Evento> eventos = objectDb.query(new Predicate<Evento>() {
			public boolean match(Evento evento) {
				return evento.getReclamo().getId().equals(id);
			}
		});
		return eventos;
	}

	// 5. Listar los Reclamos con categoría <X>
	public List<Reclamo> reclamoDeCategoria(Categoria categoria) {
		List<Reclamo> reclamos = objectDb.query(new Predicate<Reclamo>() {
			public boolean match(Reclamo reclamo) {
				return reclamo.getCategoria().equals(categoria);
			}
		});
		return reclamos;
	}

	// 6. List<ReclamoDTO> reclamoEntreFechas(Date desde, Date hasta);
	public List<Reclamo> reclamoEntreFechas(LocalDate fechaDesde, LocalDate fechaHasta) {
		List<Reclamo> reclamos = objectDb.query(new Predicate<Reclamo>() {
			public boolean match(Reclamo reclamo) {
				LocalDate fecha = reclamo.getFecha();
				return fecha.isAfter(fechaDesde) || fecha.isEqual(fechaDesde) || fecha.isEqual(fechaHasta)
						|| fecha.isBefore(fechaHasta);
			}
		});
		return reclamos;
	}
}
