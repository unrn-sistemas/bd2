package ar.edu.unrn.dbII.modelo;

import java.util.UUID;

import com.db4o.activation.ActivationPurpose;

public class Categoria extends GenericModel {
	private String nombre;
	private Integer puntaje;

	/**
	 * Constructor para instancias existentes en la BD.
	 */
	public Categoria(String nombre, Integer puntaje) {
		super();
		this.nombre = nombre;
		this.puntaje = puntaje;
	}

	public Categoria(UUID id, String nombre, Integer puntaje) {
		super(id);
		this.nombre = nombre;
		this.puntaje = puntaje;
	}

	/**
	 * Cualquier parámetro puede ser null.
	 * 
	 * @param nombre
	 *            nullable
	 * @param puntaje
	 *            nullable
	 */
	public void update(String nombre, Integer puntaje) {
		activate(ActivationPurpose.WRITE);
		this.nombre = nombre == null ? this.nombre : nombre;
		this.puntaje = puntaje == null ? this.puntaje : puntaje;
	}

	public String getNombre() {
		activate(ActivationPurpose.READ);
		return nombre;
	}

	public Integer getPuntaje() {
		activate(ActivationPurpose.READ);
		return puntaje;
	}

	@Override
	public boolean equals(Object model) {
		if (!(model instanceof Categoria))
			return false;
		return this.getId().equals(((Categoria) model).getId());
	}

}
