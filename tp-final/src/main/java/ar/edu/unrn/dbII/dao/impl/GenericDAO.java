package ar.edu.unrn.dbII.dao.impl;

import java.util.List;
import java.util.UUID;

import com.db4o.ObjectContainer;

import ar.edu.unrn.dbII.dao.DAOInterface;

public abstract class GenericDAO<T> implements DAOInterface<T> {
	protected ObjectContainer objectDb;

	public GenericDAO(ObjectContainer objectDb) {
		this.objectDb = objectDb;
	}

	// --------------------------------------------------
	// Métodos de la interface
	// --------------------------------------------------
	@Override
	public List<T> findAll() {
		return objectDb.query(this.getActualClass());
	}

	@Override
	public T find(UUID id) {
		List<T> list = objectDb.queryByExample(this.getPrototype(id));
		return list.size() > 0 ? list.get(0) : null;
	}

	// --------------------------------------------------
	// Métodos hook
	// --------------------------------------------------
	protected abstract Class<T> getActualClass();

	protected abstract T getPrototype(UUID id);

}
