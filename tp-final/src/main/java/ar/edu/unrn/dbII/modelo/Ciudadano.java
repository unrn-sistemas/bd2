package ar.edu.unrn.dbII.modelo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.db4o.activation.ActivationPurpose;

public class Ciudadano extends GenericModel {
	private String nombre;
	private String apellido;
	private String dni;
	private String email;
	private Integer puntajeAcumulado;

	private List<Reclamo> reclamos = new ArrayList<Reclamo>();
	private List<Canje> canjes = new ArrayList<Canje>();

	/**
	 * Constructor para instancias existentes en la BD. No sirve para actualizar
	 * instancias existentes.
	 */
	public Ciudadano(UUID id, String nombre, String apellido, String dni, String email, Integer puntajeAcumulado) {
		super(id);
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.email = email;
		this.puntajeAcumulado = puntajeAcumulado;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Ciudadano(String nombre, String apellido, String dni, String email) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.email = email;
		this.puntajeAcumulado = 0;
	}

	// ---------------------------------------------------
	// Reglas del negocio
	// ---------------------------------------------------
	public void canjear(Producto producto) {
		activate(ActivationPurpose.WRITE);
		// Si el mes no es septiembre, se evalúa de forma normal. Sino, es
		// gratis.
		if (LocalDate.now().getMonthValue() != 9) {
			if (!producto.canjeable(this.puntajeAcumulado)) {
				throw new RuntimeException("No tiene puntos suficientes para canjear el producto");
			}
			this.puntajeAcumulado -= producto.getPuntajeCosto();
		}
		canjes.add(new Canje(LocalDate.now(), this, producto));
	}

	public void reclamar(String direccion, String detalle, Categoria categoria) {
		activate(ActivationPurpose.WRITE);
		this.reclamos.add(new Reclamo(LocalDate.now(), direccion, detalle, this, categoria));
		// Acumulo a mi puntaje actual, el puntaje de la Categoría del nuevo
		// Reclamo. Y si es septiembre, lo multiplico.
		this.puntajeAcumulado += LocalDate.now().getMonthValue() != 9 ? categoria.getPuntaje()
				: categoria.getPuntaje() * 2;
	}

	/**
	 * Cualquier parámetro puede ser null.
	 * 
	 * @param nombre
	 *            nullable
	 * @param apellido
	 *            nullable
	 * @param email
	 *            nullable
	 */
	public void update(String nombre, String apellido, String email) {
		activate(ActivationPurpose.WRITE);
		this.nombre = nombre == null ? this.nombre : nombre;
		this.apellido = apellido == null ? this.apellido : apellido;
		this.email = email == null ? this.email : email;
	}

	// ---------------------------------------------------
	// Getters
	// ---------------------------------------------------
	public String getNombre() {
		activate(ActivationPurpose.READ);
		return nombre;
	}

	public String getApellido() {
		activate(ActivationPurpose.READ);
		return apellido;
	}

	public String getDni() {
		activate(ActivationPurpose.READ);
		return dni;
	}

	public String getEmail() {
		activate(ActivationPurpose.READ);
		return email;
	}

	public Integer getPuntajeAcumulado() {
		activate(ActivationPurpose.READ);
		return puntajeAcumulado;
	}

	public List<Reclamo> getReclamos() {
		activate(ActivationPurpose.READ);
		return reclamos;
	}

	public List<Canje> getCanjes() {
		activate(ActivationPurpose.READ);
		return canjes;
	}

	@Override
	public boolean equals(Object model) {
		if (!(model instanceof Ciudadano))
			return false;
		return this.getId().equals(((Ciudadano) model).getId());
	}

}
