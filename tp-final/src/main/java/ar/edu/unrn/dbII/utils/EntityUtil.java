package ar.edu.unrn.dbII.utils;

import com.db4o.ObjectContainer;

public class EntityUtil {

	public static void sendToDB(Command action) {
		ObjectContainer objectDb = Db4oUtil.openObjectDb();
		try {
			action.execute(objectDb);
			objectDb.commit();
		} catch (Exception e) {
			// TODO refinar errores
			e.printStackTrace();
			throw e;
		} finally {
			Db4oUtil.closeObjectDb();
		}
	}

	public static <T> T retrieveFromDB(Query<T> action) {
		ObjectContainer objectDb = Db4oUtil.openObjectDb();
		T t = null;
		try {
			t = action.execute(objectDb);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Db4oUtil.closeObjectDb();
		}
		return t;
	}
}
