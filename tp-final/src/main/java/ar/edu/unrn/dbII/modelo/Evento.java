package ar.edu.unrn.dbII.modelo;

import java.time.LocalDate;
import java.util.UUID;

import com.db4o.activation.ActivationPurpose;

public class Evento extends GenericModel {
	private String descripcion;
	private LocalDate fecha;
	private Reclamo reclamo;

	public Evento(UUID id, String descripcion, LocalDate fecha, Reclamo reclamo) {
		super(id);
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.reclamo = reclamo;
	}

	/**
	 * Constructor para instancias que no están en la DB.
	 */
	public Evento(String descripcion, LocalDate fecha, Reclamo reclamo) {
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.reclamo = reclamo;
	}

	public String getDescripcion() {
		activate(ActivationPurpose.READ);
		return descripcion;
	}

	public LocalDate getFecha() {
		activate(ActivationPurpose.READ);
		return fecha;
	}

	public Reclamo getReclamo() {
		activate(ActivationPurpose.READ);
		return reclamo;
	}
}
