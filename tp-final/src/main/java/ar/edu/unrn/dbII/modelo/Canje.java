package ar.edu.unrn.dbII.modelo;

import java.time.LocalDate;
import java.util.UUID;

import com.db4o.activation.ActivationPurpose;

public class Canje extends GenericModel {
	private LocalDate fecha;
	private Ciudadano ciudadano;
	private Producto producto;

	/**
	 * Constructor para instancias existentes en la BD.
	 */
	public Canje(UUID id, LocalDate fecha, Ciudadano ciudadano, Producto producto) {
		super(id);
		this.fecha = fecha;
		this.ciudadano = ciudadano;
		this.producto = producto;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Canje(LocalDate fecha, Ciudadano ciudadano, Producto producto) {
		this.fecha = fecha;
		this.ciudadano = ciudadano;
		this.producto = producto;
	}

	public LocalDate getFecha() {
		activate(ActivationPurpose.READ);
		return fecha;
	}

	public Producto getProducto() {
		activate(ActivationPurpose.READ);
		return producto;
	}

	public Ciudadano getCiudadano() {
		activate(ActivationPurpose.READ);
		return ciudadano;
	}

}
