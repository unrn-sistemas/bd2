package ar.edu.unrn.dbII.utils;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.ta.TransparentPersistenceSupport;

import ar.edu.unrn.dbII.modelo.Ciudadano;
import ar.edu.unrn.dbII.modelo.MunicipioPp;
import ar.edu.unrn.dbII.modelo.Reclamo;

public class Db4oUtil {
	private static ObjectContainer db = null;
	private static final String dbName = "src/main/resources/db4o-test.db4o";

	public static ObjectContainer openObjectDb() {
		if (db != null)
			return db;

		EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
		config.common().add(new TransparentPersistenceSupport());
		config.common().objectClass(MunicipioPp.class).cascadeOnUpdate(true);
		config.common().objectClass(Ciudadano.class).cascadeOnUpdate(true);
		config.common().objectClass(Reclamo.class).cascadeOnUpdate(true);
		db = Db4oEmbedded.openFile(config, dbName);

		return db;
	}

	public static void closeObjectDb() {
		if (db != null) {
			db.close();
			db = null;
		}
	}
}
