package ar.edu.unrn.dbII.modelo;

import java.util.UUID;

import com.db4o.activation.ActivationPurpose;
import com.db4o.activation.Activator;
import com.db4o.ta.Activatable;

public abstract class GenericModel implements Activatable {

	private UUID id;
	private transient Activator activator;

	public GenericModel() {
		id = UUID.randomUUID();
	}

	public GenericModel(UUID id) {
		this.id = id;
	}

	public UUID getId() {
		activate(ActivationPurpose.READ);
		return id;
	}

	@Override
	public void activate(ActivationPurpose purpose) {
		if (activator != null) {
			activator.activate(purpose);
		}
	}

	@Override
	public void bind(Activator activator) {
		if (this.activator == activator) {
			return;
		}
		if (this.activator != null && activator != null) {
			throw new IllegalStateException();
		}
		this.activator = activator;
	}
}