# Changelog

# A tener en cuenta
DB4O elimina todo intermediario entre Java y la DB. Hibernate no tiene cabida, y por ello, se deben eliminar todo tipo de objetos y estructuras que facilitaban el trabajo con Hibernate, pero, siempre se debe tratar que el impacto de las modificaciones no sea demasiado.

## v1.0.0 Native Query is life
- [2016-02-15] `TestApiImpl`: tests pasando al 100%.
- [2016-02-15] `ApiImpl`: todos los métodos corregidos con el nuevo parámentro de la DB.
- [2016-02-15] `DAOs`: se corrigieron errores de comparación de `UUID`.
- [2016-02-15] `ProductoDAO`: se implentaron todos los métodos con Native Query.
- [2016-02-15] `GenericDAO`: se corrigió la implementación para recuperar un único objeto. Esto se replicó en el resto de los DAO. Que también se corrigieron. Se testeó en los métodos `findAll` de todos los tests de DAOs.
- [2016-02-14] `CiudadanoDAOTest`: todos los tests pasan al 100%.
- [2016-02-14] `CiudadanoDAO`: se corrigieron todas las consultas propias del Ciudadano utilizando Native Query.
- [2016-02-14] `utils/MapOrderer`: se creó esta clase para facilitar el ordenamiento de un mapa por sus valores. Esto permitió que `CiudadanoDAO@productosMasCanjeados` y `CiudadanoDAO@listarEnOrdenDeCantidadDeCanjes` sean más fáciles de implementar.
- [2016-02-14] `CiudadanoDAO@listarEnOrdenDeCantidadDeCanjes`: se tuvo que hacer un cambio en la firma por el hecho de que es más correcto y fácil almacenar un mapa por el objeto Ciudadano e ir aumentando las cantidades que se repite, que tener como clave la cantidad de veces que se repite.
- [2016-02-14] `Proyecto`: se eliminaron las dependencias de hibernate y squirrelDb. Se agregó como dependencia la librería local de db4o.
- [2016-02-14] `CiudadanoDAOTest`: pasan al 100%.
- [2016-02-14] `CiudadanoDAO`: se realizaron las mismas modificaciones que a `CategoriaDAO`.
- [2016-02-14] `models/Ciudadano`: se realizaron las mismas modificaciones que a la `Categoria`.
- [2016-02-14] `models/Canje`: se realizaron las mismas modificaciones que a la `Categoria`.
- [2016-02-14] `models/Reclamo`: se realizaron las mismas modificaciones que a la `Categoria`.
- [2016-02-14] `models/Evento`: se realizaron las mismas modificaciones que a la `Categoria`.
- [2016-02-14] `utils/Db4oUtil`: se agregaron las clases `Ciudadano` y `Reclamo` como parte de aquellas que tienen colecciones de objetos.
- [2016-02-14] `ProductoDAOTest`: pasan al 100%.
- [2016-02-14] `ProductoDAO`: se realizaron las mismas modificaciones que a `CategoriaDAO`.
- [2016-02-14] `models/Producto`: se realizaron las mismas modificaciones que a la `Categoria`.
- [2016-02-14] `CategoriaDAOTest`: pasan al 100%.
- [2016-02-14] `models/Categoria`: se tuvo que agregar a cada getter y al método update el flag para que el motor de la DB sepa qué hacer.
- [2016-02-14] `Modelos`: se tuvo que implementar la interfaz `Activatable` en el `GenericModel` y agregarle al método `getId` el flag de que necesita leer de la db.
- [2016-02-14] `Modelos`: se limpiaron las clases `Categoria`, `MunicipioPp` y `GenericModel` de todo lo relacionado a Hibernate.
	- Se eliminaron los constructores vacíos.
	- Se eliminaron los setters innecesarios.
- [2016-02-14] `models/MunicipioPp`: se implementó la interfaz `Activatable` para que puedan activarse las modificaciones con el método `commit`.
- [2016-02-14] `TestMunicipioPp`: Se está utilizando como chequeo para que funcionen.
- [2016-02-14] `models/MunicipioPp`: se eliminaron los setters, se corrigió la instanciación vacía, para que cree los arreglos allí dentro. En cada `add()` se agregó un chequeo `list.equals(o)` para que verifique si ya existe el objeto en la lista, y asi evitar almacenamiento redundante de objetos idénticos. 
- [2016-02-14] `CategoriaModel`: se eliminó el constructor vacío (necesario para Hibernate), y se eliminaron todos los setters.
- [2016-02-14] `GenericModel`: se corrigió un problema con el ID. Antes lo generaba Hibernate, ahora se debe explicitar que se genera. 
- [2016-02-14] `test.db04.book`: se agregó un archivo del cap 6 para corroborar que se hacen bien las operaciones de almacenado de más de un objeto en un array.
- [2016-02-13] `utils/Db4oUtil`: se incluyó la configuración para la persistencia transparente. 
- [2016-02-13] `TestMunicipioPp`: el test para la creación de la única instancia del Municipio fue hecha exitosamente. 
- [2016-02-13] `MunicipioPpDao`: se corrigió para que utilice la instancia `ObjectContainer objectDb`. 
- [2016-02-13] `utils`: a partir de los cambios a `JpaUtil`, se corrigieron todas las clases de este paquete.
- [2016-02-13] `utils/Db4oUtil`: el utilitario `JpaUtil` cambió de nombre, dejó de crear instancias del entityManager de hibernate, y pasó a crear una instancia de la Db4o la cual abrirá y cerrará.
- [2016-02-13] `Db4o`: se implementaron dos tests siguiendo las explicaciones de los capítulos 2 y 4 del libro ofrecido en la plataforma.
- [2016-02-13] `Proyecto`: se clonó el contenido de `jpa-reclamos`. 100% tests pasando.
- [2016-02-13] `Proyecto`: se creó el proyecto del tp-final siguiendo los pasos realizados en `jpa-reclamos`.

### Error durante la v1.0.0
- Cuando se almacena la instancia del municipio en la DB, luego no se actualiza:
	-Si el municipio se guardó por primera vez con los arreglos vacíos, y luego se agrega una categoría y se vuelve a guardar, no se hace efectiva.
	- Si se almacena con una categoría, y luego se agrega otra, esa 2a no se guarda en la DB.
	- SOLUCIÓN: para evitar estar agregando explícitamente las clases que deben guardarse en cascada, en la clase principal se debe implementar la interfaz `Activable` y modificar la configuración con la que se carga la DB. Se tuvo que mantener que cuando se modifica el municipio, se actualice en cascada.