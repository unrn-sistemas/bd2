# DB2 - Práctico 04: JPA
## Componentes
* Java 1.8
* Hibernate 5.0.2
* Gradle 2.7
* Derby 10.12

## Requisitos para la instalación
- Tener Java 1.8 y Gradle ^2.11 instalados. Gradle, que es una carpeta que se descomprime, necesita que esté asignada a la variable PATH.
- Tener GIT instalado y configurado en el PATH para clonar repositorios por línea de comando.

## Pasos para la instalación
- Luego de clonarlo, acceder desde la terminal a la ruta de este TP.
- Ejecutar `gradle --refresh-dependencies` para actualizar las dependencias.
- Ejecutar `gradle tasks` para listar todas las tareas disponibles.
- Ejecutar `gradle test` para que ejecute los tests.
- Luego, abrir el archivo `/build/reports/tests/index.html` para revisar el resultado de los tests.

## Pasos para incluir el proyecto en Eclipse
- Ejecutar `gradle eclipse`.
- Instalar desde Eclipse Marketplace el paquete `Gradle integration for Eclipse`.
- Importar el proyecto. Debería detectarlo automáticamente como proyecto Gradle, sino, clic derecho en el proyecto > Configure > Convert to Gradle project.
- Dejar que Eclipse haga un build del proyecto.

## ¿Cómo se creó el proyecto?
1. Se instaló Gradle en el sistema y se agregó en la variable PATH del sistema.
2. Se creó un boilerplate con el comando: `gradle init --type java-library`.
3. Se crearon los source folders:
    * src/main/java
    * src/main/resources
    * src/test/java
4. Se agregaron las dependencias en el archivo `build.gradle`.
5. Se instaló el plugin de gradle para eclipse.
6. Se ejecutó el comando: `gradle eclipse` para que se generen los archivos propios de un proyecto para eclipse.
7. Se importó el proyecto en Eclipse.
8. Se configuró el proyecto como proyecto gradle.

## Particularidades del proyecto
* Se utilizó UUID como tipo de dato para el id.
* El ID es un atributo de una clase genérica de la cual heredan todas las otras clases.
* Se utilizó LocalDate como tipo de dato para las fechas.
* Se utilizó el Pattern Command para la ejecución de una transacción. Se encuentra dentro del paquete `ar.edu.unrn.dbII.utils` con el nombre `EntityUtil`.
* En comparación al tp03, se refactorizaron los tests de los DAOs al implementar la transacción mediante la clase `EntityUtil`.
* Se utilizaron las expresiones lamda para simplificar la instanciación de clases anónimas.
* El archivo `META-INF/persistence.xml` se movió del source folder `etc` al `src/main/resources`.
* SELECT 1 FROM X WHERE id = :id es lo más eficiente al momento de verificar si E.

## Cosas para hacer
* Optimizar las listas de las relaciones one-to-many de forma que agregar un método no fuerce a recuperar toda la colección de ese objeto.
    * Actualización: actualmente, las listas no recuperan toda la colección por defecto. Ni siquiera con la etiqueta OrderColumn.
* Implementar el pattern Command&Query por completo.
    *  Actualización: completado.
* Analizar un arreglo desatachado para así comprobar que al momento de guardar un elemento nuevo, y estando el arreglo vacío, hibernate no recupera toda la colección antes de guardar el nuevo elemento.
    *  Actualización: no se puede comprobar. Al momento de intentar recuperar la colección asociada al objeto, el sistema intenta acceder a la DB, pero como está desatachado tira un error.
* Utilizar el microfw Spark para crear una api web.
* Escribir un root object.
