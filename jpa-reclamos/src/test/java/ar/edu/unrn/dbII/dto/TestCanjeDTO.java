package ar.edu.unrn.dbII.dto;

import static java.util.UUID.randomUUID;

import java.time.LocalDate;

import org.bson.Document;
import org.bson.json.JsonWriterSettings;
import org.junit.Assert;
import org.junit.Test;

import ar.edu.unrn.dbII.modelo.Canje;
import ar.edu.unrn.dbII.modelo.Ciudadano;
import ar.edu.unrn.dbII.modelo.Producto;

public class TestCanjeDTO {

	@Test
	public void testToDocument() {
		try {
			CanjeDTO canje = new CanjeDTO(new Canje(randomUUID(), LocalDate.now(),
					new Ciudadano(randomUUID(), null, null, null, null, null), new Producto(randomUUID(), null, null)));
			Document document = canje.toDocument();
			String json = document.toJson(new JsonWriterSettings(true));
			System.out.println(json);
			Assert.assertTrue(true);

			CanjeDTO canjeCopia = new CanjeDTO(document);
			Assert.assertTrue(canje.getFecha().equals(canjeCopia.getFecha()));
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail("Falló");
		}
	}

}
