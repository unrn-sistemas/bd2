package ar.edu.unrn.dbII.conection;

import javax.persistence.EntityManager;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import ar.edu.unrn.dbII.utils.JpaUtil;

public class JPATest {
	protected static EntityManager entityManager;

	@BeforeClass
	public static void inicializar() {
		entityManager = JpaUtil.openEntityManager();
	}

	@AfterClass
	public static void finalizar() {
		entityManager.close();
	}

}
