package ar.edu.unrn.dbII.dao.impl;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ar.edu.unrn.dbII.conection.JPATest;
import ar.edu.unrn.dbII.modelo.Categoria;
import ar.edu.unrn.dbII.utils.EntityUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CategoriaDAOTest extends JPATest {

	@Test
	public void a_persist() {
		Categoria categoria = new Categoria("Nueva Categoria", 123);
		try {
			EntityUtil.sendToDB(
					(EntityManager entityManager) -> MunicipioPpDAO.getInstance(entityManager).add(categoria));
		} catch (Exception e) {
			fail("Error");
			e.printStackTrace();
		}
	}

	@Test
	public void b_findAll() {
		try {
			EntityUtil.sendToDB((EntityManager entityManager) -> {
				CategoriaDAO categoriaDAO = new CategoriaDAO(entityManager);
				List<Categoria> categorias = categoriaDAO.findAll();
				assertTrue(categorias.size() > 0);

				Categoria categoria = categoriaDAO.find(categorias.get(0).getId());
				assertTrue(Categoria.class == categoria.getClass());
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

	@Test
	public void c_testRemove() {
		try {
			EntityUtil.sendToDB((EntityManager entityManager) -> {
				CategoriaDAO categoriaDAO = new CategoriaDAO(entityManager);
				List<Categoria> categorias = categoriaDAO.findAll();
				MunicipioPpDAO.getInstance(entityManager).remove(categorias.get(0));
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

}
