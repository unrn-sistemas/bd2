package ar.edu.unrn.dbII.dto;

import java.time.LocalDate;
import java.util.UUID;

import org.bson.Document;
import org.bson.json.JsonWriterSettings;
import org.junit.Assert;
import org.junit.Test;

import ar.edu.unrn.dbII.modelo.Evento;
import ar.edu.unrn.dbII.modelo.Reclamo;

public class TestEventoDTO {

	@Test
	public void testToDocument() {
		try {
			EventoDTO evento = new EventoDTO(new Evento(UUID.randomUUID(), "Evento 1", LocalDate.now(),
					new Reclamo(UUID.randomUUID(), null, null, null, null, null)));
			Document document = evento.toDocument();
			String json = document.toJson(new JsonWriterSettings(true));
			System.out.println(json);
			Assert.assertTrue(true);

			EventoDTO eventoCopia = new EventoDTO(document);
			Assert.assertTrue(evento.getId().equals(eventoCopia.getId()));
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail("Falló");
		}
	}

}
