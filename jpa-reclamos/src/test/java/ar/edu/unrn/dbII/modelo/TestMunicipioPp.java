package ar.edu.unrn.dbII.modelo;

import javax.persistence.EntityManager;

import org.junit.Assert;
import org.junit.Test;

import ar.edu.unrn.dbII.dao.impl.MunicipioPpDAO;
import ar.edu.unrn.dbII.utils.JpaUtil;

public class TestMunicipioPp {

	@Test
	public void testGetInstance() {
		EntityManager entityManager = JpaUtil.openEntityManager();
		MunicipioPp municipio = MunicipioPpDAO.getInstance(entityManager);
		Assert.assertTrue(municipio != null);
		JpaUtil.closeEntityManager();
	}

}
