package ar.edu.unrn.dbII.dto;

import java.util.UUID;

import org.bson.Document;
import org.bson.json.JsonWriterSettings;
import org.junit.Assert;
import org.junit.Test;

import ar.edu.unrn.dbII.modelo.Producto;

public class TestProductoDTO {

	@Test
	public void testToDocument() {
		try {
			ProductoDTO productoOriginal = new ProductoDTO(new Producto(UUID.randomUUID(), "Producto 1", 123));
			Document document = productoOriginal.toDocument();
			String json = document.toJson(new JsonWriterSettings(true));
			System.out.println(json);
			Assert.assertTrue(true);
			ProductoDTO productoCopia = new ProductoDTO(document);

			// It's JSON translating good the UUID?
			Assert.assertTrue(productoOriginal.getId().equals(productoCopia.getId()));
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail("Falló");
		}
	}

}
