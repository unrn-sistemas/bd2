package ar.edu.unrn.dbII.dao.impl;

import static org.junit.Assert.fail;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ar.edu.unrn.dbII.conection.JPATest;
import ar.edu.unrn.dbII.modelo.Categoria;
import ar.edu.unrn.dbII.modelo.Ciudadano;
import ar.edu.unrn.dbII.modelo.Producto;
import ar.edu.unrn.dbII.utils.EntityUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CiudadanoDAOTest extends JPATest {
	public static final Integer categoriaPuntaje = 123;
	public static final Integer productoPuntaje = 5;

	@Test
	public void a_persist() {
		Ciudadano ciudadano = new Ciudadano("Luciano", "Graziani", "35591234", "lgraziani2712@gmail.com");
		try {
			EntityUtil.sendToDB(
					(EntityManager entityManager) -> MunicipioPpDAO.getInstance(entityManager).add(ciudadano));
		} catch (Exception e) {
			fail("Error");
			e.printStackTrace();
		}
	}

	@Test
	public void b_findAll() {
		try {
			EntityUtil.sendToDB((EntityManager entityManager) -> {
				CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(entityManager);
				List<Ciudadano> ciudadanos = ciudadanoDAO.findAll();
				Assert.assertTrue(ciudadanos.size() > 0);

				Ciudadano ciudadano = ciudadanoDAO.find(ciudadanos.get(0).getId());
				Assert.assertTrue(Ciudadano.class == ciudadano.getClass());
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

	@Test
	public void c_reclamar() {
		try {
			EntityUtil.sendToDB((EntityManager entityManager) -> {
				CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(entityManager);
				Ciudadano ciudadano = ciudadanoDAO.findAll().get(0);
				Categoria categoria = new Categoria("Categoria nueva", categoriaPuntaje);
				MunicipioPpDAO.getInstance(entityManager).add(categoria);
				ciudadano.reclamar("ASjdhf", "fdjshdkfjh", categoria);
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

	@Test
	public void d_canjear() {
		try {
			EntityUtil.sendToDB((EntityManager entityManager) -> {
				CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(entityManager);
				Ciudadano ciudadano = ciudadanoDAO.findAll().get(0);
				Producto productoAccesible = new Producto("Producto accesible", productoPuntaje);
				Producto productoCaro = new Producto("Producto caro", 500);
				MunicipioPpDAO.getInstance(entityManager).add(productoAccesible);
				MunicipioPpDAO.getInstance(entityManager).add(productoCaro);
				ciudadano.canjear(productoAccesible);
				Assert.assertTrue(
						"¿El puntaje restante del ciudadano es igual al puntaje de la Categoría menos al del Producto?",
						ciudadano.getPuntajeAcumulado() == (categoriaPuntaje - productoPuntaje));
				ciudadano.canjear(productoCaro);
			});
		} catch (RuntimeException e) {
			if (!e.getMessage().equals("No tiene puntos suficientes para canjear el producto")) {
				fail("Falló");
				e.printStackTrace();
			}
		}
	}

	@Test
	public void z_remove() {
		try {
			EntityUtil.sendToDB((EntityManager entityManager) -> {
				CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(entityManager);
				Ciudadano ciudadano = ciudadanoDAO.findAll().get(0);
				MunicipioPpDAO.getInstance(entityManager).remove(ciudadano);
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

}
