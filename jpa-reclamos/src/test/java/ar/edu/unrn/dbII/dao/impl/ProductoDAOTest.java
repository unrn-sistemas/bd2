package ar.edu.unrn.dbII.dao.impl;

import static org.junit.Assert.fail;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import ar.edu.unrn.dbII.conection.JPATest;
import ar.edu.unrn.dbII.modelo.MunicipioPp;
import ar.edu.unrn.dbII.modelo.Producto;
import ar.edu.unrn.dbII.utils.EntityUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductoDAOTest extends JPATest {

	@Test
	public void a_persist() {
		Producto producto = new Producto("Nuevo Producto", 123);
		try {
			EntityUtil
					.sendToDB((EntityManager entityManager) -> MunicipioPpDAO.getInstance(entityManager).add(producto));
		} catch (Exception e) {
			fail("Error");
			e.printStackTrace();
		}
	}

	@Test
	public void b_testFindAll() {
		try {
			EntityUtil.sendToDB((EntityManager entityManager) -> {
				ProductoDAO productoDAO = new ProductoDAO(entityManager);
				List<Producto> productos = productoDAO.findAll();
				Assert.assertTrue(productos.size() > 0);

				Producto producto = productoDAO.find(productos.get(0).getId());
				Assert.assertTrue(Producto.class == producto.getClass());
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

	@Test
	public void c_testRemove() {
		try {
			EntityUtil.sendToDB((EntityManager entityManager) -> {
				ProductoDAO productoDAO = new ProductoDAO(entityManager);
				Producto producto = productoDAO.findAll().get(0);
				MunicipioPp municipio = MunicipioPpDAO.getInstance(entityManager);
				municipio.remove(producto);
			});
		} catch (Exception e) {
			e.printStackTrace();
			fail("Falló");
		}
	}

}
