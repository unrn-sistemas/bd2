package ar.edu.unrn.dbII.dao.impl;

import javax.persistence.EntityManager;

import ar.edu.unrn.dbII.modelo.MunicipioPp;

public class MunicipioPpDAO {

	/**
	 * Este método verifica en la BD si existe el objeto Root, en caso de no, lo inserta, en caso de sí, no hace nada.
	 * En definitiva, un pattern Singleton para la persistencia en BD.
	 */
	public static MunicipioPp getInstance(EntityManager entityManager) {
		MunicipioPp municipio = entityManager.find(MunicipioPp.class, 1);
		if (municipio != null)
			return municipio;
		municipio = new MunicipioPp();
		entityManager.persist(municipio);

		return municipio;
	}

}
