package ar.edu.unrn.dbII.modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "municipio")
public class MunicipioPp {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@OneToMany(orphanRemoval = true, cascade = { CascadeType.ALL })
	@JoinColumn(name = "municipio_id")
	List<Ciudadano> ciudadanos = new ArrayList<>();

	@OneToMany(orphanRemoval = true, cascade = { CascadeType.ALL })
	@JoinColumn(name = "municipio_id")
	List<Categoria> categorias = new ArrayList<>();

	@OneToMany(orphanRemoval = true, cascade = { CascadeType.ALL })
	@JoinColumn(name = "municipio_id")
	List<Producto> productos = new ArrayList<>();

	public MunicipioPp() {

	}

	public List<Ciudadano> getCiudadanos() {
		return ciudadanos;
	}

	public void setCiudadanos(List<Ciudadano> ciudadanos) {
		this.ciudadanos = ciudadanos;
	}

	public List<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}

	public List<Producto> getProductos() {
		return productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	public void add(Categoria categoria) {
		categorias.add(categoria);
	}

	public void remove(Categoria categoria) {
		categorias.remove(categoria);
	}

	public void add(Producto producto) {
		productos.add(producto);
	}

	public void remove(Producto producto) {
		productos.remove(producto);
	}

	public void add(Ciudadano ciudadano) {
		ciudadanos.add(ciudadano);
	}

	public void remove(Ciudadano ciudadano) {
		ciudadanos.remove(ciudadano);
	}

}
