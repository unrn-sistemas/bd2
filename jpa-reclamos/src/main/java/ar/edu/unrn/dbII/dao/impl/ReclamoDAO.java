package ar.edu.unrn.dbII.dao.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import ar.edu.unrn.dbII.modelo.Categoria;
import ar.edu.unrn.dbII.modelo.Evento;
import ar.edu.unrn.dbII.modelo.Reclamo;

@SuppressWarnings("unchecked")
public class ReclamoDAO extends GenericDAO<Reclamo> {

	public ReclamoDAO(EntityManager entityManager) {
		super(entityManager);
	}

	// --------------------------------------------------
	// Métodos propios
	// --------------------------------------------------
	public List<Evento> listarEventos(UUID id) {
		String JPHQL = "SELECT evento FROM Evento evento WHERE evento.reclamo.id = :id";
		Query query = entityManager.createQuery(JPHQL);
		query.setParameter("id", id);
		return query.getResultList();
	}

	public List<Reclamo> reclamoDeCategoria(Categoria categoria) {
		String JPHQL = "SELECT reclamo FROM Reclamo reclamo WHERE reclamo.categoria.id = :categoria";
		Query query = entityManager.createQuery(JPHQL);
		query.setParameter("categoria", categoria.getId());
		return query.getResultList();
	}

	public List<Reclamo> reclamoEntreFechas(LocalDate fechaDesde, LocalDate fechaHasta) {
		StringBuffer JPHQL = new StringBuffer("SELECT reclamo FROM Reclamo reclamo ");
		if (fechaDesde != null && fechaHasta != null)
			JPHQL.append("WHERE reclamo.fecha BETWEEN :fechaDesde AND :fechaHasta");
		if (fechaDesde == null && fechaHasta != null) {
			JPHQL.append("WHERE reclamo.fecha <= :fechaHasta");
		}
		if (fechaHasta == null && fechaDesde != null) {
			JPHQL.append("WHERE reclamo.fecha >= :fechaDesde");
		}
		Query query = entityManager.createQuery(JPHQL.toString());
		if (fechaDesde != null && fechaHasta != null) {
			query.setParameter("fechaDesde", fechaDesde);
			query.setParameter("fechaHasta", fechaHasta);
		}
		if (fechaDesde == null && fechaHasta != null)
			query.setParameter("fechaHasta", fechaHasta);
		if (fechaHasta == null && fechaDesde != null)
			query.setParameter("fechaDesde", fechaDesde);

		return query.getResultList();

	}

	// --------------------------------------------------
	// Métodos hook
	// --------------------------------------------------
	@Override
	protected Class<Reclamo> getActualClass() {
		return Reclamo.class;
	}

	@Override
	protected String getAllQuery() {
		return "FROM Reclamo reclamos";
	}

	@Override
	protected String getObjectName() {
		return "reclamo";
	}

}
