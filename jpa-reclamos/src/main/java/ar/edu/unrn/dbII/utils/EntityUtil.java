package ar.edu.unrn.dbII.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.RollbackException;

import org.hibernate.exception.ConstraintViolationException;

public class EntityUtil {

	public static void sendToDB(Command action) {
		EntityManager entityManager = JpaUtil.openEntityManager();

		EntityTransaction transaction = null;
		try {
			transaction = entityManager.getTransaction();
			transaction.begin();

			action.execute(entityManager);

			transaction.commit();
		} catch (RollbackException e) {
			if (transaction != null && transaction.isActive())
				transaction.rollback();
			Throwable throwable = e.getCause().getCause();
			if (throwable instanceof ConstraintViolationException)
				throw new RuntimeException("Borre los objetos relacionados al que intentó borrar antes de hacerlo", e);
			e.printStackTrace();
			throw e;
		} finally {
			entityManager.close();
		}
	}

	public static <T> T retrieveFromDB(Query<T> action) {
		EntityManager entityManager = JpaUtil.openEntityManager();
		EntityTransaction transaction = null;
		T t = null;
		try {
			transaction = entityManager.getTransaction();
			transaction.begin();

			t = action.execute(entityManager);

			transaction.commit();
		} catch (Exception e) {
			if (transaction != null && transaction.isActive())
				transaction.rollback();
			throw e;
		} finally {
			entityManager.close();
		}
		return t;
	}
}
