package ar.edu.unrn.dbII.dao.impl;

import javax.persistence.EntityManager;

import ar.edu.unrn.dbII.modelo.Categoria;

public class CategoriaDAO extends GenericDAO<Categoria> {

	public CategoriaDAO(EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected String getAllQuery() {
		return "FROM Categoria categorias";
	}

	@Override
	protected Class<Categoria> getActualClass() {
		return Categoria.class;
	}

	@Override
	protected String getObjectName() {
		return "Categoria";
	}
}
