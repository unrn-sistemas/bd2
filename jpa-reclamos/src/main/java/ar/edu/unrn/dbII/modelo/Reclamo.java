package ar.edu.unrn.dbII.modelo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "reclamos")
public class Reclamo extends GenericModel {
	private LocalDate fecha;
	private String direccion;
	private String detalle;

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "ciudadano_id")
	private Ciudadano ciudadano;

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "categoria_id")
	private Categoria categoria;

	@OneToMany(mappedBy = "reclamo", cascade = { CascadeType.ALL })
	private List<Evento> eventos = new ArrayList<Evento>();

	/**
	 * Para instancias ya existentes en la DB.
	 */
	public Reclamo() {

	}

	public Reclamo(UUID id, LocalDate fecha, String direccion, String detalle, Ciudadano ciudadano,
			Categoria categoria) {
		super(id);
		this.fecha = fecha;
		this.direccion = direccion;
		this.detalle = detalle;
		this.categoria = categoria;
		this.ciudadano = ciudadano;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Reclamo(LocalDate fecha, String direccion, String detalle, Ciudadano ciudadano, Categoria categoria) {
		this.fecha = fecha;
		this.direccion = direccion;
		this.detalle = detalle;
		this.ciudadano = ciudadano;
		this.categoria = categoria;
	}

	// ---------------------------------------------------
	// Reglas del negocio
	// ---------------------------------------------------

	// ---------------------------------------------------
	// Getters
	// ---------------------------------------------------

	public void agregarEvento(Evento evento) {
		eventos.add(evento);
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public Ciudadano getCiudadano() {
		return ciudadano;
	}

	public void setCiudadano(Ciudadano ciudadano) {
		this.ciudadano = ciudadano;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public List<Evento> getEventos() {
		return eventos;
	}

	public void setEventos(List<Evento> eventos) {
		this.eventos = eventos;
	}

}
