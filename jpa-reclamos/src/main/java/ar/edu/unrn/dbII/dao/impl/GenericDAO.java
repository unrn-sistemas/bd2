package ar.edu.unrn.dbII.dao.impl;

import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import ar.edu.unrn.dbII.dao.DAOInterface;

@SuppressWarnings("unchecked")
public abstract class GenericDAO<T> implements DAOInterface<T> {
	protected EntityManager entityManager;

	public GenericDAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	// --------------------------------------------------
	// Métodos de la interface
	// --------------------------------------------------
	@Override
	public List<T> findAll() {
		Query query = entityManager.createQuery(this.getAllQuery());
		return (List<T>) query.getResultList();
	}

	@Override
	public T find(UUID id) {
		return (T) entityManager.find(this.getActualClass(), id);
	}

	// --------------------------------------------------
	// Métodos hook
	// --------------------------------------------------
	protected abstract String getAllQuery();

	protected abstract Class<T> getActualClass();

	protected abstract String getObjectName();

}
