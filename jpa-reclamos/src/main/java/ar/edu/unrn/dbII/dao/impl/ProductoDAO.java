package ar.edu.unrn.dbII.dao.impl;

import javax.persistence.EntityManager;

import ar.edu.unrn.dbII.modelo.Producto;

public class ProductoDAO extends GenericDAO<Producto> {

	public ProductoDAO(EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	protected String getAllQuery() {
		return "FROM Producto productos";
	}

	@Override
	protected Class<Producto> getActualClass() {
		return Producto.class;
	}

	@Override
	protected String getObjectName() {
		return "Producto";
	}

}
