package ar.edu.unrn.dbII.modelo;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "categorias")
public class Categoria extends GenericModel {
	private String nombre;
	private Integer puntaje;

	public Categoria() {

	}

	/**
	 * Constructor para instancias existentes en la BD.
	 */
	public Categoria(UUID id, String nombre, Integer puntaje) {
		super(id);
		this.nombre = nombre;
		this.puntaje = puntaje;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Categoria(String nombre, Integer puntaje) {
		this.nombre = nombre;
		this.puntaje = puntaje;
	}

	/**
	 * Cualquier parámetro puede ser null.
	 * 
	 * @param nombre
	 *            nullable
	 * @param puntaje
	 *            nullable
	 */
	public void update(String nombre, Integer puntaje) {
		this.nombre = nombre == null ? this.nombre : nombre;
		this.puntaje = puntaje == null ? this.puntaje : puntaje;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(Integer puntaje) {
		this.puntaje = puntaje;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object model) {
		if (!(model instanceof Categoria))
			return false;
		return this.getId().equals(((Categoria) model).getId());
	}

}
