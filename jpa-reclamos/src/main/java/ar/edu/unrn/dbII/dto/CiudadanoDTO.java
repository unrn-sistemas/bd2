package ar.edu.unrn.dbII.dto;

import java.util.UUID;

import org.bson.Document;

import ar.edu.unrn.dbII.modelo.Ciudadano;

public class CiudadanoDTO extends GenericDTO {
	private String nombre;
	private String apellido;
	private String dni;
	private String email;
	private Integer puntajeAcumulado;

	public CiudadanoDTO() {
		super();
	}

	public CiudadanoDTO(Ciudadano ciudadano) {
		this.id = ciudadano.getId();
		this.nombre = ciudadano.getNombre();
		this.apellido = ciudadano.getApellido();
		this.dni = ciudadano.getDni();
		this.email = ciudadano.getEmail();
		this.puntajeAcumulado = ciudadano.getPuntajeAcumulado();
	}

	public CiudadanoDTO(Document document) {
		this.id = document.get("id", UUID.class);
		this.nombre = document.getString("nombre");
		this.apellido = document.getString("apellido");
		this.dni = document.getString("dni");
		this.email = document.getString("email");
		this.puntajeAcumulado = document.getInteger("puntajeAcumulado");
	}

	public Document toDocument() {
		Document document = super.toDocument();
		document.append("nombre", nombre).append("apellido", apellido).append("dni", dni).append("email", email)
				.append("puntajeAcumulado", puntajeAcumulado);
		return document;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getPuntajeAcumulado() {
		return puntajeAcumulado;
	}

	public void setPuntajeAcumulado(Integer puntaje) {
		this.puntajeAcumulado = puntaje;
	}

}
