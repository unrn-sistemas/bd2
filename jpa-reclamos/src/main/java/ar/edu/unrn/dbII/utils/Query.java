package ar.edu.unrn.dbII.utils;

import javax.persistence.EntityManager;

public interface Query<T> {
	T execute(EntityManager entityManager);
}
