package ar.edu.unrn.dbII.dto;

import java.util.UUID;

import org.bson.Document;

import ar.edu.unrn.dbII.modelo.Producto;

public class ProductoDTO extends GenericDTO {
	private String nombre;
	private Integer puntajeCosto;

	public ProductoDTO() {
	}

	public ProductoDTO(Producto producto) {
		this.id = producto.getId();
		this.puntajeCosto = producto.getPuntajeCosto();
		this.nombre = producto.getNombre();
	}

	public ProductoDTO(Document document) {
		this.id = document.get("id", UUID.class);
		this.puntajeCosto = document.getInteger("puntajeCosto");
		this.nombre = document.getString("nombre");
	}

	public Document toDocument() {
		Document document = super.toDocument();
		document.append("nombre", nombre).append("puntajeCosto", puntajeCosto);
		return document;
	}

	public Integer getPuntajeCosto() {
		return puntajeCosto;
	}

	public void setPuntajeCosto(Integer puntajeCosto) {
		this.puntajeCosto = puntajeCosto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
