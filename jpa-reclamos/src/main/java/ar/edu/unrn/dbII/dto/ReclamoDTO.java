package ar.edu.unrn.dbII.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bson.Document;

import ar.edu.unrn.dbII.modelo.Evento;
import ar.edu.unrn.dbII.modelo.Reclamo;

public class ReclamoDTO extends GenericDTO {
	private LocalDate fecha;
	private String direccion;
	private String detalle;
	private UUID categoriaId;
	private UUID ciudadanoId;
	private List<EventoDTO> eventos = new ArrayList<EventoDTO>();

	public ReclamoDTO() {
	}

	public ReclamoDTO(Reclamo reclamo) {
		this.id = reclamo.getId();
		this.fecha = reclamo.getFecha();
		this.direccion = reclamo.getDireccion();
		this.detalle = reclamo.getDetalle();
		this.categoriaId = reclamo.getCategoria().getId();
		this.ciudadanoId = reclamo.getCiudadano().getId();
		List<Evento> eventos = reclamo.getEventos();
		if (eventos != null)
			for (Evento evento : eventos)
				this.eventos.add(new EventoDTO(evento));
	}

	public ReclamoDTO(Document doc) {
		this.id = doc.get("id", UUID.class);
		this.fecha = LocalDate.parse(doc.getString("fecha"));
		this.direccion = doc.getString("direccion");
		this.detalle = doc.getString("detalle");
		this.categoriaId = doc.get("categoriaId", UUID.class);
		this.ciudadanoId = doc.get("ciudadanoId", UUID.class);
		@SuppressWarnings("unchecked")
		List<Document> eventos = (List<Document>) doc.get("eventos");
		for (Document eventoDoc : eventos) {
			this.eventos.add(new EventoDTO(eventoDoc));
		}
	}

	public Document toDocument() {
		Document document = super.toDocument();
		document.append("fecha", fecha.toString()).append("direccion", direccion).append("detalle", detalle)
				.append("categoriaId", categoriaId).append("ciudadanoId", ciudadanoId);
		List<Document> eventosDoc = new ArrayList<Document>();
		for (EventoDTO evento : eventos)
			eventosDoc.add(evento.toDocument());
		document.append("eventos", eventosDoc);

		return document;
	}

	public List<EventoDTO> getEventos() {
		return eventos;
	}

	public void setEventos(List<EventoDTO> eventos) {
		this.eventos = eventos;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public UUID getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(UUID categoriaId) {
		this.categoriaId = categoriaId;
	}

	public UUID getCiudadanoId() {
		return ciudadanoId;
	}

	public void setCiudadanoId(UUID ciudadanoId) {
		this.ciudadanoId = ciudadanoId;
	}

}
