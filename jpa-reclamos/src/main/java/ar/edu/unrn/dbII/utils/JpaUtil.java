package ar.edu.unrn.dbII.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {
	private static final EntityManagerFactory entityManager;

	static {
		try {
			entityManager = Persistence.createEntityManagerFactory("jpa-reclamos");
		} catch (Throwable ex) {
			// Log the exception.
			System.err.println("Initial SessionFactorycreation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static EntityManager openEntityManager() {
		return entityManager.createEntityManager();
	}
	
	public static void closeEntityManager() {
		if (entityManager.isOpen())
			entityManager.close();
	}
}
