package ar.edu.unrn.dbII.modelo;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "productos")
public class Producto extends GenericModel {
	private Integer puntajeCosto;
	private String nombre;

	public Producto() {
	}

	/**
	 * Constructor para instancias que están en la DB.
	 */
	public Producto(UUID id, String nombre, Integer puntajeCosto) {
		super(id);
		this.nombre = nombre;
		this.puntajeCosto = puntajeCosto;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Producto(String nombre, Integer puntajeCosto) {
		this.nombre = nombre;
		this.puntajeCosto = puntajeCosto;
	}

	/**
	 * Cualquier parámetro puede ser null.
	 * 
	 * @param nombre
	 *            nullable
	 * @param puntajeCosto
	 *            nullable
	 */
	public void update(String nombre, Integer puntajeCosto) {
		this.nombre = nombre == null ? this.nombre : nombre;
		this.puntajeCosto = puntajeCosto == null ? this.puntajeCosto : puntajeCosto;
	}

	public String getNombre() {
		return nombre;
	}

	public Integer getPuntajeCosto() {
		return puntajeCosto;
	}

	protected Boolean canjeable(Integer puntajeAcumulado) {
		return puntajeAcumulado >= this.puntajeCosto;
	}

	public void setPuntajeCosto(Integer puntajeCosto) {
		this.puntajeCosto = puntajeCosto;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public boolean equals(Object model) {
		if (model == this)
			return true;
		if (!(model instanceof Producto))
			return false;

		return this.getId().equals(((Producto) model).getId());
	}

}
