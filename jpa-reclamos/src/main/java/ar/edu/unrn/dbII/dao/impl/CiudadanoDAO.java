package ar.edu.unrn.dbII.dao.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import ar.edu.unrn.dbII.modelo.Canje;
import ar.edu.unrn.dbII.modelo.Categoria;
import ar.edu.unrn.dbII.modelo.Ciudadano;
import ar.edu.unrn.dbII.modelo.Producto;

public class CiudadanoDAO extends GenericDAO<Ciudadano> {

	public CiudadanoDAO(EntityManager entityManager) {
		super(entityManager);
	}

	// --------------------------------------------------
	// Métodos propios
	// --------------------------------------------------
	@SuppressWarnings("unchecked")
	public List<Canje> listarCanjes(UUID idCiudadano) {
		String HQLString = "SELECT canje " + "FROM Canje canje " + "WHERE canje.ciudadano.id = :id";
		Query query = entityManager.createQuery(HQLString);
		query.setParameter("id", idCiudadano);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Ciudadano> canjearonProductoElDia(Producto producto, LocalDate fecha) {
		List<Ciudadano> ciudadanos = entityManager
				.createQuery("SELECT ciudadano FROM Ciudadano ciudadano JOIN ciudadano.canjes canje "
						+ "WHERE canje.producto.id = :id AND canje.fecha = :fecha")
				.setParameter("id", producto.getId()).setParameter("fecha", fecha).getResultList();
		return ciudadanos;
	}

	@SuppressWarnings("unchecked")
	public List<Ciudadano> conReclamosDeCategoria(Categoria categoria) {
		List<Ciudadano> ciudadanos = entityManager
				.createQuery(
						"SELECT ciudadano FROM Ciudadano ciudadano JOIN ciudadano.reclamos reclamo WHERE reclamo.categoria.id = :id")
				.setParameter("id", categoria.getId()).getResultList();

		return ciudadanos;
	}

	// --------------------------------------------------
	// Métodos hook
	// --------------------------------------------------
	@Override
	protected String getAllQuery() {
		return "FROM Ciudadano ciudadanos";
	}

	@Override
	protected Class<Ciudadano> getActualClass() {
		return Ciudadano.class;
	}

	@Override
	protected String getObjectName() {
		return "Ciudadano";
	}

	/**
	 * Devuelve un listado de los productos que tienen la mayor cantidad de canjes realizados.
	 * 
	 * La poco eficiente solución que se nos ocurrió es la siguiente: 1r consulta: se recupera la cantidad de canjes más
	 * alta. 2a consulta: se recuperan todos los ID de los productos con igual cantidad de canjes que el valor anterior.
	 * 3a y más consultas: se recupera cada uno de los productos recuperados en la consulta anterior.
	 * 
	 * Una mejora sería realizar la primer y segunda consulta en una sola. Pero creemos que (al menos en Derby) la
	 * tercera en adelante seguirán estando.
	 */
	@SuppressWarnings("unchecked")
	public List<Producto> productosMasCanjeados() {
		String HQLString = "SELECT COUNT(*) as cantidad FROM Canje canjes GROUP BY canjes.producto.nombre ORDER BY 1 DESC";
		Query query = entityManager.createQuery(HQLString);
		Long count = (Long) query.getResultList().get(0);

		HQLString = "SELECT pro.id FROM Canje canje JOIN canje.producto pro GROUP BY pro HAVING COUNT(pro.id) = :cant";
		query = entityManager.createQuery(HQLString);
		query.setParameter("cant", count);
		List<UUID> productosId = (List<UUID>) query.getResultList();
		List<Producto> productos = new ArrayList<Producto>();
		for (UUID productoId : productosId) {
			productos.add(entityManager.find(Producto.class, productoId));
		}

		return productos;
	}

	/**
	 * Devuelve el nombre de los ciudadanos ordenados según la cantidad de canjes.
	 * 
	 * @return {"nombre","cantidad"}
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, Ciudadano> listarEnOrdenDeCantidadDeCanjes() {
		Map<Integer, Ciudadano> ciudadanos = new HashMap<Integer, Ciudadano>();
		List<Object[]> objects = entityManager
				.createQuery("SELECT c.id, COUNT(*) as cantidad FROM Ciudadano c GROUP BY c.id ORDER BY 2 DESC")
				.getResultList();

		// Transforma cada long del COUNT(*) a entero
		for (int i = 0; i < objects.size(); i++) {
			Object[] object = objects.get(i);
			ciudadanos.put(safeLongToInt((long) object[1]), entityManager.find(Ciudadano.class, object[0]));
		}
		return ciudadanos;
	}

	@SuppressWarnings("unchecked")
	public List<Producto> listarProductosQueFueronCanjeadosEnFecha(LocalDate fecha) {
		String HQLString = "SELECT canjes.producto " + "FROM Ciudadano ciudadano, " + "IN (ciudadano.canjes) canjes, "
				+ "IN (ciudadano.reclamos) reclamos "
				+ "WHERE YEAR(reclamos.fecha) = :year and MONTH(reclamos.fecha) = :month";
		Query query = entityManager.createQuery(HQLString);
		query.setParameter("month", fecha.getMonthValue());
		query.setParameter("year", fecha.getYear());
		return query.getResultList();
	}

	private Integer safeLongToInt(long l) {
		if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
			throw new IllegalArgumentException(l + " cannot be cast to int without changing its value.");
		}
		return (int) l;
	}

}
