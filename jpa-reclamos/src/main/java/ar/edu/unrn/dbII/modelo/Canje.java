package ar.edu.unrn.dbII.modelo;

import java.time.LocalDate;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "canjes")
public class Canje extends GenericModel {
	private LocalDate fecha;

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "ciudadano_id")
	private Ciudadano ciudadano;

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "producto_id")
	private Producto producto;

	public Canje() {
	}

	/**
	 * Constructor para instancias existentes en la BD.
	 */
	public Canje(UUID id, LocalDate fecha, Ciudadano ciudadano, Producto producto) {
		super(id);
		this.fecha = fecha;
		this.ciudadano = ciudadano;
		this.producto = producto;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Canje(LocalDate fecha, Ciudadano ciudadano, Producto producto) {
		this.fecha = fecha;
		this.ciudadano = ciudadano;
		this.producto = producto;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public Producto getProducto() {
		return producto;
	}

	public Ciudadano getCiudadano() {
		return ciudadano;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public void setCiudadano(Ciudadano ciudadano) {
		this.ciudadano = ciudadano;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}
