package ar.edu.unrn.dbII.api.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.persistence.EntityManager;

import ar.edu.unrn.dbII.api.Api;
import ar.edu.unrn.dbII.dao.impl.CategoriaDAO;
import ar.edu.unrn.dbII.dao.impl.CiudadanoDAO;
import ar.edu.unrn.dbII.dao.impl.MunicipioPpDAO;
import ar.edu.unrn.dbII.dao.impl.ProductoDAO;
import ar.edu.unrn.dbII.dao.impl.ReclamoDAO;
import ar.edu.unrn.dbII.dto.CanjeDTO;
import ar.edu.unrn.dbII.dto.CategoriaDTO;
import ar.edu.unrn.dbII.dto.CiudadanoDTO;
import ar.edu.unrn.dbII.dto.EventoDTO;
import ar.edu.unrn.dbII.dto.ProductoDTO;
import ar.edu.unrn.dbII.dto.ReclamoDTO;
import ar.edu.unrn.dbII.modelo.Canje;
import ar.edu.unrn.dbII.modelo.Categoria;
import ar.edu.unrn.dbII.modelo.Ciudadano;
import ar.edu.unrn.dbII.modelo.Evento;
import ar.edu.unrn.dbII.modelo.Producto;
import ar.edu.unrn.dbII.modelo.Reclamo;
import ar.edu.unrn.dbII.utils.EntityUtil;

public class ApiImpl implements Api {
	private static ApiImpl instance = null;

	private ApiImpl() {
	}

	public static synchronized ApiImpl getInstance() {
		if (instance != null)
			return instance;

		return instance = new ApiImpl();
	}

	/*----------------------------------------------------
	 | Categorías
	 |----------------------------------------------------
	 */
	@Override
	public void nuevaCategoria(String nombre, Integer puntaje) throws RuntimeException {
		EntityUtil.sendToDB((EntityManager entityManager) -> MunicipioPpDAO.getInstance(entityManager)
				.add(new Categoria(nombre, puntaje)));
	}

	@Override
	public void bajaCategoria(UUID id) throws RuntimeException {
		EntityUtil.sendToDB((EntityManager entityManager) -> {
			CategoriaDAO categoriaDAO = new CategoriaDAO(entityManager);
			MunicipioPpDAO.getInstance(entityManager).remove(categoriaDAO.find(id));
		});
	}

	@Override
	public void modificarCategoria(UUID id, String nuevoNombre, Integer nuevoPuntaje) throws RuntimeException {
		EntityUtil.sendToDB((EntityManager entityManager) -> {
			CategoriaDAO categoriaDAO = new CategoriaDAO(entityManager);
			Categoria categoria = categoriaDAO.find(id);
			categoria.update(nuevoNombre, nuevoPuntaje);
		});
	}

	@Override
	public List<CategoriaDTO> listarCategorias() throws RuntimeException {
		return EntityUtil.retrieveFromDB((EntityManager entityManager) -> {
			List<CategoriaDTO> categoriasDTO = new ArrayList<CategoriaDTO>();
			CategoriaDAO categoriaDAO = new CategoriaDAO(entityManager);
			List<Categoria> categorias = categoriaDAO.findAll();
			for (Categoria categoria : categorias)
				categoriasDTO.add(new CategoriaDTO(categoria));

			return categoriasDTO;
		});
	}

	/*----------------------------------------------------
	 | Productos
	 |----------------------------------------------------
	 */
	@Override
	public void nuevoProducto(String nombre, Integer puntajeCosto) throws RuntimeException {
		EntityUtil.sendToDB((EntityManager entityManager) -> {
			MunicipioPpDAO.getInstance(entityManager).add(new Producto(nombre, puntajeCosto));
		});
	}

	@Override
	public void bajaProducto(UUID id) throws RuntimeException {
		EntityUtil.sendToDB((EntityManager entityManager) -> {
			ProductoDAO productoDAO = new ProductoDAO(entityManager);
			MunicipioPpDAO.getInstance(entityManager).remove(productoDAO.find(id));
		});
	}

	@Override
	public void modificarProducto(UUID id, String nuevoNombre, Integer nuevoPuntajeCosto) throws RuntimeException {
		EntityUtil.sendToDB((EntityManager entityManager) -> {
			ProductoDAO productoDAO = new ProductoDAO(entityManager);
			Producto producto = productoDAO.find(id);
			producto.update(nuevoNombre, nuevoPuntajeCosto);
		});
	}

	@Override
	public List<ProductoDTO> listarProductos() throws RuntimeException {
		return EntityUtil.retrieveFromDB((EntityManager entityManager) -> {
			List<ProductoDTO> productosDTO = new ArrayList<ProductoDTO>();
			ProductoDAO productoDAO = new ProductoDAO(entityManager);
			List<Producto> productos = productoDAO.findAll();
			for (Producto producto : productos)
				productosDTO.add(new ProductoDTO(producto));

			return productosDTO;
		});
	}

	/*----------------------------------------------------
	 | Ciudadanos
	 |----------------------------------------------------
	 */
	@Override
	public void nuevoCiudadano(String nombre, String apellido, String dni, String email) throws RuntimeException {
		EntityUtil.sendToDB((EntityManager entityManager) -> MunicipioPpDAO.getInstance(entityManager)
				.add(new Ciudadano(nombre, apellido, dni, email)));
	}

	@Override
	public void bajaCiudadano(UUID id) throws RuntimeException {
		EntityUtil.sendToDB((EntityManager entityManager) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(entityManager);
			MunicipioPpDAO.getInstance(entityManager).remove(ciudadanoDAO.find(id));
		});
	}

	@Override
	public void modificarCiudadano(UUID id, String nombre, String apellido, String email) throws RuntimeException {
		EntityUtil.sendToDB((EntityManager entityManager) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(entityManager);
			Ciudadano ciudadano = ciudadanoDAO.find(id);
			ciudadano.update(nombre, apellido, email);
		});
	}

	@Override
	public List<CiudadanoDTO> listarCiudadanos() throws RuntimeException {
		return EntityUtil.retrieveFromDB((EntityManager entityManager) -> {
			List<CiudadanoDTO> ciudadanosDTO = new ArrayList<CiudadanoDTO>();
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(entityManager);
			List<Ciudadano> ciudadanos = ciudadanoDAO.findAll();
			for (Ciudadano ciudadano : ciudadanos)
				ciudadanosDTO.add(new CiudadanoDTO(ciudadano));

			return ciudadanosDTO;
		});
	}

	@Override
	public List<CanjeDTO> listarCanjes(UUID idCiudadano) throws RuntimeException {
		return EntityUtil.retrieveFromDB((EntityManager entityManager) -> {
			List<CanjeDTO> canjesDTO = new ArrayList<CanjeDTO>();
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(entityManager);
			List<Canje> canjes = ciudadanoDAO.listarCanjes(idCiudadano);
			for (Canje canje : canjes)
				canjesDTO.add(new CanjeDTO(canje));

			return canjesDTO;
		});
	}

	@Override
	public List<CiudadanoDTO> canjearonProductoElDia(UUID idProducto, LocalDate fecha) throws RuntimeException {
		return EntityUtil.retrieveFromDB((EntityManager entityManager) -> {
			List<CiudadanoDTO> ciudadanosDTO = new ArrayList<CiudadanoDTO>();
			ProductoDAO productoDAO = new ProductoDAO(entityManager);
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(entityManager);
			Producto producto = productoDAO.find(idProducto);
			List<Ciudadano> ciudadanos = ciudadanoDAO.canjearonProductoElDia(producto, fecha);
			for (Ciudadano ciudadano : ciudadanos)
				ciudadanosDTO.add(new CiudadanoDTO(ciudadano));

			return ciudadanosDTO;
		});
	}

	@Override
	public List<CiudadanoDTO> conReclamosDeCategoria(UUID idCategoria) throws RuntimeException {
		return EntityUtil.retrieveFromDB((EntityManager entityManager) -> {
			List<CiudadanoDTO> ciudadanosDTO = new ArrayList<CiudadanoDTO>();
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(entityManager);
			CategoriaDAO categoriaDAO = new CategoriaDAO(entityManager);
			List<Ciudadano> ciudadanos = ciudadanoDAO.conReclamosDeCategoria(categoriaDAO.find(idCategoria));
			for (Ciudadano ciudadano : ciudadanos)
				ciudadanosDTO.add(new CiudadanoDTO(ciudadano));

			return ciudadanosDTO;
		});
	}

	@Override
	public List<ProductoDTO> listarProductosQueFueronCanjeadosEnFecha(LocalDate fecha) {
		return EntityUtil.retrieveFromDB((EntityManager entityManager) -> {
			List<ProductoDTO> productosDTO = new ArrayList<ProductoDTO>();
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(entityManager);
			List<Producto> productos = ciudadanoDAO.listarProductosQueFueronCanjeadosEnFecha(fecha);
			for (Producto producto : productos)
				productosDTO.add(new ProductoDTO(producto));

			return productosDTO;
		});
	}

	@Override
	public List<ProductoDTO> productosMasCanjeados() {
		return EntityUtil.retrieveFromDB((EntityManager entityManager) -> {
			List<ProductoDTO> productosDTO = new ArrayList<ProductoDTO>();
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(entityManager);
			List<Producto> productos = ciudadanoDAO.productosMasCanjeados();
			for (Producto producto : productos)
				productosDTO.add(new ProductoDTO(producto));

			return productosDTO;
		});
	}

	@Override
	public Map<Integer, CiudadanoDTO> listarEnOrdenDeCantidadDeCanjes() {
		return EntityUtil.retrieveFromDB((EntityManager entityManager) -> {
			Map<Integer, CiudadanoDTO> ciudadanos = new HashMap<Integer, CiudadanoDTO>();
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(entityManager);
			Map<Integer, Ciudadano> ciudadanosTemp = ciudadanoDAO.listarEnOrdenDeCantidadDeCanjes();
			Set<Integer> keys = ciudadanosTemp.keySet();
			for (Integer key : keys)
				ciudadanos.put(key, new CiudadanoDTO(ciudadanosTemp.get(key)));

			return ciudadanos;
		});
	}

	/*----------------------------------------------------
	 | Canjes
	 |----------------------------------------------------
	 */
	@Override
	public void canjear(UUID idCiudadano, UUID idProducto) throws RuntimeException {
		EntityUtil.sendToDB((EntityManager entityManager) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(entityManager);
			ProductoDAO productoDAO = new ProductoDAO(entityManager);
			Ciudadano ciudadano = ciudadanoDAO.find(idCiudadano);
			ciudadano.canjear(productoDAO.find(idProducto));
		});
	}

	/*----------------------------------------------------
	 | Reclamos
	 |----------------------------------------------------
	 */
	@Override
	public void reclamar(UUID idCiudadano, UUID idCategoria, String direccion, String detalle) throws RuntimeException {
		EntityUtil.sendToDB((EntityManager entityManager) -> {
			CiudadanoDAO ciudadanoDAO = new CiudadanoDAO(entityManager);
			CategoriaDAO categoriaDAO = new CategoriaDAO(entityManager);
			Ciudadano ciudadano = ciudadanoDAO.find(idCiudadano);
			ciudadano.reclamar(direccion, detalle, categoriaDAO.find(idCategoria));
		});
	}

	@Override
	public List<ReclamoDTO> listarReclamos() throws RuntimeException {
		return EntityUtil.retrieveFromDB((EntityManager entityManager) -> {
			List<ReclamoDTO> reclamosDTO = new ArrayList<ReclamoDTO>();
			ReclamoDAO reclamoDAO = new ReclamoDAO(entityManager);
			List<Reclamo> reclamos = reclamoDAO.findAll();
			for (Reclamo reclamo : reclamos)
				reclamosDTO.add(new ReclamoDTO(reclamo));

			return reclamosDTO;
		});
	}

	@Override
	public ReclamoDTO traerReclamo(UUID idReclamo) throws RuntimeException {
		return (ReclamoDTO) EntityUtil.retrieveFromDB((EntityManager entityManager) -> {
			ReclamoDAO reclamoDAO = new ReclamoDAO(entityManager);
			return new ReclamoDTO(reclamoDAO.find(idReclamo));
		});
	}

	@Override
	public ReclamoDTO traerReclamoConEventos(UUID idReclamo) throws RuntimeException {
		return (ReclamoDTO) EntityUtil.retrieveFromDB((EntityManager entityManager) -> {
			ReclamoDAO reclamoDAO = new ReclamoDAO(entityManager);
			Reclamo reclamo = reclamoDAO.find(idReclamo);
			reclamo.getEventos(); // Se fuerza a que se recuperen \o/
			return new ReclamoDTO(reclamo);
		});
	}

	@Override
	public List<ReclamoDTO> reclamoEntreFechas(LocalDate fechaDesde, LocalDate fechaHasta) throws RuntimeException {
		return EntityUtil.retrieveFromDB((EntityManager entityManager) -> {
			List<ReclamoDTO> reclamosDTO = new ArrayList<ReclamoDTO>();
			ReclamoDAO reclamoDAO = new ReclamoDAO(entityManager);
			List<Reclamo> reclamos = reclamoDAO.reclamoEntreFechas(fechaDesde, fechaHasta);
			for (Reclamo reclamo : reclamos)
				reclamosDTO.add(new ReclamoDTO(reclamo));

			return reclamosDTO;
		});
	}

	/*----------------------------------------------------
	 | Eventos
	 |----------------------------------------------------
	 */
	@Override
	public List<EventoDTO> traerEventos(UUID idReclamo) throws RuntimeException {
		return EntityUtil.retrieveFromDB((EntityManager entityManager) -> {
			List<EventoDTO> eventosDTO = new ArrayList<EventoDTO>();
			ReclamoDAO reclamoDAO = new ReclamoDAO(entityManager);
			List<Evento> eventos = reclamoDAO.listarEventos(idReclamo);
			for (Evento evento : eventos)
				eventosDTO.add(new EventoDTO(evento));

			return eventosDTO;
		});
	}

	@Override
	public void agregarEvento(UUID idReclamo, String descripcion) throws RuntimeException {
		EntityUtil.sendToDB((EntityManager entityManager) -> {
			ReclamoDAO reclamoDAO = new ReclamoDAO(entityManager);
			Reclamo reclamo = reclamoDAO.find(idReclamo);
			reclamo.agregarEvento(new Evento(descripcion, LocalDate.now(), reclamo));
		});
	}
}
