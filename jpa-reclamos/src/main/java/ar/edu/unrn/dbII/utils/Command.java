package ar.edu.unrn.dbII.utils;

import javax.persistence.EntityManager;

public interface Command {
	void execute(EntityManager em);
}
