package ar.edu.unrn.dbII.dto;

import java.time.LocalDate;
import java.util.UUID;

import org.bson.Document;

import ar.edu.unrn.dbII.modelo.Evento;

public class EventoDTO extends GenericDTO {
	private String descripcion;
	private LocalDate fecha;
	private UUID reclamoId;

	public EventoDTO(UUID id, String descripcion, LocalDate fecha, UUID reclamoId) {
		this.id = id;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.reclamoId = reclamoId;
	}

	public EventoDTO(Evento evento) {
		this.id = evento.getId();
		this.descripcion = evento.getDescripcion();
		this.fecha = evento.getFecha();
		this.reclamoId = evento.getReclamo().getId();
	}

	public EventoDTO(Document doc) {
		this.id = doc.get("id", UUID.class);
		this.descripcion = doc.getString("descripcion");
		this.fecha = LocalDate.parse(doc.getString("fecha"));
		this.reclamoId = doc.get("reclamoId", UUID.class);
	}

	public Document toDocument() {
		Document document = super.toDocument();
		document.append("descripcion", descripcion).append("fecha", fecha.toString()).append("reclamoId", reclamoId);
		return document;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public UUID getReclamoId() {
		return reclamoId;
	}

	public void setReclamoId(UUID reclamoId) {
		this.reclamoId = reclamoId;
	}

}
