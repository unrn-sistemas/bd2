package ar.edu.unrn.dbII.modelo;

import java.time.LocalDate;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "eventos")
public class Evento extends GenericModel {
	private String descripcion;
	private LocalDate fecha;

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "reclamo_id")
	private Reclamo reclamo;

	/**
	 * Constructor para instancias existentes en la BD.
	 */
	public Evento() {

	}

	public Evento(UUID id, String descripcion, LocalDate fecha, Reclamo reclamo) {
		super(id);
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.reclamo = reclamo;
	}

	/**
	 * Constructor para instancias que no están en la DB.
	 */
	public Evento(String descripcion, LocalDate fecha, Reclamo reclamo) {
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.reclamo = reclamo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public Reclamo getReclamo() {
		return reclamo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public void setReclamo(Reclamo reclamo) {
		this.reclamo = reclamo;
	}

}
