package ar.edu.unrn.dbII.dto;

import java.util.UUID;

import org.bson.Document;

public abstract class GenericDTO {
	protected UUID id;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Document toDocument() {
		Document document = new Document().append("id", id);

		return document;
	}
}
