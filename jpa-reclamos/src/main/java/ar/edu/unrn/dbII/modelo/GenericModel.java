package ar.edu.unrn.dbII.modelo;

import java.util.UUID;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class GenericModel {

	@Id
	@GeneratedValue
	private UUID id;

	public GenericModel() {
	}

	public GenericModel(UUID id) {
		this.id = id;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}