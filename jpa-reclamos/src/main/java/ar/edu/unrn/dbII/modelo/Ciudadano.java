package ar.edu.unrn.dbII.modelo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ciudadanos")
public class Ciudadano extends GenericModel {
	private String nombre;
	private String apellido;
	private String dni;
	private String email;
	private Integer puntajeAcumulado;

	@OneToMany(mappedBy = "ciudadano", cascade = { CascadeType.ALL })
	@OrderColumn(name = "idx")
	private List<Reclamo> reclamos = new ArrayList<Reclamo>();

	@OneToMany(mappedBy = "ciudadano", cascade = { CascadeType.ALL })
	@OrderColumn(name = "idx")
	private List<Canje> canjes = new ArrayList<Canje>();

	public Ciudadano() {
	}

	/**
	 * Constructor para instancias existentes en la BD. No sirve para actualizar instancias existentes.
	 */
	public Ciudadano(UUID id, String nombre, String apellido, String dni, String email, Integer puntajeAcumulado) {
		super(id);
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.email = email;
		this.puntajeAcumulado = puntajeAcumulado;
	}

	/**
	 * Constructor para instancias inexistentes en la BD.
	 */
	public Ciudadano(String nombre, String apellido, String dni, String email) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.email = email;
		this.puntajeAcumulado = 0;
	}

	// ---------------------------------------------------
	// Reglas del negocio
	// ---------------------------------------------------
	public void canjear(Producto producto) {
		// Si el mes no es septiembre, se evalúa de forma normal. Sino, es
		// gratis.
		if (LocalDate.now().getMonthValue() != 9) {
			if (!producto.canjeable(this.puntajeAcumulado)) {
				throw new RuntimeException("No tiene puntos suficientes para canjear el producto");
			}
			this.puntajeAcumulado -= producto.getPuntajeCosto();
		}
		canjes.add(new Canje(LocalDate.now(), this, producto));
	}

	public void reclamar(String direccion, String detalle, Categoria categoria) {
		this.reclamos.add(new Reclamo(LocalDate.now(), direccion, detalle, this, categoria));
		// Acumulo a mi puntaje actual, el puntaje de la Categoría del nuevo
		// Reclamo. Y si es septiembre, lo multiplico.
		this.puntajeAcumulado += LocalDate.now().getMonthValue() != 9 ? categoria.getPuntaje()
				: categoria.getPuntaje() * 2;
	}

	/**
	 * Cualquier parámetro puede ser null.
	 * 
	 * @param nombre
	 *            nullable
	 * @param apellido
	 *            nullable
	 * @param email
	 *            nullable
	 */
	public void update(String nombre, String apellido, String email) {
		this.nombre = nombre == null ? this.nombre : nombre;
		this.apellido = apellido == null ? this.apellido : apellido;
		this.email = email == null ? this.email : email;
	}

	// ---------------------------------------------------
	// Getters
	// ---------------------------------------------------
	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getDni() {
		return dni;
	}

	public String getEmail() {
		return email;
	}

	public Integer getPuntajeAcumulado() {
		return puntajeAcumulado;
	}

	public List<Reclamo> getReclamos() {
		return reclamos;
	}

	public List<Canje> getCanjes() {
		return canjes;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPuntajeAcumulado(Integer puntajeAcumulado) {
		this.puntajeAcumulado = puntajeAcumulado;
	}

	public void setReclamos(List<Reclamo> reclamos) {
		this.reclamos = reclamos;
	}

	public void setCanjes(List<Canje> canjes) {
		this.canjes = canjes;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object model) {
		if (!(model instanceof Ciudadano))
			return false;
		return this.getId().equals(((Ciudadano) model).getId());
	}

}
