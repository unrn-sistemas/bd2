package ar.edu.unrn.dbII.router;

import static spark.Spark.get;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import ar.edu.unrn.dbII.api.Api;
import ar.edu.unrn.dbII.api.impl.ApiImpl;
import ar.edu.unrn.dbII.dto.CategoriaDTO;

public class Router {
	private static final Api api;

	static {
		api = ApiImpl.getInstance();
		api.nuevaCategoria("Categoria 1", 10);
		api.nuevaCategoria("Categoria 2", 20);
		api.nuevaCategoria("Categoria 3", 30);
		api.nuevaCategoria("Categoria 4", 30);
		api.nuevoProducto("Producto 1", 5);
		api.nuevoProducto("Producto 2", 15);
		api.nuevoProducto("Producto 3", 25);
		api.nuevoProducto("Producto 4", 35);
		api.nuevoCiudadano("Gabriela", "Cayú", "33841853", "gcayu@unrn.edu.ar");
		api.nuevoCiudadano("Luciano", "Graziani", "35591234", "lgraziani@unrn.edu.ar");
		api.nuevoCiudadano("Mauro", "Cambarieri", "25252525", "mcambarieri@unrn.edu.ar");
		api.nuevoCiudadano("Enrique", "Molinari", "28282828", "mmolinari@unrn.edu.ar");
	}

	public static void main(String[] args) {
		get("/", (req, res) -> {
			List<CategoriaDTO> categorias = api.listarCategorias();
			List<Document> categoriasDoc = new ArrayList<Document>();
			for (CategoriaDTO categoria : categorias)
				categoriasDoc.add(categoria.toDocument());

			return new Document().append("array", categoriasDoc).toJson();
		});
	}
}
