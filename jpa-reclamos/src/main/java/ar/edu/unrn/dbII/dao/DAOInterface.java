package ar.edu.unrn.dbII.dao;

import java.util.List;
import java.util.UUID;

public interface DAOInterface<T> {

	T find(UUID id);

	List<T> findAll();
}
